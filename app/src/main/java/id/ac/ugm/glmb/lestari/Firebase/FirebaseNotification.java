package id.ac.ugm.glmb.lestari.Firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.json.JSONObject;

import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.MainActivity;


/**
 * Created by root on 5/7/18.
 */

public class FirebaseNotification {
    private static final String TAG = "NotificationManager";
    private static FirebaseNotification notifme;
    public static final int ID_SMALL_NOTIFICATION = 235;
    private Context ctx;

    public static synchronized FirebaseNotification getInstance(Context context) {
        if (notifme == null) {
            notifme = new FirebaseNotification(context);
        }
        return notifme;
    }

    public FirebaseNotification(Context ctx) {
        this.ctx = ctx;
    }

    public void sendPushNotification(JSONObject json) {
        Log.d(TAG, "Notification JSON " + json.toString());
        try {
            /*JSONObject data = json.getJSONObject("data");
            String title = data.getString("title");
            String message = data.getString("message");*/

//            JSONObject data = json.getJSONObject("data");
            String title = "";
            try {
                title = json.getString("title").substring(0, 1).toUpperCase()+json.getString("title").substring(1).toUpperCase();
            } catch (IndexOutOfBoundsException e) {

            }
            String message = "Kiriman telah dipublikasikan";
            switch (json.getString("jenis")){
                case  "1":
                    message = "lihat selengkapnya di Lestari";
                    break;
                case "2":
                    message = "lihat selengkapnya di Lestari";
                    break;
                case "3":
                    message = "Seseorang telah berkontribusi pada degradasi Anda, lihat selengkapnya di Lestari";
                    break;
            }
            Intent intent  = new Intent(ctx, MainActivity.class);

            showSmallNotification(title, message, intent);
        }
        /*catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        }*/ catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    public void showSmallNotification(String title, String message, Intent intent) {
        String NOTIFICATION_CHANNEL_ID = "lestari_channel";
        NotificationManager notificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notifikasi Lestari", NotificationManager.IMPORTANCE_DEFAULT);
            // Configure the notification channel.
            notificationChannel.setDescription("Lestari");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.GREEN);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ctx, NOTIFICATION_CHANNEL_ID);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            mBuilder.setPriority(Notification.PRIORITY_HIGH);
        }
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        ctx,
                        ID_SMALL_NOTIFICATION,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);
        mBuilder.setOnlyAlertOnce(true);
        Notification notification = mBuilder.setSmallIcon(R.drawable.notif_img).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setContentTitle(title)
                .setSmallIcon(R.drawable.notif_img)
                .setLargeIcon(BitmapFactory.decodeResource(ctx.getResources(), R.drawable.notif_img))
                .setContentText(message)
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(ID_SMALL_NOTIFICATION, notification);
    }
}
