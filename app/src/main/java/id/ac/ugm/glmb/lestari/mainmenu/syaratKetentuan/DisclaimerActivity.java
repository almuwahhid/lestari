package id.ac.ugm.glmb.lestari.mainmenu.syaratKetentuan;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariParam;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;

public class DisclaimerActivity extends ActivityGeneral {

    @BindView(R.id.helper_loading_box)
    protected RelativeLayout helper_loading_box;
    @BindView(R.id.helper_empty)
    protected RelativeLayout helper_empty;
    @BindView(R.id.helper_noconnection)
    protected RelativeLayout helper_noconnection;

    @BindView(R.id.webView)
    WebView webView;

    String header = "";
    String data = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disclaimer);
        ButterKnife.bind(this);

        ToolbarLestari toolbarLestari = new ToolbarLestari(getContext(),"Syarat dan Ketentuan");

        doGetDisclaimer(LestariParam.stringSyaratKetentuan());
    }

    private void doGetDisclaimer(String URL){
        LestariRequest.POST(URL, getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                helper_noconnection.setVisibility(View.GONE);
                helper_empty.setVisibility(View.GONE);
                helper_loading_box.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(JSONObject response) {
                helper_loading_box.setVisibility(View.GONE);
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        JSONObject dataJ = response.getJSONObject("data");
                        data = dataJ.getString("Isi");
                        String html = LestariConstant.WEBVIEW_STYLE;
                        String mime = "text/html";
                        String encoding = "utf-8";

                        webView.loadData(String.format(html, data), mime, encoding);
                    }else{
                        helper_empty.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                helper_noconnection.setVisibility(View.VISIBLE);
                helper_loading_box.setVisibility(View.GONE);
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }
        });
    }
}
