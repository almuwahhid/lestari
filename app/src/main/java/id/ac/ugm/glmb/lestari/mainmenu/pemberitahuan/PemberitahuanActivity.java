package id.ac.ugm.glmb.lestari.mainmenu.pemberitahuan;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;

public class PemberitahuanActivity extends ActivityGeneral {
    ToolbarLestari toolbarLestari;
    @BindView(R.id.recyclerView) protected RecyclerView recyclerView;

    List<PemberitahuanClass> classes;
    LinearLayoutManager lm;
    PemberitahuanAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemberitahuan);
        ButterKnife.bind(this);

        toolbarLestari = new ToolbarLestari(getContext(), "Pemberitahuan");
        classes = new ArrayList<>();
        lm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(lm);
        adapter = new PemberitahuanAdapter(getContext(), classes);

        recyclerView.setAdapter(adapter);
    }
}
