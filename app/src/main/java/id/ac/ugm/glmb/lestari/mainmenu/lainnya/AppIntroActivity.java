package id.ac.ugm.glmb.lestari.mainmenu.lainnya;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;

import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.MainActivity;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;

public class AppIntroActivity extends AppIntro {

    boolean isFromLainnya = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_app_intro);

        if(getIntent().hasExtra("lainnya")){
            isFromLainnya = true;
        }

        SliderPage sliderPage = new SliderPage();
        sliderPage.setDescription(getResources().getString(R.string.desc_intro_lestari));
        sliderPage.setTitle("Lestari");
        sliderPage.setImageDrawable(R.drawable.map_indonesia);
        sliderPage.setBgColor(ContextCompat.getColor(getContext(),R.color.primary));
        addSlide(AppIntroFragment.newInstance(sliderPage));

        sliderPage = new SliderPage();
        sliderPage.setDescription(getResources().getString(R.string.desc_intro_degradasi));
        sliderPage.setTitle("Upload Degradasi");
        sliderPage.setImageDrawable(R.drawable.intro_degradasi);
        sliderPage.setBgColor(ContextCompat.getColor(getContext(),R.color.blue_300));
        addSlide(AppIntroFragment.newInstance(sliderPage));

        sliderPage = new SliderPage();
        sliderPage.setDescription(getResources().getString(R.string.desc_intro_kontribusi));
        sliderPage.setTitle("Menambahkan Kontribusi");
        sliderPage.setImageDrawable(R.drawable.intro_kontribusi);
        sliderPage.setBgColor(ContextCompat.getColor(getContext(),R.color.red_200));
        addSlide(AppIntroFragment.newInstance(sliderPage));

        sliderPage = new SliderPage();
        sliderPage.setDescription(getResources().getString(R.string.desc_intro_download));
        sliderPage.setTitle("Download Degradasi");
        sliderPage.setImageDrawable(R.drawable.intro_download);
        sliderPage.setBgColor(ContextCompat.getColor(getContext(),R.color.green_200));
        addSlide(AppIntroFragment.newInstance(sliderPage));

        sliderPage = new SliderPage();
        sliderPage.setDescription(getResources().getString(R.string.desc_intro_erosi));
        sliderPage.setTitle("Erosi");
        sliderPage.setImageDrawable(R.drawable.ic_erosi_intro);
        sliderPage.setBgColor(ContextCompat.getColor(getContext(),R.color.blue_200));
        addSlide(AppIntroFragment.newInstance(sliderPage));

        sliderPage = new SliderPage();
        sliderPage.setDescription(getResources().getString(R.string.desc_intro_sedimentasi));
        sliderPage.setTitle("Sedimentasi");
        sliderPage.setImageDrawable(R.drawable.ic_sedimentasi_intro);
        sliderPage.setBgColor(ContextCompat.getColor(getContext(),R.color.brown_200));
        addSlide(AppIntroFragment.newInstance(sliderPage));

        sliderPage = new SliderPage();
        sliderPage.setDescription(getResources().getString(R.string.desc_intro_eutrofikasi));
        sliderPage.setTitle("Eutrofikasi");
        sliderPage.setImageDrawable(R.drawable.ic_eutrofikasi_intro);
        sliderPage.setBgColor(ContextCompat.getColor(getContext(),R.color.orange_200));
        addSlide(AppIntroFragment.newInstance(sliderPage));

        sliderPage = new SliderPage();
        sliderPage.setDescription(getResources().getString(R.string.desc_intro_banjir));
        sliderPage.setTitle("Banjir");
        sliderPage.setImageDrawable(R.drawable.ic_banjir_intro);
        sliderPage.setBgColor(ContextCompat.getColor(getContext(),R.color.teal_200));
        addSlide(AppIntroFragment.newInstance(sliderPage));

        sliderPage = new SliderPage();
        sliderPage.setDescription(getResources().getString(R.string.desc_intro_sampah));
        sliderPage.setTitle("Sampah");
        sliderPage.setImageDrawable(R.drawable.ic_sampah_intro);
        sliderPage.setBgColor(ContextCompat.getColor(getContext(),R.color.yellow_300));
        addSlide(AppIntroFragment.newInstance(sliderPage));

        sliderPage = new SliderPage();
        sliderPage.setDescription(getResources().getString(R.string.desc_intro_limbah));
        sliderPage.setTitle("Limbah");
        sliderPage.setImageDrawable(R.drawable.ic_limbah_intro);
        sliderPage.setBgColor(ContextCompat.getColor(getContext(),R.color.yellow_600));
        addSlide(AppIntroFragment.newInstance(sliderPage));

        sliderPage = new SliderPage();
        sliderPage.setDescription(getResources().getString(R.string.desc_intro_longsor));
        sliderPage.setTitle("Longsor");
        sliderPage.setImageDrawable(R.drawable.ic_longsor_intro);
        sliderPage.setBgColor(ContextCompat.getColor(getContext(),R.color.purple_200));
        addSlide(AppIntroFragment.newInstance(sliderPage));

        sliderPage = new SliderPage();
        sliderPage.setDescription(getResources().getString(R.string.desc_intro_kebakaran));
        sliderPage.setTitle("Kebakaran");
        sliderPage.setImageDrawable(R.drawable.ic_kebakaran_intro);
        sliderPage.setBgColor(ContextCompat.getColor(getContext(),R.color.deep_orange_200));
        addSlide(AppIntroFragment.newInstance(sliderPage));

        sliderPage = new SliderPage();
        sliderPage.setDescription(getResources().getString(R.string.desc_intro_pengendalian));
        sliderPage.setTitle("Pengendalian");
        sliderPage.setImageDrawable(R.drawable.ic_pengendalian_intro);
        sliderPage.setBgColor(ContextCompat.getColor(getContext(),R.color.green_A200));
        addSlide(AppIntroFragment.newInstance(sliderPage));

        setColorDoneText(ContextCompat.getColor(getContext(),R.color.white));
        setIndicatorColor(ContextCompat.getColor(getContext(),R.color.grey_700), ContextCompat.getColor(getContext(),R.color.grey_700));

        showSkipButton(true);
        setProgressButtonEnabled(true);
    }
    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.

    }

    private void rediretToNext(){
        if(isFromLainnya){
            finish();
        } else {
            Lestari.setSPBoolean(getContext(), LestariConstant.SP_INTRO, true);
            startActivity(new Intent(getContext(), MainActivity.class));
        }
    }


    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        rediretToNext();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        rediretToNext();
    }

    private Context getContext(){
        return this;
    }
}
