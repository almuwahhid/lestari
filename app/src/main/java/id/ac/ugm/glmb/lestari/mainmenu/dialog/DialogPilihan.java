package id.ac.ugm.glmb.lestari.mainmenu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.helper.DialogFilterHelper;

public class DialogPilihan {

    Context context;
    Dialog dialog;
    List<PilihanClass> lists, lists_real;
    RecyclerView recyclerView;
    CardView card_search;
    ImageView img_empty, img_close;
    EditText edt_search;
    TextView tv_title;
    String title;
    boolean isSearchEmpty = true;
    boolean isSearch = false;

    DialogPilihanAdapter dialogPilihanAdapter;
    OnItemClick itemClickListener;

    public DialogPilihan(Context context, List<PilihanClass> datas, String title) {
        this.context = context;
        lists = new ArrayList<>();
        lists_real = new ArrayList<>();
        for (PilihanClass pilihanClass : datas){
            this.lists_real.add(pilihanClass);
            this.lists.add(pilihanClass);
        }

        dialog = new Dialog(context);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimHorizontal;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_dialog_pilihan);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        this.title = title;
        initContext();
    }

    public DialogPilihan(Context context, List<PilihanClass> datas, String title, Boolean isSearch) {
        this.context = context;
        lists = new ArrayList<>();
        lists_real = new ArrayList<>();
        for (PilihanClass pilihanClass : datas){
            this.lists_real.add(pilihanClass);
            this.lists.add(pilihanClass);
        }
        this.isSearch = isSearch;

        dialog = new Dialog(context);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimHorizontal;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_dialog_pilihan);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        this.title = title;
        initContext();
    }

    public void setTitle(String title) {
        this.title = title;
        tv_title.setText(title);
    }

    private void initContext(){
        dialogPilihanAdapter = new DialogPilihanAdapter(context, lists);

        tv_title = dialog.findViewById(R.id.tv_title);
        recyclerView = dialog.findViewById(R.id.recyclerView);
        edt_search = dialog.findViewById(R.id.edt_search);
        card_search = dialog.findViewById(R.id.card_search);
        img_empty = dialog.findViewById(R.id.img_empty);
        img_close = dialog.findViewById(R.id.img_close);
        tv_title.setText(title);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(dialogPilihanAdapter);
        dialogPilihanAdapter.setOnClickListener(new DialogPilihanAdapter.OnClickListener() {
            @Override
            public void onClick(View view, PilihanClass pilihanClass, int position) {
                itemClickListener.onItemClick(view, pilihanClass, position);
                dismiss();
            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        if(!isSearch){
            card_search.setVisibility(View.GONE);
        }

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(edt_search.getText().toString().equals("")){
                    isSearchEmpty = true;
                    img_empty.setColorFilter(ContextCompat.getColor(context, R.color.grey_300), android.graphics.PorterDuff.Mode.SRC_IN);
                    lists.clear();
                    for(PilihanClass pickerModel:lists_real){
                        lists.add(pickerModel);
                    }
                    dialogPilihanAdapter.notifyDataSetChanged();
                }else{
                    isSearchEmpty = false;
                    img_empty.setColorFilter(ContextCompat.getColor(context, R.color.grey_500), android.graphics.PorterDuff.Mode.SRC_IN);
                    lists.clear();
                    for (PilihanClass pickerModel: DialogFilterHelper.dataSearch(lists_real, edt_search.getText().toString())){
                        lists.add(pickerModel);
                    }
                    dialogPilihanAdapter.notifyDataSetChanged();
                }

                /*if(list_recycler.size()==0){
                    lay_empty.setVisibility(View.VISIBLE);
                }else{
                    lay_empty.setVisibility(View.GONE);
                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        img_empty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isSearchEmpty){
                    isSearchEmpty = true;
                    edt_search.setText("");
                    img_empty.setColorFilter(ContextCompat.getColor(context, R.color.grey_300), android.graphics.PorterDuff.Mode.SRC_IN);
                    lists.clear();
                    for(PilihanClass pickerModel:lists_real){
                        lists.add(pickerModel);
                    }
                    dialogPilihanAdapter.notifyDataSetChanged();
                }
                /*if(lis.size()==0){
                    lay_empty.setVisibility(View.VISIBLE);
                }else{
                    lay_empty.setVisibility(View.GONE);
                }*/
            }
        });
    }


    public void dismiss(){
        if(dialog!= null)
            this.dialog.dismiss();
    }

    public void show(){
        if(!isSearchEmpty){
            isSearchEmpty = true;
            img_empty.setColorFilter(ContextCompat.getColor(context, R.color.grey_300), android.graphics.PorterDuff.Mode.SRC_IN);
            lists.clear();
            for(PilihanClass pickerModel:lists_real){
                lists.add(pickerModel);
            }
            dialogPilihanAdapter.notifyDataSetChanged();
        }
        this.dialog.show();
    }

    public void setOnItemClick(OnItemClick onClickListener) {
        this.itemClickListener = onClickListener;
    }

    public interface OnItemClick{
        void onItemClick(View view, PilihanClass pilihanClass, int position);
    }

    public void updateList(List<PilihanClass> classes){
        this.lists.clear();
        for(PilihanClass pilihanClass:classes){
            this.lists.add(pilihanClass);
        }
        dialogPilihanAdapter.notifyDataSetChanged();
    }

}
