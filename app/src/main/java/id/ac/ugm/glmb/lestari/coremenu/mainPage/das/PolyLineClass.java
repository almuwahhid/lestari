package id.ac.ugm.glmb.lestari.coremenu.mainPage.das;

import java.io.Serializable;
import java.util.List;

/**
 * Created by root on 7/31/18.
 */

public class PolyLineClass implements Serializable{

    List<Titik> titiks;

    public List<Titik> getTitiks() {
        return titiks;
    }

    public void setTitiks(List<Titik> titiks) {
        this.titiks = titiks;
    }
}
