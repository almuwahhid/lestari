package id.ac.ugm.glmb.lestari.coremenu.splashScreen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import id.ac.ugm.glmb.lestari.BuildConfig;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.MainActivity;
import id.ac.ugm.glmb.lestari.mainmenu.CheckUpdate.CheckUpdateActivity;
import id.ac.ugm.glmb.lestari.mainmenu.CheckUpdate.VersionChecker;
import id.ac.ugm.glmb.lestari.mainmenu.login.LoginActivity;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

public class SplashScreenActivity extends AppCompatActivity implements VersionChecker.OnError{

    Intent intent;
    VersionChecker versionChecker;
    Thread timer;
    String latestVersion = "";

    boolean flagTimer = false, flagPause = false;
    TextView tv_version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        tv_version = findViewById(R.id.tv_version);
        tv_version.setText("Versi "+ BuildConfig.versi);
        versionChecker = new VersionChecker();
        versionChecker.setOnError(this);
        versionChecker.execute();
        YoYo.with(Techniques.FadeIn)
                .duration(2000)
                .repeat(0)
                .playOn(findViewById(R.id.img_logo));

    }

    private void initTimer(){
        timer = new Thread() {
            public void run() {
                try {
                    //Create the database
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    continueIntent();
                } catch (Exception e) {
                    e.printStackTrace();
                    continueIntent();
                } finally {
                    if(!flagPause){
                        finallyAction();
                    } else {
                        flagTimer = true;
                    }
                }
            }
        };
    }

    private void finallyAction(){
        if(!latestVersion.isEmpty()){
//                        Log.d("asdCheck", "runAsli: "+Function.VERSION_NAME);
//                        Log.d("asdCheck", "runAmbil data: "+latestVersion);
            if(BuildConfig.VERSION_NAME.equalsIgnoreCase(latestVersion)){
                continueIntent();
            }else{
                startActivity(new Intent(SplashScreenActivity.this, CheckUpdateActivity.class));
                finish();
            }
        }else{
            continueIntent();
        }
    }

    private void continueIntent(){
        Lestari.checkLoginStatus(SplashScreenActivity.this, new LestariUi.OnEventConfirm() {
            @Override
            public void onEventTrue() {
                intent = new Intent(SplashScreenActivity.this, MainActivity.class);
            }

            @Override
            public void onEventFalse() {
                intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
            }
        });

        startActivity(intent);
        finish();
    }

    @Override
    public void onError(String s) {
        latestVersion = s;
        Log.e("aasd", "onError: masuk yee"+s);
        initTimer();
        timer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        flagPause = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(flagTimer){
            finallyAction();
        }
    }
}
