package id.ac.ugm.glmb.lestari.utilities;

import id.ac.ugm.glmb.lestari.BuildConfig;

/**
 * Created by root on 6/23/18.
 */

public class LestariParam {
    public static String a = BuildConfig.base_url;
    private static String b = BuildConfig.api;
    private static String c = a;

    public static String stringLogin() {
        return c + BuildConfig.login;
    }

    public static String stringRegister() {
        return c + BuildConfig.register;
    }

    public static String stringEditUser() {
        return c + BuildConfig.editUser;
    }

    public static String stringEditPassword() {
        return c + BuildConfig.editPassword;
    }

    public static String stringDegradasi() {
        return c + BuildConfig.degradasi;
    }

    public static String stringGallery() {
        return c + BuildConfig.gallery;
    }

    public static String stringCity() {
        return c + BuildConfig.kota;
    }

    public static String stringMateri() {
        return c + BuildConfig.materi;
    }

    public static String stringSyaratKetentuan() {
        return c + BuildConfig.syaratKetentuan;
    }

    public static String stringLupaPassword() {
        return c + BuildConfig.lupaPassword;
    }

    public static String stringLoginEksternal() {
        return c + BuildConfig.loginExternal;
    }

    public static String stringProfile() {
        return c + BuildConfig.profil;
    }

    public static String google_credential() {
        return BuildConfig.cre_google;
    }

    public static String das() {
        return BuildConfig.das;
    }

}
