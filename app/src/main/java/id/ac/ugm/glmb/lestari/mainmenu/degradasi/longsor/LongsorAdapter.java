package id.ac.ugm.glmb.lestari.mainmenu.degradasi.longsor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.PhotoActivity;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

public class LongsorAdapter extends RecyclerView.Adapter<LongsorAdapter.MyHolder> {
    Context context;
    List<LongsorClass> classes;
    Boolean isDetail = false;

    public LongsorAdapter(Context context, List<LongsorClass> classes, Boolean isDetail) {
        this.context = context;
        this.classes = classes;
        this.isDetail = isDetail;
    }

    public LongsorAdapter(Context context, List<LongsorClass> classes) {
        this.context = context;
        this.classes = classes;
    }

    public LongsorAdapter() {
    }

    @Override
    public LongsorAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_longsor_adapter, parent, false);
        MyHolder rcv = new MyHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(LongsorAdapter.MyHolder holder, int position) {
        LongsorClass kelas = classes.get(position);
        new PhotoActivity(context, holder.recyclerView, holder.tv_nodata, kelas.getGalleries());
        holder.tv_deskripsi.setText(kelas.getDeskripsi());
        holder.tv_name.setText(kelas.getName());
        try{
            String tgl = kelas.getDate().split(" ")[0];
            int year = Integer.valueOf(tgl.split("-")[0]);
            int month = Integer.valueOf(tgl.split("-")[1])-1;
            int date = Integer.valueOf(tgl.split("-")[2]);
            holder.tv_date.setText(date+" "+ LestariUi.monthName(month)+" "+year+", "+kelas.getDate().split(" ")[1]);
        } catch (IndexOutOfBoundsException e){
            holder.tv_date.setText("");
        } catch (NumberFormatException e){
            holder.tv_date.setText("");
        }

        holder.tv_tipologi.setText(kelas.getTipologi());
        holder.tv_panjang.setText(kelas.getPanjang());
        holder.tv_lebar.setText(kelas.getLebar());
        holder.tv_kedalaman.setText(kelas.getKedalaman());
    }

    @Override
    public int getItemCount() {
        return classes.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_tipologi)
        TextView tv_tipologi;
        @BindView(R.id.tv_lebar)
        TextView tv_lebar;
        @BindView(R.id.tv_panjang)
        TextView tv_panjang;
        @BindView(R.id.tv_kedalaman)
        TextView tv_kedalaman;

        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;
        @BindView(R.id.tv_deskripsi)
        TextView tv_deskripsi;
        @BindView(R.id.tv_nodata)
        TextView tv_nodata;
        @BindView(R.id.tv_date)
        TextView tv_date;
        @BindView(R.id.tv_name)
        TextView tv_name;
        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
