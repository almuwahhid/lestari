package id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.longsor;

import java.util.ArrayList;
import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;

/**
 * Created by root on 7/22/18.
 */

public class TambahLongsorHelper {
    public static List<PilihanClass> listTipologi(){
        List<PilihanClass> listDegradasi = new ArrayList<>();
        PilihanClass pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.EROSI);
        pilihanClass.setName("Translasional");
        pilihanClass.setId("Translasional");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Pergerakan Blok");
        pilihanClass.setName("Pergerakan Blok");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Debris Flow");
        pilihanClass.setName("Debris Flow");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Jatuhan Batuan");
        pilihanClass.setName("Jatuhan Batuan");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Rotasional");
        pilihanClass.setName("Rotasional");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Earthflow");
        pilihanClass.setName("Earthflow");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Topple");
        pilihanClass.setName("Topple");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Rayapan");
        pilihanClass.setName("Rayapan");
        listDegradasi.add(pilihanClass);


        pilihanClass = new PilihanClass();
        pilihanClass.setId("Lateral");
        pilihanClass.setName("Lateral");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Debris Avalance");
        pilihanClass.setName("Debris Avalance");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("-");
        pilihanClass.setName("Lainnya");
        listDegradasi.add(pilihanClass);

        return listDegradasi;
    }
}
