package id.ac.ugm.glmb.lestari.mainmenu.dialog;

/**
 * Created by root on 7/10/18.
 */

public class PilihanClass {
    String id, name="";

    Boolean state = false;

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
