package id.ac.ugm.glmb.lestari.mainmenu.lainnya;

import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.BuildConfig;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;

public class AboutActivity extends ActivityGeneral {
    @BindView(R.id.tv_version) protected TextView tv_version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);

        new ToolbarLestari(getContext(), "Tentang Aplikasi");

        tv_version.setText("Versi "+ BuildConfig.versi);
    }
}
