package id.ac.ugm.glmb.lestari.mainmenu.dialog;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import id.ac.ugm.glmb.lestari.R;

public class DialogPilihanAdapter extends RecyclerView.Adapter<DialogPilihanAdapter.MyHolder> {
    Context context;
    List<PilihanClass> lists;
    OnClickListener itemClickListener;

    public DialogPilihanAdapter(Context context, List<PilihanClass> lists) {
        this.context = context;
        this.lists = lists;
    }

    public DialogPilihanAdapter() {
    }

    @Override
    public DialogPilihanAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_dialog_pilihan_adapter, parent, false);
        MyHolder rcv = new MyHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(DialogPilihanAdapter.MyHolder holder, final int position) {
        holder.tv_item.setText(lists.get(position).getName());
        holder.lay_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onClick(v, lists.get(position), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView tv_item;
        LinearLayout lay_item;
        public MyHolder(View itemView) {
            super(itemView);
            tv_item = itemView.findViewById(R.id.tv_item);
            lay_item = itemView.findViewById(R.id.lay_item);
        }
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.itemClickListener = onClickListener;
    }

    public interface OnClickListener {
        void onClick(View view, PilihanClass pilihanClass, int position);
    }
}
