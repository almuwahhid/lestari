package id.ac.ugm.glmb.lestari.mainmenu.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import id.ac.ugm.glmb.lestari.R;

public class DialogPhoto {
    Context context;
    Dialog dialog;
    View view;
    ImageView img_close, img_photo, img_content;
    Boolean showDegradasi = false;
    Boolean isPhotoProfil = false;
    RelativeLayout lay_photo, helper_loading_box, helper_noconnection;
    String url;

    OnPickPhoto onPickPhoto;

    public DialogPhoto(Context context, String url) {
        this.context = context;
        this.url = url;
        this.view = View.inflate(context, R.layout.lay_dialog_photo, null);
        initComponent();
    }

    public DialogPhoto() {
    }

    public DialogPhoto(Context context, String url, Boolean isPhotoProfil, OnPickPhoto onPickPhoto) {
        this.context = context;
        this.isPhotoProfil = isPhotoProfil;
        this.view = View.inflate(context, R.layout.lay_dialog_photo, null);
        this.url = url;
        this.onPickPhoto = onPickPhoto;

        initComponent();
    }

    private void initComponent(){
        dialog = new Dialog(context);

        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTopAnimation;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.lay_dialog_photo);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.setCancelable(true);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP;
//        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);


        img_close = dialog.findViewById(R.id.img_close);
        lay_photo = dialog.findViewById(R.id.lay_photo);
        helper_loading_box = dialog.findViewById(R.id.helper_loading_box);
        helper_noconnection = dialog.findViewById(R.id.helper_noconnection);
        img_photo = dialog.findViewById(R.id.img_photo);
        img_content = dialog.findViewById(R.id.img_content);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        lay_photo.setLayoutParams(new FrameLayout.LayoutParams(width, height));

        if(isPhotoProfil){
            img_photo.setVisibility(View.VISIBLE);
            img_photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    onPickPhoto.onPickPhoto();
                }
            });
        }

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        helper_loading_box.setVisibility(View.VISIBLE);
        helper_noconnection.setVisibility(View.GONE);
        Picasso.with(context)
                .load(this.url)
                .error(R.drawable.no_data_available)
                .into(img_content, new Callback() {
                    @Override
                    public void onSuccess() {
                        helper_loading_box.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        helper_loading_box.setVisibility(View.GONE);
                        helper_noconnection.setVisibility(View.VISIBLE);
                    }
                });
        show();
    }

    public void show(){
        if(dialog!=null){
            dialog.show();
        }
    }

    public void updatePhoto(String url){
        this.url = url;
        if(img_content!=null){
            helper_loading_box.setVisibility(View.VISIBLE);
            helper_noconnection.setVisibility(View.GONE);
            Picasso.with(context)
                    .load(this.url)
                    .error(R.drawable.no_data_available)
//                    .placeholder(R.drawable.loading)
                    .into(img_content, new Callback() {
                        @Override
                        public void onSuccess() {
                            helper_loading_box.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            helper_loading_box.setVisibility(View.GONE);
                            helper_noconnection.setVisibility(View.VISIBLE);
                        }
                    });
        }
    }

    public void dismiss(){
        if(dialog!=null){
            showDegradasi = false;
            dialog.dismiss();
        }
    }

    public void onPickPhoto(OnPickPhoto onPickPhoto){
        this.onPickPhoto = onPickPhoto;
    }

    public interface OnPickPhoto{
        public void onPickPhoto();
    }
}