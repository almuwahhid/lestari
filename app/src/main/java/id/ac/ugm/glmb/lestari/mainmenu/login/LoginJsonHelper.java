package id.ac.ugm.glmb.lestari.mainmenu.login;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.profil.UserModel;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariUtil;


/**
 * Created by root on 3/17/18.
 */

public class LoginJsonHelper {
    public static boolean isUserValid(Context context, JSONObject jsonObject){
        boolean isValid = false;
        try {
            JSONObject data = jsonObject.getJSONObject("data");
            if(data.getString("level").equalsIgnoreCase(context.getResources().getString(R.string.user_notation))){
                UserModel userModel = new UserModel();
                userModel.setId_user(data.getString("id_user"));
                userModel.setUsername(data.getString("username"));
                userModel.setPhoto(data.getString("photo"));
                userModel.setFullname(data.getString("fullname"));
                userModel.setAddress(data.getString("address"));
                userModel.setGender(data.getString("gender"));
                userModel.setPhone(data.getString("phone"));
                userModel.setLatlng(data.getString("latlng"));
                userModel.setEmail(data.getString("email"));
                userModel.setToken_login(data.getString("token_login"));
                userModel.setBirthday(data.getString("birthday"));
                userModel.setHash_id(data.getString("hash_id"));
//                id_prov is null
                userModel.setId_prov(data.getString("id_prov"));
                userModel.setId_kel(data.getString("id_kel"));
                userModel.setId_kab(data.getString("id_kab"));
                userModel.setId_kec(data.getString("id_kec"));
                userModel.setId_kel(data.getString("id_kel"));
                userModel.setNama_prov(data.getString("nama_prov"));
                userModel.setNama_kel(data.getString("nama_kel"));
                userModel.setNama_kab(data.getString("nama_kab"));
                userModel.setNama_kec(data.getString("nama_kec"));

                userModel.setJumlah_degradasi(data.getString("jumlah_degradasi"));
                userModel.setJumlah_kontribusi(data.getString("jumlah_kontribusi"));

                LestariUtil.saveDataUser(context, userModel);
                isValid = true;
            }else{
                Lestari.alertWarning(context, context.getResources().getString(R.string.login_failed));
                isValid = false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return isValid;
    }
}
