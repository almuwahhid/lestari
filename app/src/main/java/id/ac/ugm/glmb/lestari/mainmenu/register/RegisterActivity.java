package id.ac.ugm.glmb.lestari.mainmenu.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogPilihan;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.lestari.mainmenu.syaratKetentuan.DisclaimerActivity;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariParam;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

import static id.ac.ugm.glmb.lestari.utilities.Lestari.pilihanJK;

public class RegisterActivity extends ActivityGeneral implements View.OnClickListener{
    @BindView(R.id.input_email) protected EditText input_email;
    @BindView(R.id.input_username) protected EditText input_username;
    @BindView(R.id.input_fullname) protected EditText input_fullname;
    @BindView(R.id.input_jk) protected EditText input_jk;
    @BindView(R.id.input_password) protected EditText input_password;
    @BindView(R.id.input_password_ulangi) protected EditText input_password_ulangi;
    @BindView(R.id.card_register) protected CardView card_register;
    @BindView(R.id.cb_checklist) protected CheckBox cb_checklist;
    @BindView(R.id.tv_syarat) protected TextView tv_syarat;

    String username, password, fullname, email, gender;

    List<Integer> forms = new ArrayList<>();
    ToolbarLestari toolbarLestari;
    DialogPilihan dialogPilihan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        toolbarLestari = new ToolbarLestari(getContext(), "Register");
        dialogPilihan = new DialogPilihan(getContext(), pilihanJK(), "JENIS KELAMIN");
        dialogPilihan.setOnItemClick(new DialogPilihan.OnItemClick() {
            @Override
            public void onItemClick(View view, PilihanClass pilihanClass, int position) {
                gender = pilihanClass.getId();
                input_jk.setText(pilihanClass.getName());
                input_jk.setError(null);
            }
        });

        setFormsToValidate();

        card_register.setOnClickListener(this);
        input_jk.setOnClickListener(this);
        tv_syarat.setOnClickListener(this);
    }


    private void validateData(){
        if(Lestari.isFormValid(this, getWindow().getDecorView(), forms)){
            if(!Lestari.isValidEmail(input_email.getText().toString().trim())){
                input_email.setError("Email tidak valid");
            }else if(!input_password.getText().toString().equalsIgnoreCase(input_password_ulangi.getText().toString())){
                input_password_ulangi.setError("Password harus sama");
            }else{
                if(cb_checklist.isChecked()){
                    fullname = input_fullname.getText().toString();
                    username = input_username.getText().toString();
                    email = input_email.getText().toString();
                    password = input_password.getText().toString();
                doRegister(LestariParam.stringRegister());
                } else {
                    Lestari.alertWarning(getContext(), "Anda belum menyetujui syarat & ketentuan yang berlaku");
                }
            }
        }
    }

    private void setFormsToValidate(){
        forms.add(R.id.input_email);
        forms.add(R.id.input_username);
        forms.add(R.id.input_fullname);
        forms.add(R.id.input_jk);
        forms.add(R.id.input_password);
        forms.add(R.id.input_password_ulangi);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.card_register:
                validateData();
                break;
            case R.id.input_jk:
                dialogPilihan.show();
                break;
            case R.id.tv_syarat:
                startActivity(new Intent(getContext(), DisclaimerActivity.class));
                break;
        }
    }

    private void doRegister(String URL){
        LestariRequest.POST(URL, RegisterActivity.this, new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                //XUtils.CreateDialog(DetailNegaraActivity.this, R.mipmap.ic_launcher_round);
                //mLoading.setVisibility(View.VISIBLE);
                LestariUi.ShowLoadingDialog(RegisterActivity.this);
            }

            @Override
            public void onSuccess(JSONObject response) {
                LestariUi.HideLoadingDialog(RegisterActivity.this);
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        LestariUi.ToastShort(getContext(), getResources().getString(R.string.register_success_auth));
                        finish();
                    }else{
                        Lestari.alertWarning(getContext(), getResources().getString(R.string.register_failed)+", "+response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                LestariUi.HideLoadingDialog(RegisterActivity.this);
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                //param.put("access_token", SafeTravel.stringPintu());
                param.put("lestari_register_user", ""+true);
                param.put("lestari_username", username);
                param.put("lestari_password", password);
                param.put("lestari_fullname", fullname);
                param.put("lestari_gender", gender);
                param.put("lestari_register_from", "mobile");
                param.put("lestari_level", "user");
                param.put("lestari_email", ""+email);
                param.put("hash_id", "");
                param.put("gotro_email_auth", "0");
//                param.put("type_auth", "1");
                Log.d("asdParams", "requestParam: "+param);
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }
        });
    }
}
