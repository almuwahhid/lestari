package id.ac.ugm.glmb.lestari.utilities;

import id.ac.ugm.glmb.lestari.BuildConfig;

/**
 * Created by root on 6/23/18.
 */

public class LestariConstant {
    public static String sp_notation = "lestari_sp_";
    public static String temporary_image = "temp_image.jpg";
    public static String playstore = "https://play.google.com/store/apps/details?id=id.ac.ugm.glmb.lestari";
//    public static String playstore = "https://play.google.com/store/apps/details?id=id.gotro.gotro";

    public static int CODE_SUCCESS = 1;
    public static int CODE_CONFIRM_ERROR = 5;
    public static int CODE_SESSION_ERROR = 4;
    public static int CODE_FAILED = 2;
    public static int CODE_EMPTY = 3;

    public static String NO_CONNECTION = "1019";

    public static final String APP_ID = BuildConfig.APPLICATION_ID;
    public static final String SP_NOTATION = APP_ID+"_sharedPreference_";
    public static final String SP_LOGIN_STATUS = sp_notation+"loginstatus";

//    User Shared Preference
    public static final String SP_USER_ID = sp_notation+"user_id";
    public static final String SP_USER_USERNAME = sp_notation+"user_username";
    public static final String SP_USER_FULLNAME= sp_notation+"user_fullname";
    public static final String SP_USER_PHOTO = sp_notation+"user_photo";
    public static final String SP_USER_EMAIL = sp_notation+"user_email";
    public static final String SP_USER_ADDRESS = sp_notation+"user_address";
    public static final String SP_USER_GENDER = sp_notation+"user_gender";
    public static final String SP_USER_PHONE = sp_notation+"user_phone";
    public static final String SP_USER_LATLNG = sp_notation+"user_latlng";
    public static final String SP_USER_ID_KEL = sp_notation+"user_id_kel";
    public static final String SP_USER_ID_KEC = sp_notation+"user_id_kec";
    public static final String SP_USER_ID_KAB = sp_notation+"user_id_kab";
    public static final String SP_USER_ID_PROV = sp_notation+"user_id_prov";
    public static final String SP_USER_NAMA_PROV = sp_notation+"user_nama_prov";
    public static final String SP_USER_NAMA_KAB = sp_notation+"user_nama_kab";
    public static final String SP_USER_NAMA_KEC = sp_notation+"user_nama_kec";
    public static final String SP_USER_NAMA_KEL = sp_notation+"user_nama_kel";
    public static final String SP_USER_TOKEN = sp_notation+"user_token";
    public static final String SP_USER_BIRTHDAY = sp_notation+"user_birthday";
    public static final String SP_USER_JML_DEGRADASI = sp_notation+"user_jml_degradasi";
    public static final String SP_USER_JML_KONTRIBUSI = sp_notation+"user_jml_kontribusi";

    public static final String SP_EROSI = sp_notation+"erosi";
    public static final String SP_LONGSOR = sp_notation+"longsor";
    public static final String SP_SEDIMENTASI = sp_notation+"sedimentasi";
    public static final String SP_EUTROFIKASI = sp_notation+"eutrofikasi";
    public static final String SP_SAMPAH = sp_notation+"sampah";
    public static final String SP_LIMBAH = sp_notation+"limbah";
    public static final String SP_BANJIR = sp_notation+"banjir";
    public static final String SP_KEBAKARAN = sp_notation+"kebakaran";
    public static final String SP_PENGENDALIAN = sp_notation+"pengendalian";
    public static final String SP_CITY_FILTER = sp_notation+"city_filter";
    public static final String SP_CITY_FILTER_NAME = sp_notation+"city_filter_name";
    public static final String SP_FILTER_USER = sp_notation+"filter_user";

    public static final String SP_PENGATURAN_NOTIFIKASI = sp_notation+"notification";

    public static final String SP_INTRO_KONTRIBUSI_ADD  = sp_notation+"add_kontribusi";
    public static final String SP_INTRO_KONTRIBUSI_REDIRECT  = sp_notation+"redirect_degradasi";
    public static final String SP_INTRO_DAS  = sp_notation+"intro_das";
    public static final String SP_INTRO = sp_notation+"intro_page";

    public static final String SP_REQ_DAS_INTERNET = sp_notation+"req_das_internet";
    public static final String SP_REQ_DAS = sp_notation+"req_das";
    public static final String SP_FAILED_DAS = sp_notation+"failed_das";
    public static final String SP_NEXT_DAS = sp_notation+"next_das";
    public static final String SP_REQ_PROFILE = sp_notation+"req_profile";
    public static final String SP_UPDATE_PROFILE = sp_notation+"update_profile";

    public static final String EROSI = "erosi";
    public static final String LONGSOR = "longsor";
    public static final String SEDIMENTASI = "sedimentasi";
    public static final String EUTROFIKASI = "eutrofikasi";
    public static final String SAMPAH = "sampah";
    public static final String LIMBAH = "limbah";
    public static final String BANJIR = "banjir";
    public static final String KEBAKARAN = "kebakaran";
    public static final String PENGENDALIAN = "pengendalian";

    public static final String PERCIK = "percik";
    public static final String LEMBAR = "lembar";
    public static final String ALUR = "alur";
    public static final String PARIT = "parit";
    public static final String TEBING = "tebing";
    public static final String PIPA = "pipa";

    public static final String CAIR = "cair";
    public static final String PADAT = "padat";

    public static final String WEBVIEW_STYLE        = "<html> <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'> <body  style=\"text-align:left;background-color: transparent;font-size:15px;color:#616161;padding:8px;font-family:'Roboto'\"> %s </body></Html>";
    public static final String WEBVIEW_STYLE_MATERI        = "<html> <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'> <body  style=\"text-align:left;background-color: transparent;font-size:12px;color:#616161;font-family:'Roboto'\"> %s </body></Html>";

//    Degradasi Shared Preference

}

