package id.ac.ugm.glmb.lestari.coremenu.mainPage.helper;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;

/**
 * Created by root on 6/20/18.
 */

public class DialogFilterHelper {
    public static List<PilihanClass> dataSearch(List<PilihanClass> commonDatas, String search_kata){
        ArrayList<PilihanClass> datas = new ArrayList<>();
        for(PilihanClass commonData : commonDatas){
            if(commonData.getName().toLowerCase().indexOf(search_kata.toLowerCase()) != -1){
                datas.add(commonData);
            }
        }
        return datas;
    }

    public static List<PilihanClass> getWilayah(JSONArray jsonArray){
        List<PilihanClass> pickerModels = new ArrayList<>();
        try {
            for (int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                PilihanClass pickerModel = new PilihanClass();
                pickerModel.setId(jsonObject.getString("kode"));
                pickerModel.setName(jsonObject.getString("nama"));
                pickerModels.add(pickerModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return pickerModels;
    }
}
