package id.ac.ugm.glmb.lestari.mainmenu.login;

import java.io.Serializable;

/**
 * Created by root on 8/4/18.
 */

public class LoginModel implements Serializable {
    public String uId = "", email = "", name = "", photo = "";

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
