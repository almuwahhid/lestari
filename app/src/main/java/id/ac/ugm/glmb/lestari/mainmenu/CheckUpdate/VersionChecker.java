/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018. Shendy Aditya Syamsudin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package id.ac.ugm.glmb.lestari.mainmenu.CheckUpdate;

import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.UncheckedIOException;

import id.ac.ugm.glmb.lestari.utilities.LestariConstant;

/*import org.jsoup.Jsoup;
import org.jsoup.UncheckedIOException;
import org.jsoup.select.Elements;

import java.io.IOException;

import id.gotro.gotro.utilities.GotroConstant;*/


/**
 * Created by root on 3/23/18.
 */

public class VersionChecker extends AsyncTask<String, String, String> {
    public interface OnError{
        public void onError(String s);
    }
    OnError onError;
    String newVersion = "";
    @Override
    protected String doInBackground(String... params) {
        try{
            Elements elements = Jsoup.connect(LestariConstant.playstore)
                    .timeout(5000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .timeout(10*1000)
                    .get()
                    .select("span.htlgb");
//                    .select("span.fuuuckzsasd");
            for(int x = 0;x<elements.size();x++){
                Log.d("checkUpdate", "doInBackground: "+elements.get(x).ownText());
                if(isDataValid(elements.get(x).ownText())){
                    Log.d("checkUpdate", "data: "+elements.get(x).ownText());
                    newVersion = elements.get(x).ownText();
                }
            }
            /*newVersion = Jsoup.connect(Function.GPlay.replace(".dev",""))
                    .timeout(5000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .timeout(10*1000)
                    .get()
                    .select("span.htlgb")
                    .get(3)
                    .ownText();*/
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        } catch (NullPointerException e){
            return "";
        } catch (UncheckedIOException e){
            return "";
        } catch (Exception e){
            return "";
        }
        Log.e("shit", "doInBackground: "+newVersion);
        return newVersion;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.e("shit", "onPostExecute: "+s);
        onError.onError(s);
    }

    public void setOnError(OnError onError){
        this.onError = onError;
    }

    private static String pattern = "^[0-9_.]*$";
    public static boolean isDataValid(String x){
        if(x.matches(pattern)){
            if(x.length()>=3){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}