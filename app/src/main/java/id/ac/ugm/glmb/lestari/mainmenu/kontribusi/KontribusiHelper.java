package id.ac.ugm.glmb.lestari.mainmenu.kontribusi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.degradasi.Degradasi;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;

/**
 * Created by root on 7/27/18.
 */

public class KontribusiHelper {
    public static List<Degradasi> listKontribusi(JSONArray jsonArray){
        List<Degradasi> listKontribusi = new ArrayList<>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                Degradasi degradasi = new Degradasi();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                degradasi.setId(jsonObject.getString("parent"));
                degradasi.setIdjenis(jsonObject.getString("jenis_degradasi"));
                degradasi.setLat(jsonObject.getString("latitude"));
                degradasi.setLng(jsonObject.getString("longitude"));
                degradasi.setJenisLokasi(jsonObject.getString("jenis_lokasi_kejadian"));
                degradasi.setAlamat(jsonObject.getString("alamat"));
                degradasi.setNamajenis(jsonObject.getString("jenis_degradasi"));
                degradasi.setNamaSubJenis(jsonObject.getString("sub_degradasi"));
                degradasi.setDate(jsonObject.getString("date"));
                degradasi.setStatus(jsonObject.getString("status"));
                degradasi.setChild(jsonObject.getString("id_degradasi"));
                listKontribusi.add(degradasi);
            }
        }catch (JSONException e){

        }
        return listKontribusi;
    }

    public static List<PilihanClass> listDegradasi(){
        List<PilihanClass> listDegradasi = new ArrayList<>();
        PilihanClass pilihanClass;
        pilihanClass = new PilihanClass();
        pilihanClass.setId("terbaru");
        pilihanClass.setName("Kontribusi Terbaru");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.EROSI);
        pilihanClass.setName("Erosi");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.LONGSOR);
        pilihanClass.setName("Longsor");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.SEDIMENTASI);
        pilihanClass.setName("Sedimentasi");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.EUTROFIKASI);
        pilihanClass.setName("Eutrofikasi");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.SAMPAH);
        pilihanClass.setName("Sampah");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.LIMBAH);
        pilihanClass.setName("Limbah");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.BANJIR);
        pilihanClass.setName("Banjir");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.KEBAKARAN);
        pilihanClass.setName("Kebakaran");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.PENGENDALIAN);
        pilihanClass.setName("Pengendalian");
        listDegradasi.add(pilihanClass);

        return listDegradasi;
    }
}
