package id.ac.ugm.glmb.lestari.coremenu.mainPage;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogPilihanAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;

public class DialogFilterAdapter extends RecyclerView.Adapter<DialogFilterAdapter.MyHolder> {
    Context context;
    List<PilihanClass> classList;
    OnClickListener itemClickListener;

    public DialogFilterAdapter(Context context, List<PilihanClass> classList) {
        this.context = context;
        this.classList = classList;
    }

    @Override
    public DialogFilterAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_dialog_filter_adapter, parent, false);
        MyHolder rcv = new MyHolder(layoutView);
        return rcv;
    }

    public DialogFilterAdapter() {
    }

    @Override
    public void onBindViewHolder(final DialogFilterAdapter.MyHolder holder, final int position) {
        if(classList.get(position).getState()){
            holder.cb_checklist.setChecked(true);
        } else {
            holder.cb_checklist.setChecked(false);
        }

        holder.cb_checklist.setText(classList.get(position).getName());
        holder.lay_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.cb_checklist.isChecked()){
                    holder.cb_checklist.setChecked(false);
                    itemClickListener.onClick(classList.get(position), position, false);
                } else {
                    holder.cb_checklist.setChecked(true);
                    itemClickListener.onClick(classList.get(position), position, true);
                }
            }
        });


        holder.cb_checklist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.cb_checklist.isChecked()){
                    holder.cb_checklist.setChecked(false);
                    itemClickListener.onClick(classList.get(position), position, false);
                } else {
                    holder.cb_checklist.setChecked(true);
                    itemClickListener.onClick(classList.get(position), position, true);
                }
            }
        });

        holder.cb_checklist.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(holder.cb_checklist.isChecked()){
                    holder.cb_checklist.setChecked(false);
                    itemClickListener.onClick(classList.get(position), position, false);
                } else {
                    holder.cb_checklist.setChecked(true);
                    itemClickListener.onClick(classList.get(position), position, true);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return classList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        RelativeLayout lay_filter;
        CheckBox cb_checklist;
        public MyHolder(View itemView) {
            super(itemView);
            this.setIsRecyclable(false);
            lay_filter = itemView.findViewById(R.id.lay_filter);
            cb_checklist = itemView.findViewById(R.id.cb_checklist);
        }
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.itemClickListener = onClickListener;
    }

    public interface OnClickListener {
        void onClick(PilihanClass pilihanClass, int position, boolean val);
    }
}
