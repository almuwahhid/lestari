package id.ac.ugm.glmb.lestari.utilities;

/**
 * Created by root on 7/20/18.
 */

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.youtube.player.YouTubeBaseActivity;

import id.ac.ugm.glmb.uilibs.R;
import id.ac.ugm.glmb.uilib.SuperUser.RequestHandler;


/**
 * Created by root on 2/26/18.
 */

public class ActivityYoutube extends YouTubeBaseActivity {

    protected Context getContext(){
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.pull_in_right, R.anim.pull_stay);
    }


    @Override
    protected void onPause() {
        overridePendingTransition(R.anim.pull_stay, R.anim.push_out_right);
        super.onPause();
    }


    @Override
    protected void onDestroy() {
        Runtime.getRuntime().gc();
        System.gc();
        super.onDestroy();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                RequestHandler.getInstance().cancelPendingRequest();
                this.onBackPressed();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        RequestHandler.getInstance().cancelPendingRequest();
        super.onBackPressed();
    }
}

