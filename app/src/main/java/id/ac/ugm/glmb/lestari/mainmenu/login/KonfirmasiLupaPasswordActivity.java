package id.ac.ugm.glmb.lestari.mainmenu.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariParam;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

public class KonfirmasiLupaPasswordActivity extends ActivityGeneral implements View.OnClickListener{
    @BindView(R.id.input_password) protected EditText input_password;
    @BindView(R.id.input_password_ulangi) protected EditText input_password_ulangi;
    @BindView(R.id.input_key) protected EditText input_key;
    @BindView(R.id.input_email) protected EditText input_email;
    @BindView(R.id.lay_kirim) protected RelativeLayout lay_kirim;

    List<Integer> formLupaPassword = new ArrayList<>();
    private void addFormLupaPassword() {
        formLupaPassword = new ArrayList<>();
        formLupaPassword.add(R.id.input_key);
        formLupaPassword.add(R.id.input_password);
        formLupaPassword.add(R.id.input_password_ulangi);
        formLupaPassword.add(R.id.input_email);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konfirmasi_lupa_password);
        ButterKnife.bind(this);

        new ToolbarLestari(getContext(),"Konfirmasi Lupa Password");
        addFormLupaPassword();

        lay_kirim.setOnClickListener(this);

    }

    private void validateDate(){
        if (Lestari.isFormValid(getContext(), getWindow().getDecorView(), formLupaPassword)) {
            if(input_password.getText().toString().equalsIgnoreCase(input_password_ulangi.getText().toString())){
                doResetPassword(LestariParam.stringLupaPassword());
            }else {
                input_password.setText("Password tidak sesuai");
                LestariUi.ToastShort(getContext(), "Password tidak sesuai");
            }
        }else {
            LestariUi.ToastShort(getContext(), "Anda belum mengisi semua form yang tersedia");
        }
    }

    private void doResetPassword(String URL){
        LestariRequest.POST(URL, getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                LestariUi.ShowLoadingDialog(getContext());
            }

            @Override
            public void onSuccess(JSONObject response) {
                LestariUi.HideLoadingDialog(getContext());
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        LestariUi.ToastShort(getContext(), "Kode reset sudah terkirim, silahkan cek email Anda");
                        startActivity(new Intent(getContext(), LoginActivity.class));
                        finish();
                    }else if(response.getInt("code")==LestariConstant.CODE_CONFIRM_ERROR){
                        Lestari.alertWarning(getContext(), "Maaf, email Anda tidak terdaftar");
                    }else if(response.getInt("code")== LestariConstant.CODE_EMPTY){
                        Lestari.alertWarning(getContext(), "Maaf, kode yang Anda kirim tidak terdaftar");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                LestariUi.HideLoadingDialog(getContext());
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("lestari_reset_password", "true");
                param.put("lestari_email", input_email.getText().toString());
                param.put("lestari_level", "user");
                param.put("lestari_code", input_key.getText().toString());
                param.put("lestari_new_password", input_password.getText().toString());
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> header_param = new HashMap();
                return header_param;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lay_kirim:
                validateDate();
                break;
        }
    }
}
