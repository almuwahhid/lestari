package id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.sedimentasi;

import java.util.ArrayList;
import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;

/**
 * Created by root on 7/22/18.
 */

public class TambahSedimentasiHelper {
    public static List<PilihanClass> listPosisi(){
        List<PilihanClass> listDegradasi = new ArrayList<>();
        PilihanClass pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.EROSI);
        pilihanClass.setName("Sungai");
        pilihanClass.setId("Sungai");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Danau");
        pilihanClass.setName("Danau");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Waduk");
        pilihanClass.setName("Waduk");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Delta");
        pilihanClass.setName("Delta");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("-");
        pilihanClass.setName("Lainnya");
        listDegradasi.add(pilihanClass);

        return listDegradasi;
    }
}
