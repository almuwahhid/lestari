package id.ac.ugm.glmb.lestari.coremenu.mainPage;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.helper.MenuClass;

public class NavMenuAdapter extends RecyclerView.Adapter<NavMenuAdapter.MyHolder> {

    public NavMenuAdapter() {
    }

    Context context;
    List<MenuClass> menuClasses;
    OnAction onAction;

    public NavMenuAdapter(Context context, List<MenuClass> menuClasses, OnAction onAction) {
        this.context = context;
        this.menuClasses = menuClasses;
        this.onAction = onAction;
    }

    @NonNull
    @Override
    public NavMenuAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.navbar_menu_adapter_main, parent, false);
        MyHolder rcv = new MyHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(@NonNull NavMenuAdapter.MyHolder holder, int position) {
        final MenuClass menuClass = menuClasses.get(position);
        if(menuClass.getAction()){
            holder.card_point.setVisibility(View.VISIBLE);
        }

        holder.tv_point.setText(String.valueOf(menuClass.getTotal()));
        holder.img_menu.setImageResource(menuClass.getDrawable_item());
//        holder.img_menu.setColorFilter(ContextCompat.getColor(context, R.color.primary));
        holder.tv_menu.setText(menuClass.getText());
        if(menuClass.getText().equalsIgnoreCase("logout")){
            holder.tv_menu.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuClass.getOnAction().onAction(context);
                onAction.onAction(context);
            }
        });
    }

    @Override
    public int getItemCount() {
        return menuClasses.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card_point)
        CardView card_point;
        @BindView(R.id.tv_point)
        TextView tv_point;
        @BindView(R.id.tv_menu)
        TextView tv_menu;
        @BindView(R.id.img_menu)
        ImageView img_menu;
        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnAction{
        public void onAction(Context context);
    }
}
