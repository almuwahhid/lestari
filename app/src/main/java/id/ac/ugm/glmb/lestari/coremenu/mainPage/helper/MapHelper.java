package id.ac.ugm.glmb.lestari.coremenu.mainPage.helper;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import id.ac.ugm.glmb.lestari.coremenu.mainPage.das.ListPolyLineClass;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.das.PolyLineClass;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.das.Titik;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.Degradasi;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiClass;
import id.ac.ugm.glmb.lestari.utilities.Haversine;
import id.ac.ugm.glmb.lestari.utilities.Lestari;

/**
 * Created by root on 6/21/18.
 */

public class MapHelper {

    public static PolygonOptions polyLine(JSONArray coordinates){
        PolygonOptions polygonOptions = new PolygonOptions();
        polygonOptions.strokeColor(Color.argb(150,200,0,0));
//        polygonOptions.fillColor(Color.argb(150,0,0,150));
        polygonOptions.strokeWidth(2.0f);

        if(coordinates.length()>0){
            try {
                JSONArray lines = coordinates.getJSONArray(0);
                double[][] coords = new double[lines.length()][];
                for (int i = 0; i < lines.length(); i++) {
                    JSONArray xyJson = lines.getJSONArray(i);
                    coords[i] = new double[xyJson.length()];   // length is always 2
                /*for (int j = 0; j < xyJson.length(); j++) {
                    coords[i][j] = Double.parseDouble(xyJson.getJSONObject(j).toString());
                }*/
                    coords[i][0] = xyJson.optDouble(0);
                    coords[i][1] = xyJson.optDouble(1);
                }

                for (int i = 0; i < coords.length; i++) {
                    polygonOptions.add(new LatLng(coords[i][0], coords[i][1]));
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("error", "polyLine: error line");
                return null;
            }
        } else {
            return null;
        }
        return polygonOptions;
    }

    public static JSONArray loadJSONFromAsset(Context context) {
        String json = null;
        JSONObject jsonObject = null;
        JSONArray coordinates = null;
        try {
            InputStream is = context.getAssets().open("aliransungai.json");

            int size = is.available();

            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

            jsonObject = new JSONObject(json);
            coordinates = jsonObject.getJSONArray("geometries");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return coordinates;
    }

    public static ListPolyLineClass listPolyLine(JSONArray coordinates){
        ListPolyLineClass listPolyLineClass = new ListPolyLineClass();
        try {
            List<PolyLineClass> polyLineClasses = new ArrayList<>();
            for (int i = 0; i < coordinates.length(); i++) {
                JSONObject j1 = coordinates.getJSONObject(i);
                if(j1.getString("type").equalsIgnoreCase("Polygon")){
                    JSONArray j2 = j1.getJSONArray("coordinates");

                    for (int j = 0; j < j2.length(); j++) {
                        JSONArray j3 = j2.getJSONArray(j);
                        String[][] coords = new String[j3.length()][];

                        PolyLineClass polyLineClass = new PolyLineClass();
                        List<Titik> titiks = new ArrayList<>();
                        for (int k = 0; k < j3.length(); k++) {
                            JSONArray xyJson = j3.getJSONArray(k);
                            coords[k] = new String[xyJson.length()];

                            Titik titik = new Titik();
                            titik.setLat(xyJson.getString(1));
                            titik.setLng(xyJson.getString(0));
                            titiks.add(titik);

                        }
                        polyLineClass.setTitiks(titiks);
                        polyLineClasses.add(polyLineClass);
//                        double[] d = new double[j3.length()];

                    }
                }
            }
            listPolyLineClass.setClasses(polyLineClasses);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listPolyLineClass;
    }

    public static List<Degradasi> listDegradasi(JSONObject object){
        List<Degradasi> listDegradasi = new ArrayList<>();
        try {
            JSONArray jsonArray = object.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Degradasi degradasi = new Degradasi();
                degradasi.setNamaSubJenis(jsonObject.getString("sub_degradasi"));
                degradasi.setId(jsonObject.getString("id_degradasi"));
                /*degradasi.setLat(jsonObject.getString("latitude"));
                degradasi.setLng(jsonObject.getString("longitude"));*/
                degradasi.setJenisLokasi(jsonObject.getString("jenis_lokasi_kejadian"));
                degradasi.setLat(jsonObject.getString("latitude"));
                degradasi.setLng(jsonObject.getString("longitude"));
                degradasi.setIdjenis(jsonObject.getString("jenis_degradasi"));
                degradasi.setAlamat(jsonObject.getString("alamat"));
                degradasi.setId_user(jsonObject.getString("id_user"));
                degradasi.setDas(jsonObject.getString("das"));
                listDegradasi.add(degradasi);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return listDegradasi;
    }

    public static String kehilanganTanahAlur(ErosiClass erosiClass){
        double val = 0.0;
        double volume = 0.0;

        try {
            volume = ((0.5)*Double.parseDouble(erosiClass.getLebar())*Double.parseDouble(erosiClass.getKedalaman())) * Double.parseDouble(erosiClass.getPanjang());
            val = volume * Double.parseDouble(erosiClass.getBerat_volume()) / (Double.parseDouble(erosiClass.getPanjang())*Double.parseDouble(erosiClass.getLebar())) * 10;
            Log.d("masukAlur", "volume: "+volume);
            Log.d("masukAlur", "Lebar: "+Double.parseDouble(erosiClass.getLebar()));
            Log.d("masukAlur", "Kedalaman: "+Double.parseDouble(erosiClass.getKedalaman()));
            Log.d("masukAlur", "Panjang: "+Double.parseDouble(erosiClass.getPanjang()));
            Log.d("masukAlur", "Area R: "+Double.parseDouble(erosiClass.getArea_rangkupan()));
            Log.d("masukAlur", "Berat V: "+Double.parseDouble(erosiClass.getBerat_volume()));
            Log.d("masukAlur", "P 2: "+((0.5)*Double.parseDouble(erosiClass.getLebar())*Double.parseDouble(erosiClass.getKedalaman()))
                    *Double.parseDouble(erosiClass.getPanjang())/Double.parseDouble(erosiClass.getArea_rangkupan()));
            Log.d("masukAlur", "result: "+val);
        } catch (NumberFormatException e){
            Log.d("kehilanganTanahAlur", "kehilanganTanahAlur: "+e.getMessage() );
            e.printStackTrace();
            val = 0.0;
        }
        return new DecimalFormat("#.##").format(val);
    }

    public static String kehilanganTanahTebing(ErosiClass erosiClass){
        double val = 0.0;
        try {
            val = Double.parseDouble(erosiClass.getLebar())*Double.parseDouble(erosiClass.getPanjang())*Double.parseDouble(erosiClass.getKedalaman())*Double.parseDouble(erosiClass.getBerat_volume())*10;
        } catch (NumberFormatException e){
            Log.e("kehilanganTanahAlur", "kehilanganTanahAlur: "+e.getMessage() );
            e.printStackTrace();
            val = 0.0;
        }
        return new DecimalFormat("#.##").format(val);
    }

    public static String kehilanganTanahParit(ErosiClass erosiClass){
        int x = 12;
        for (int i = 0; i < x; i++) {

        }

        double val = 0.0;
        try {
            val = ((0.5*(Double.parseDouble(erosiClass.getLebar_atas())+Double.parseDouble(erosiClass.getLebar_bawah()))*Double.parseDouble(erosiClass.getKedalaman()))
                    *Double.parseDouble(erosiClass.getLebar())/Double.parseDouble(erosiClass.getArea_rangkupan()))*Double.parseDouble(erosiClass.getBerat_volume())*10;
        } catch (NumberFormatException e){
            Log.e("kehilanganTanahAlur", "kehilanganTanahAlur: "+e.getMessage() );
            e.printStackTrace();
            val = 0.0;
        }

        return new DecimalFormat("#.##").format(val);
    }

    public static String kehilanganTanahPercik(ErosiClass erosiClass){
        double val = 0.0;
        try {
            val = Double.parseDouble(erosiClass.getKetinggian_pedestial())*Double.parseDouble(erosiClass.getBerat_volume())*10f;
            Log.e("datadata", "kehilanganTanahAlur: "+new DecimalFormat("#.##").format(val) );
            Log.e("datadata", "kehilanganTanahAlur: "+val );
        } catch (NumberFormatException e){
            Log.e("datadata", "kehilanganTanahAlur: "+e.getMessage() );
            e.printStackTrace();
            val = 0.0;
        }

        return new DecimalFormat("#.##").format(val);
    }

    public static String kehilanganTanahLembar(ErosiClass erosiClass){
        double val = 0.0;

        try {
             val = ((Double.parseDouble(erosiClass.getKedalaman())/(Double.parseDouble(erosiClass.getBahan_kasar())/100))-(Double.parseDouble(erosiClass.getKedalaman())))
                     *Double.parseDouble(erosiClass.getBerat_volume())*10;
            Log.d("kehilanganTanahLembar", "P 1: "+((Double.parseDouble(erosiClass.getKedalaman())/(Double.parseDouble(erosiClass.getBahan_kasar())/100))-(Double.parseDouble(erosiClass.getKedalaman()))));
            Log.d("kehilanganTanahLembar", "P 2: "+Double.parseDouble(erosiClass.getBerat_volume())*10);
        } catch (NumberFormatException e){
            Log.d("kehilanganTanahLembar", "P 1: "+(Double.parseDouble(erosiClass.getKedalaman())*0.001)+" -- "+Double.parseDouble(erosiClass.getBahan_kasar())/100+" -- "+(Double.parseDouble(erosiClass.getKedalaman())*0.001)*Double.parseDouble(erosiClass.getBahan_kasar())/100);
            Log.d("kehilanganTanahLembar", "P 2: "+(Double.parseDouble(erosiClass.getKedalaman())*0.001)+" -- "+Double.parseDouble(erosiClass.getBahan_kasar())/100+" -- "+(Double.parseDouble(erosiClass.getKedalaman())*0.001)*Double.parseDouble(erosiClass.getBahan_kasar())/100);
            Log.e("kehilanganTanahLembar", "kehilanganTanahAlur: "+e.getMessage() );
            e.printStackTrace();
            val = 0.0;
        }

        return new DecimalFormat("#.##").format(val);
    }

    public static boolean isZoomable(List<MarkerCluster> markerClusters){
        Log.d("markerCLuster", "isZoomable: "+markerClusters.size());
        for (MarkerCluster m : markerClusters){
            for (MarkerCluster n : markerClusters){
                if(Haversine.distance(m.getLatitude(), m.getLongitude(), n.getLatitude(), n.getLongitude()) > 0.3){
                    return true;
                }
            }
        }
        return false;
    }
}
