package id.ac.ugm.glmb.lestari.coremenu.mainPage.das;

import java.io.Serializable;
import java.util.List;

/**
 * Created by root on 7/31/18.
 */

public class ListPolyLineClass implements Serializable{
    List<PolyLineClass> classes;

    public List<PolyLineClass> getClasses() {
        return classes;
    }

    public void setClasses(List<PolyLineClass> classes) {
        this.classes = classes;
    }
}
