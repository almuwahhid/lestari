package id.ac.ugm.glmb.lestari.utilities;

import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;

import id.ac.ugm.glmb.lestari.mainmenu.profil.UserModel;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

/**
 * Created by root on 6/23/18.
 */

public class LestariUtil {
    private static String TAG = "LestariUtil";
    public static int heigh_bottomsheet_header(Context context, int height){
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

        float densityFloat = displayMetrics.density;
        int pixels = (int) (height * densityFloat + 0.5f); // 264 is height of AppBar Layout
        float heightDp = displayMetrics.heightPixels / densityFloat;
        return pixels;
    }

    public static void emptyUser(Context context, LestariUi.OnEventChange onEventChange){
        FirebaseMessaging.getInstance().unsubscribeFromTopic(Lestari.getSPString(context, LestariConstant.SP_USER_ID));
        Lestari.removeSPString(context, LestariConstant.SP_USER_ADDRESS);
        Lestari.removeSPString(context, LestariConstant.SP_USER_BIRTHDAY);
        Lestari.removeSPString(context, LestariConstant.SP_USER_EMAIL);
        Lestari.removeSPString(context, LestariConstant.SP_USER_FULLNAME);
        Lestari.removeSPString(context, LestariConstant.SP_USER_GENDER);
        Lestari.removeSPString(context, LestariConstant.SP_USER_ID);
        Lestari.removeSPString(context, LestariConstant.SP_USER_ID_KAB);
        Lestari.removeSPString(context, LestariConstant.SP_USER_ID_KEC);
        Lestari.removeSPString(context, LestariConstant.SP_USER_ID_KEL);
        Lestari.removeSPString(context, LestariConstant.SP_USER_ID_PROV);
        Lestari.removeSPString(context, LestariConstant.SP_USER_LATLNG);
        Lestari.removeSPString(context, LestariConstant.SP_USER_NAMA_KAB);
        Lestari.removeSPString(context, LestariConstant.SP_USER_NAMA_KEC);
        Lestari.removeSPString(context, LestariConstant.SP_USER_NAMA_KEL);
        Lestari.removeSPString(context, LestariConstant.SP_USER_NAMA_PROV);
        Lestari.removeSPString(context, LestariConstant.SP_USER_PHONE);
        Lestari.removeSPString(context, LestariConstant.SP_USER_PHOTO);
        Lestari.removeSPString(context, LestariConstant.SP_USER_TOKEN);
        Lestari.removeSPString(context, LestariConstant.SP_USER_USERNAME);
        Lestari.setSPBoolean(context, LestariConstant.SP_LOGIN_STATUS, false);

        if(Lestari.isMyServiceRunning(context, MainService.class)){
            context.stopService(new Intent(context, MainService.class));
        }
        if(onEventChange!=null){
            onEventChange.onChange();
        }
    }

    public static void saveDataUser(Context context, UserModel userModel){
        Lestari.setSPBoolean(context, LestariConstant.SP_LOGIN_STATUS, true);
        Lestari.setSPString(context, LestariConstant.SP_USER_ID, userModel.getId_user());
        Lestari.setSPString(context, LestariConstant.SP_USER_USERNAME, userModel.getUsername());
        Lestari.setSPString(context, LestariConstant.SP_USER_FULLNAME, userModel.getFullname());
        Lestari.setSPString(context, LestariConstant.SP_USER_PHOTO, userModel.getPhoto());
        Lestari.setSPString(context, LestariConstant.SP_USER_EMAIL, userModel.getEmail());
        Lestari.setSPString(context, LestariConstant.SP_USER_ADDRESS, userModel.getAddress());
        Lestari.setSPString(context, LestariConstant.SP_USER_GENDER, userModel.getGender());
        Lestari.setSPString(context, LestariConstant.SP_USER_PHONE, userModel.getPhone());
        Lestari.setSPString(context, LestariConstant.SP_USER_LATLNG, userModel.getLatlng());
        Lestari.setSPString(context, LestariConstant.SP_USER_ID_KAB, userModel.getId_kab());
        Lestari.setSPString(context, LestariConstant.SP_USER_ID_KEC, userModel.getId_kec());
        Lestari.setSPString(context, LestariConstant.SP_USER_ID_KEL, userModel.getId_kel());
        Lestari.setSPString(context, LestariConstant.SP_USER_ID_PROV, userModel.getId_prov());
        Lestari.setSPString(context, LestariConstant.SP_USER_NAMA_KAB, userModel.getNama_kab());
        Lestari.setSPString(context, LestariConstant.SP_USER_NAMA_KEC, userModel.getNama_kec());
        Lestari.setSPString(context, LestariConstant.SP_USER_NAMA_KEL, userModel.getNama_kel());
        Lestari.setSPString(context, LestariConstant.SP_USER_NAMA_PROV, userModel.getNama_prov());
        Lestari.setSPString(context, LestariConstant.SP_USER_TOKEN, userModel.getToken_login());
        Lestari.setSPString(context, LestariConstant.SP_USER_BIRTHDAY, userModel.getBirthday());
        Lestari.setSPString(context, LestariConstant.SP_USER_PHONE, userModel.getPhone());

        Lestari.setSPBoolean(context, LestariConstant.SP_PENGATURAN_NOTIFIKASI, true);

        Lestari.setSPString(context, LestariConstant.SP_USER_JML_DEGRADASI, userModel.getJumlah_degradasi());
        Lestari.setSPString(context, LestariConstant.SP_USER_JML_KONTRIBUSI, userModel.getJumlah_kontribusi());

        Log.d(TAG, "saveDataUser: "+Lestari.getSPString(context, LestariConstant.SP_USER_PHONE));
    }
}
