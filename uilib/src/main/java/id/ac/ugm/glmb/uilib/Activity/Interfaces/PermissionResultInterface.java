package id.ac.ugm.glmb.uilib.Activity.Interfaces;

/**
 * Created by root on 2/26/18.
 */

public interface PermissionResultInterface {
    void permissionGranted();
    void permissionDenied();
}
