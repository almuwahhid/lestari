package id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.sampah;

import java.util.ArrayList;
import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;

/**
 * Created by root on 7/22/18.
 */

public class TambahSampahHelper {
    public static List<PilihanClass> listJenisSampah(){
        List<PilihanClass> listDegradasi = new ArrayList<>();
        PilihanClass pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.EROSI);
        pilihanClass.setName("Rumah Tangga");
        pilihanClass.setId("Rumah Tangga");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Industri");
        pilihanClass.setName("Industri");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("-");
        pilihanClass.setName("Lainnya");
        listDegradasi.add(pilihanClass);

        return listDegradasi;
    }
}
