package id.ac.ugm.glmb.lestari.coremenu.mainPage;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.helper.MarkerCluster;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.Degradasi;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;

public class DialogListAdapter extends RecyclerView.Adapter<DialogListAdapter.MyHolder> {
    Context context;
    List<MarkerCluster> markerClusters;
    OnClickListener itemClickListener;
    Gson gson;

    public DialogListAdapter(Context context, List<MarkerCluster> markerClusters) {
        this.context = context;
        this.markerClusters = markerClusters;
        gson = new Gson();
    }

    public DialogListAdapter() {
    }

    @NonNull
    @Override
    public DialogListAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_dialog_list_adapter, parent, false);
        MyHolder rcv = new MyHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(@NonNull DialogListAdapter.MyHolder holder, final int position) {
        final MarkerCluster markerCluster = markerClusters.get(position);
        Degradasi degradasi = gson.fromJson(markerCluster.getSnippet(), Degradasi.class);

        String jenis = "";
        String subjenis = "";
        try {
            jenis = degradasi.getIdjenis().substring(0, 1).toUpperCase()+degradasi.getIdjenis().substring(1).toUpperCase();
            if(!degradasi.getIdjenis().equalsIgnoreCase("")){
                subjenis = degradasi.getNamaSubJenis().substring(0, 1).toUpperCase()+degradasi.getNamaSubJenis().substring(1).toUpperCase();
            }
        } catch (IndexOutOfBoundsException e) {

        }
        holder.tv_item.setText(jenis+" "+subjenis);
        initIcon(holder.img_degradasi, markerCluster.getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onClick(v, markerCluster, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return markerClusters.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_degradasi)
        ImageView img_degradasi;
        @BindView(R.id.tv_item)
        TextView tv_item;
        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.itemClickListener = onClickListener;
    }

    public interface OnClickListener {
        void onClick(View v, MarkerCluster markerCluster, int position);
    }

    private void initIcon(ImageView imageView, String idjenis){
        switch (idjenis){
            case LestariConstant.EROSI:
                imageView.setImageResource(R.drawable.ic_erosi);
                break;
            case LestariConstant.BANJIR:
                imageView.setImageResource(R.drawable.ic_banjir);
                break;
            case LestariConstant.EUTROFIKASI:
                imageView.setImageResource(R.drawable.ic_eutrofikasi);
                break;
            case LestariConstant.SEDIMENTASI:
                imageView.setImageResource(R.drawable.ic_sedimentasi);
                break;
            case LestariConstant.LONGSOR:
                imageView.setImageResource(R.drawable.ic_longsor);
                break;
            case LestariConstant.LIMBAH:
                imageView.setImageResource(R.drawable.ic_limbah);
                break;
            case LestariConstant.KEBAKARAN:
                imageView.setImageResource(R.drawable.ic_kebakaran);
                break;
            case LestariConstant.SAMPAH:
                imageView.setImageResource(R.drawable.ic_sampah);
                break;
            case LestariConstant.PENGENDALIAN:
                imageView.setImageResource(R.drawable.ic_pengendalian);
                break;
            default:
                imageView.setImageResource(R.drawable.ic_erosi);
                break;
        }
    }
}
