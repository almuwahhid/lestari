package id.ac.ugm.glmb.lestari.mainmenu.degradasi.longsor;

import java.io.Serializable;
import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.degradasi.GaleriDegradasi;

/**
 * Created by root on 7/1/18.
 */

public class LongsorClass implements Serializable{
    String id="", id_user="", time="", date="", parent_id="", deskripsi="", panjang="", tipologi="", lebar="", kedalaman="", name = "";

    public List<GaleriDegradasi> galleries;

    public List<GaleriDegradasi> getGalleries() {
        return galleries;
    }

    public void setGalleries(List<GaleriDegradasi> galleries) {
        this.galleries = galleries;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getPanjang() {
        return panjang;
    }

    public void setPanjang(String panjang) {
        this.panjang = panjang;
    }

    public String getTipologi() {
        return tipologi;
    }

    public void setTipologi(String tipologi) {
        this.tipologi = tipologi;
    }

    public String getLebar() {
        return lebar;
    }

    public void setLebar(String lebar) {
        this.lebar = lebar;
    }

    public String getKedalaman() {
        return kedalaman;
    }

    public void setKedalaman(String kedalaman) {
        this.kedalaman = kedalaman;
    }
}
