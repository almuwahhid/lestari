package id.ac.ugm.glmb.lestari.ClusteringMap;

/**
 * Created by root on 7/23/18.
 */

public interface QuadTreePoint {
    double getLatitude();

    double getLongitude();
}
