package id.ac.ugm.glmb.lestari.utilities;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.GaleriDegradasi;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.uilib.utils.Base64Util;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

import static id.ac.ugm.glmb.lestari.utilities.LestariConstant.SP_NOTATION;

/**
 * Created by root on 7/8/18.
 */

public class Lestari {
    static Dialog dialog_alert;
    static Thread timer;
    public static Intent intentMap;

    public static SharedPreferences sp;
    public static Animation myAnim;

    public static void rediretToMap(Context context, Location location1, Location location2){
        if(location1!=null && location2 !=null){
            intentMap = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?f=d&hl=en&saddr=" + location1.getLatitude() + "," + location1.getLongitude()+ "&daddr=" + location2.getLatitude() + "," + location2.getLongitude()));
            context.startActivity(intentMap);
        }else{
            Lestari.alertWarning(context, context.getResources().getString(R.string.txt_location_not_found));
        }
    }

    public static boolean isFormValid(Context context, View view, List<Integer> forms){
        boolean isValid = true;
        for(int id: forms)
        {
            EditText et = view.findViewById(id);
            if(TextUtils.isEmpty(et.getText().toString()))
            {
                et.setError(context.getResources().getString(R.string.form_empty));
                isValid = false;
            }
        }
        return isValid;
    }

    private static void initAlert(Context context){
        dialog_alert = new Dialog(context);
        dialog_alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_alert.setContentView(R.layout.layout_alert_success);
        dialog_alert.setCanceledOnTouchOutside(true);
        dialog_alert.setCancelable(true);
        dialog_alert.getWindow().getAttributes().windowAnimations = R.style.DialogTopAnimationAlert;
//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        dialog_alert.setCanceledOnTouchOutside(true);
        dialog_alert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog_alert.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog_alert.setCancelable(true);
        Window window = dialog_alert.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        if(timer == null){
            timer = new Thread() {
                public void run() {
                    try {
                        sleep(2000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        dialog_alert.dismiss();
                    }
                }
            };
        }
    }

    public static void alertSuccess(final Context context, String title){
        initAlert(context);
        RelativeLayout lay_alert = dialog_alert.findViewById(R.id.lay_alert);
        ImageView alert_img = dialog_alert.findViewById(R.id.alert_img);
        TextView alert_title = dialog_alert.findViewById(R.id.alert_title);

        lay_alert.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
        alert_img.setImageResource(R.drawable.ic_check_black_24dp);
//        alert_img.setColorFilter(ContextCompat.getColor(context, R.color.blue_400));

        alert_title.setText(title);
        dialog_alert.show();
        timer.start();
    }

    public static void alertWarning(final Context context, String title){
        initAlert(context);
        RelativeLayout lay_alert = dialog_alert.findViewById(R.id.lay_alert);
        ImageView alert_img = dialog_alert.findViewById(R.id.alert_img);
        TextView alert_title = dialog_alert.findViewById(R.id.alert_title);

        lay_alert.setBackgroundColor(ContextCompat.getColor(context, R.color.red_300));
        alert_img.setImageResource(R.drawable.ic_error_outline_black_24dp);
//        alert_img.setColorFilter(ContextCompat.getColor(context, R.color.blue_400));

        alert_title.setText(title);
        dialog_alert.show();
        if (timer.getState() != Thread.State.NEW)
        {
            timer = new Thread() {
                public void run() {
                    try {
                        sleep(2000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        dialog_alert.dismiss();
                    }
                }
            };
            timer.start();
        } else {
            timer.start();
        }
    }

    public static void setSPBoolean(Context context, String constant, Boolean val){
        sp = context.getSharedPreferences(SP_NOTATION, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(constant, val);
        editor.commit();
    }

    public static void setSPString(Context context, String constant, String val){
        sp = context.getSharedPreferences(SP_NOTATION, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(constant, val);
        editor.commit();
    }

    public static String getSPString(Context context, String from) {
        sp = context.getSharedPreferences(SP_NOTATION, 0);
        return sp.getString(from, "");
    }

    public static void removeSPString(Context context, String to) {
        sp = context.getSharedPreferences(SP_NOTATION, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(to);
        editor.commit();
    }

    public static boolean getSPBoolean(Context context, String from) {
        if(context!=null){
            sp = context.getSharedPreferences(SP_NOTATION, 0);
            return sp.getBoolean(from, false);
        }else{
            return false;
        }
    }

    public static boolean getSPBooleanTrue(Context context, String from) {
        if(context!=null){
            sp = context.getSharedPreferences(SP_NOTATION, 0);
            return sp.getBoolean(from, true);
        }else{
            return true;
        }
    }

    public static String convertToBase64(Bitmap bitmap){
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bao);
        byte[] byteArray = bao.toByteArray();
        return Base64Util.encodeBytes(byteArray);
    }



    public static void checkLoginStatus(Context context, LestariUi.OnEventConfirm onEventConfirm){
        if (Lestari.getSPBoolean(context, LestariConstant.SP_LOGIN_STATUS)) {
            onEventConfirm.onEventTrue();
        } else {
            onEventConfirm.onEventFalse();
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static List<PilihanClass> pilihanJK(){
        List<PilihanClass> pilihanJK = new ArrayList<>();
        PilihanClass p = new PilihanClass();
        p.setId("male");
        p.setName("Laki - Laki");
        pilihanJK.add(p);

        p = new PilihanClass();
        p.setId("female");
        p.setName("Perempuan");
        pilihanJK.add(p);

        return pilihanJK;
    }

    public static UCrop ratioCrop(@NonNull UCrop uCrop) {
        //uCrop = uCrop.withAspectRatio(16, 9);
        uCrop = uCrop.withAspectRatio(1, 1);
        return uCrop;
    }

    public static UCrop configCrop(Context context, @NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
        options.setCompressionQuality(60);
        options.setToolbarColor(ContextCompat.getColor(context, R.color.primary));
        //options.setFreeStyleCropEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            options.setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        }
        return uCrop.withOptions(options);
    }

    private static String pattern = "^[a-z0-9_]*$";
    public static boolean isUsernameValid(String x){
        if(x.matches(pattern)){
            if(x.length()>=3){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public static boolean isPhotoDegradasiFilled(List<GaleriDegradasi> galeriDegradasis){
        for (GaleriDegradasi galer : galeriDegradasis){
            if(!galer.getPicture().equalsIgnoreCase(""))
                return true;
        }
        return false;
    }

    public static boolean isPhotoFull(List<GaleriDegradasi> galeriDegradasis){
        for (GaleriDegradasi galer : galeriDegradasis){
            if(galer.getPicture().equalsIgnoreCase(""))
                return false;
        }
        return true;
    }

    public static Bitmap getBitmap(int drawableRes, Activity activity) {
        Drawable drawable = activity.getResources().getDrawable(drawableRes);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static void showIntroCase(Activity activity, View v, String title, String desc, boolean istint, final OnEventChange onEventChange){
        TapTargetView.showFor(activity,                 // `this` is an Activity
                TapTarget.forView(v, title, desc)
                        // All options below are optional
                        .outerCircleColor(R.color.primary)      // Specify a color for the outer circle
                        .outerCircleAlpha(0.96f)            // Specify the alpha amount for the outer circle
                        .targetCircleColor(R.color.white)   // Specify a color for the target circle
                        .titleTextSize(20)                  // Specify the size (in sp) of the title text
                        .titleTextColor(R.color.white)      // Specify the color of the title text
                        .descriptionTextSize(14)            // Specify the size (in sp) of the description text
                        .descriptionTextColor(R.color.white)  // Specify the color of the description text
                        .textColor(R.color.white)            // Specify a color for both the title and description text
                        .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                        .dimColor(R.color.black)            // If set, will dim behind the view with 30% opacity of the given color
                        .drawShadow(true)                   // Whether to draw a drop shadow or not
                        .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                        .tintTarget(istint)                   // Whether to tint the target view's color
                        .transparentTarget(true)           // Specify whether the target is transparent (displays the content underneath)
                        .targetRadius(60),                  // Specify the target radius (in dp)
                new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);      // This call is optional
                        if(onEventChange!=null)
                            onEventChange.onAfterEvent();
                    }
                });
    }

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                android.util.Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        android.util.Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    public static String formattingNumber(String n){
        try {
            double amount = Double.parseDouble(n);
            String data = n.replaceAll("\\.", ",");
            Log.d("nmb", "formattingNumber: "+data);
            DecimalFormat f = new DecimalFormat("#,###,###");
            Log.d("numberingAsd", "formattingNumber: "+f.format(amount));
//            return String.valueOf(f.format(amount));
            return data;
        } catch (NumberFormatException e){
            return "0";
        }
    }


    public interface OnEventChange{
        void onAfterEvent();
    }
}
