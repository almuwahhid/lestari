package id.ac.ugm.glmb.lestari.mainmenu.degradasi;

import java.io.Serializable;

/**
 * Created by root on 6/20/18.
 */

public class Degradasi implements Serializable{
    String id = "", nama = "";
    String lat = "", lng = "";
    String idjenis = "", namajenis = "", alamat = "";
    String idSubJenis = "", namaSubJenis = "";
    String jenisLokasi = "";
    String id_kota = "", nama_kota = "",
    date = "", id_user = "";
    String status = "";
    String child = "";
    String das = "";

    public String getDas() {
        return das;
    }

    public void setDas(String das) {
        this.das = das;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId_kota() {
        return id_kota;
    }

    public void setId_kota(String id_kota) {
        this.id_kota = id_kota;
    }

    public String getNama_kota() {
        return nama_kota;
    }

    public void setNama_kota(String nama_kota) {
        this.nama_kota = nama_kota;
    }

    public String getJenisLokasi() {
        return jenisLokasi;
    }

    public void setJenisLokasi(String jenisLokasi) {
        this.jenisLokasi = jenisLokasi;
    }

    public String getIdSubJenis() {
        return idSubJenis;
    }

    public void setIdSubJenis(String idSubJenis) {
        this.idSubJenis = idSubJenis;
    }

    public String getNamaSubJenis() {
        return namaSubJenis;
    }

    public void setNamaSubJenis(String namaSubJenis) {
        this.namaSubJenis = namaSubJenis;
    }

    public String getNamajenis() {
        return namajenis;
    }

    public void setNamajenis(String namajenis) {
        this.namajenis = namajenis;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getIdjenis() {
        return idjenis;
    }

    public void setIdjenis(String idjenis) {
        this.idjenis = idjenis;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
