package id.ac.ugm.glmb.lestari.mainmenu.lainnya;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.syaratKetentuan.DisclaimerActivity;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;

public class LainnyaActivity extends ActivityGeneral implements View.OnClickListener{

    @BindView(R.id.lay_syarat) protected LinearLayout lay_syarat;
    @BindView(R.id.lay_panduan) protected LinearLayout lay_panduan;
    @BindView(R.id.lay_about) protected LinearLayout lay_about;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lainnya);
        ButterKnife.bind(this);

        new ToolbarLestari(getContext(), "Lainnya");

        lay_about.setOnClickListener(this);
        lay_panduan.setOnClickListener(this);
        lay_syarat.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lay_syarat:
                startActivity(new Intent(getContext(), DisclaimerActivity.class));
                break;
            case R.id.lay_panduan:
                startActivity(new Intent(getContext(), AppIntroActivity.class).putExtra("lainnya", ""));
                break;
            case R.id.lay_about:
                startActivity(new Intent(getContext(), AboutActivity.class));
                break;
        }
    }
}
