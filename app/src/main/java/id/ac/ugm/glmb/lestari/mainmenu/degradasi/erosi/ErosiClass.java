package id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi;

import java.io.Serializable;
import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.degradasi.GaleriDegradasi;

/**
 * Created by root on 6/23/18.
 */

public class ErosiClass implements Serializable{
    String id="",
            id_user="",
            time="",
            date="",
            kehilangan_tanah="",
            ketinggian_pedestial="",
            berat_volume="", kedalaman="",
            bahan_kasar="",
            lebar="",
            panjang="",
            area_rangkupan="",
            lebar_atas="",
            lebar_bawah = "",
            debit_rembesan="",
            parent_id="",
            deskripsi="",
            id_degradasi = "",
            latitude = "",
            longitude = "",
            alamat = "",
            jenis_lokasi_kejadian = "",
            tipologi_erosi = "",
            dampak = "",
            jenis_erosi = "",
            name = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GaleriDegradasi> galleries;

    public List<GaleriDegradasi> getGalleries() {
        return galleries;
    }

    public void setGalleries(List<GaleriDegradasi> galleries) {
        this.galleries = galleries;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getKehilangan_tanah() {
        return kehilangan_tanah;
    }

    public void setKehilangan_tanah(String kehilangan_tanah) {
        this.kehilangan_tanah = kehilangan_tanah;
    }

    public String getKetinggian_pedestial() {
        return ketinggian_pedestial;
    }

    public void setKetinggian_pedestial(String ketinggian_pedestial) {
        this.ketinggian_pedestial = ketinggian_pedestial;
    }

    public String getBerat_volume() {
        return berat_volume;
    }

    public void setBerat_volume(String berat_volume) {
        this.berat_volume = berat_volume;
    }

    public String getKedalaman() {
        return kedalaman;
    }

    public void setKedalaman(String kedalaman) {
        this.kedalaman = kedalaman;
    }

    public String getBahan_kasar() {
        return bahan_kasar;
    }

    public void setBahan_kasar(String bahan_kasar) {
        this.bahan_kasar = bahan_kasar;
    }

    public String getLebar() {
        return lebar;
    }

    public void setLebar(String lebar) {
        this.lebar = lebar;
    }

    public String getPanjang() {
        return panjang;
    }

    public void setPanjang(String panjang) {
        this.panjang = panjang;
    }

    public String getArea_rangkupan() {
        return area_rangkupan;
    }

    public void setArea_rangkupan(String area_rangkupan) {
        this.area_rangkupan = area_rangkupan;
    }

    public String getLebar_atas() {
        return lebar_atas;
    }

    public void setLebar_atas(String lebar_atas) {
        if(lebar_atas == null) {
            this.lebar_atas = "0";
        } else {
            this.lebar_atas = lebar_atas;
        }
    }

    public String getLebar_bawah() {
        return lebar_bawah;
    }

    public void setLebar_bawah(String lebar_bawah) {
        if(lebar_bawah == null) {
            this.lebar_bawah = "0";
        } else {
            this.lebar_bawah = lebar_bawah;
        }
    }

    public String getDebit_rembesan() {
        return debit_rembesan;
    }

    public void setDebit_rembesan(String debit_rembesan) {
        this.debit_rembesan = debit_rembesan;
    }
}
