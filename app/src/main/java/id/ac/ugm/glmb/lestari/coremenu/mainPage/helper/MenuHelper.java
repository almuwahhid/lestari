package id.ac.ugm.glmb.lestari.coremenu.mainPage.helper;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;
import java.util.List;

import id.ac.ugm.glmb.lestari.BuildConfig;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.NavMenuAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.lestari.mainmenu.kontribusi.KontribusiActivity;
import id.ac.ugm.glmb.lestari.mainmenu.lainnya.LainnyaActivity;
import id.ac.ugm.glmb.lestari.mainmenu.login.LoginActivity;
import id.ac.ugm.glmb.lestari.mainmenu.materi.MateriActivity;
import id.ac.ugm.glmb.lestari.mainmenu.profil.ProfilActivity;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariUtil;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

/**
 * Created by root on 6/18/18.
 */

public class MenuHelper {

    public static List<MenuClass> menuDefault(){
        List<MenuClass> menuClasses = new ArrayList<>();
        MenuClass menuClass = new MenuClass(R.drawable.ic_profil, "Profil", false, 0, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(Context context) {
                Intent intent = new Intent(context, ProfilActivity.class);
                context.startActivity(intent);
            }
        });
        menuClasses.add(menuClass);

        /*menuClass = new MenuClass(R.drawable.ic_notifications_none_black_24dp, "Pemberitahuan", false, 0, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(Context context) {
                Intent intent = new Intent(context, PemberitahuanActivity.class);
                context.startActivity(intent);
            }
        });
        menuClasses.add(menuClass);*/

        menuClass = new MenuClass(R.drawable.ic_kontribusi, "Kontribusi Saya", false, 0, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(Context context) {
                Intent intent = new Intent(context, KontribusiActivity.class);
                context.startActivity(intent);
            }
        });
        menuClasses.add(menuClass);

        /*menuClass = new MenuClass(R.drawable.ic_pengendalian, "Pengendalian Degradasi", false, 0, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(Context context) {
                Intent intent = new Intent(context, PengendalianActivity.class);
                context.startActivity(intent);
            }
        });
        menuClasses.add(menuClass);*/

        menuClass = new MenuClass(R.drawable.ic_materi, "Materi", false, 0, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(Context context) {
                Intent intent = new Intent(context, MateriActivity.class);
                context.startActivity(intent);
            }
        });
        menuClasses.add(menuClass);

        menuClass = new MenuClass(R.drawable.ic_lainnya, "Lainnya", false, 0, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(Context context) {
                Intent intent = new Intent(context, LainnyaActivity.class);
                context.startActivity(intent);
            }
        });
        menuClasses.add(menuClass);

        menuClass = new MenuClass(R.drawable.ic_rating, "Rate", false, 0, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(Context context) {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID.replaceAll(".dev", ""))));
            }
        });
        menuClasses.add(menuClass);

        menuClass = new MenuClass(R.drawable.ic_logout, "Logout", false, 0, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(final Context context) {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Logout");
                alert.setMessage("Apakah Anda yakin ingin logout ?");
                alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        LestariUtil.emptyUser(context, new LestariUi.OnEventChange() {
                            @Override
                            public void onChange() {
                                ((Activity)context).finish();
                                context.startActivity(new Intent(context, LoginActivity.class));
                            }
                        });
                    }
                });

                alert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alert.show();
            }
        });
        menuClasses.add(menuClass);

        return menuClasses;
    }

    public static List<MenuClass> menuCostum(int total){
        List<MenuClass> menuClasses = new ArrayList<>();
        MenuClass menuClass = new MenuClass(R.drawable.ic_profil, "Profil", false, 0, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(Context context) {
                Intent intent = new Intent(context, ProfilActivity.class);
                context.startActivity(intent);
            }
        });
        menuClasses.add(menuClass);

        /*if(total==0){
            menuClass = new MenuClass(R.drawable.ic_notifications_none_black_24dp, "Pemberitahuan", false, 0, new NavMenuAdapter.OnAction() {
                @Override
                public void onAction(Context context) {
                    Intent intent = new Intent(context, PemberitahuanActivity.class);
                    context.startActivity(intent);
                }
            });
            menuClasses.add(menuClass);
        } else{
            menuClass = new MenuClass(R.drawable.ic_notifications_none_black_24dp, "Pemberitahuan", true, total, new NavMenuAdapter.OnAction() {
                @Override
                public void onAction(Context context) {
                    Intent intent = new Intent(context, PemberitahuanActivity.class);
                    context.startActivity(intent);
                }
            });
            menuClasses.add(menuClass);
        }*/


        menuClass = new MenuClass(R.drawable.ic_kontribusi, "Kontribusi Saya", false, 0, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(Context context) {
                Intent intent = new Intent(context, KontribusiActivity.class);
                context.startActivity(intent);
            }
        });
        menuClasses.add(menuClass);

        /*menuClass = new MenuClass(R.drawable.ic_pengendalian, "Pengendalian Degradasi", false, 0, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(Context context) {
                Intent intent = new Intent(context, PengendalianActivity.class);
                context.startActivity(intent);
            }
        });
        menuClasses.add(menuClass);*/

        menuClass = new MenuClass(R.drawable.ic_materi, "Materi", false, 0, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(Context context) {
                Intent intent = new Intent(context, MateriActivity.class);
                context.startActivity(intent);
            }
        });
        menuClasses.add(menuClass);

        menuClass = new MenuClass(R.drawable.ic_lainnya, "Lainnya", false, 0, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(Context context) {
                Intent intent = new Intent(context, LainnyaActivity.class);
                context.startActivity(intent);
            }
        });
        menuClasses.add(menuClass);

        menuClass = new MenuClass(R.drawable.ic_rating, "Rate", false, 0, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(Context context) {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID.replaceAll(".dev", ""))));
            }
        });
        menuClasses.add(menuClass);

        menuClass = new MenuClass(R.drawable.ic_logout, "Logout", false, 0, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(Context context) {

            }
        });
        menuClasses.add(menuClass);

        return menuClasses;
    }

    public static List<PilihanClass> listDegradasi(Context context){
        List<PilihanClass> listDegradasi = new ArrayList<>();
        PilihanClass pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.EROSI);
        pilihanClass.setName("Erosi");
        pilihanClass.setState(Lestari.getSPBooleanTrue(context, LestariConstant.SP_EROSI));
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.LONGSOR);
        pilihanClass.setName("Longsor");
        pilihanClass.setState(Lestari.getSPBooleanTrue(context, LestariConstant.SP_LONGSOR));
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.SEDIMENTASI);
        pilihanClass.setName("Sedimentasi");
        pilihanClass.setState(Lestari.getSPBooleanTrue(context, LestariConstant.SP_SEDIMENTASI));
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.EUTROFIKASI);
        pilihanClass.setName("Eutrofikasi");
        pilihanClass.setState(Lestari.getSPBooleanTrue(context, LestariConstant.SP_EUTROFIKASI));
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.SAMPAH);
        pilihanClass.setName("Sampah");
        pilihanClass.setState(Lestari.getSPBooleanTrue(context, LestariConstant.SP_SAMPAH));
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.LIMBAH);
        pilihanClass.setName("Limbah");
        pilihanClass.setState(Lestari.getSPBooleanTrue(context, LestariConstant.SP_LIMBAH));
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.BANJIR);
        pilihanClass.setName("Banjir");
        pilihanClass.setState(Lestari.getSPBooleanTrue(context, LestariConstant.SP_BANJIR));
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.KEBAKARAN);
        pilihanClass.setName("Kebakaran");
        pilihanClass.setState(Lestari.getSPBooleanTrue(context, LestariConstant.SP_KEBAKARAN));
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.PENGENDALIAN);
        pilihanClass.setName("Pengendalian");
        pilihanClass.setState(Lestari.getSPBooleanTrue(context, LestariConstant.SP_PENGENDALIAN));
        listDegradasi.add(pilihanClass);

        return listDegradasi;
    }
}
