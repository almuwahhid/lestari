package id.ac.ugm.glmb.lestari.mainmenu.CheckUpdate;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.BuildConfig;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.MainActivity;
import id.ac.ugm.glmb.lestari.mainmenu.login.LoginActivity;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

public class CheckUpdateActivity extends ActivityGeneral {
    @BindView(R.id.btn_update) protected CardView btn_update;
    @BindView(R.id.img_close) protected ImageView img_close;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_update);

        ButterKnife.bind(this);

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Lestari.checkLoginStatus(getContext(), new LestariUi.OnEventConfirm() {
                    @Override
                    public void onEventTrue() {
                        intent = new Intent(getContext(), MainActivity.class);
                    }

                    @Override
                    public void onEventFalse() {
                        intent = new Intent(getContext(), LoginActivity.class);
                    }
                });

                startActivity(intent);
                finish();
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID.replaceAll(".dev", ""))));
            }
        });
    }
}
