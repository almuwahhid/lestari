package id.ac.ugm.glmb.lestari.mainmenu.profil;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import agency.tango.android.avatarview.AvatarPlaceholder;
import agency.tango.android.avatarview.loader.PicassoLoader;
import agency.tango.android.avatarview.views.AvatarView;
import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogPilihan;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.lestari.mainmenu.login.LoginActivity;
import id.ac.ugm.glmb.lestari.mainmenu.login.LoginJsonHelper;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogPhoto;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariParam;
import id.ac.ugm.glmb.lestari.utilities.LestariUtil;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityPermission;
import id.ac.ugm.glmb.uilib.Activity.Interfaces.PermissionResultInterface;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;
import id.ac.ugm.glmb.uilib.utils.LestariUi;
import id.ac.ugm.glmb.uilib.utils.PermissionKey;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class ProfilActivity extends ActivityPermission implements View.OnClickListener{
    @BindView(R.id.avatarView) protected AvatarView avatarView;
    @BindView(R.id.lay_ubah_password) protected LinearLayout lay_ubah_password;

    @BindView(R.id.input_username) protected EditText input_username;
    @BindView(R.id.input_telp) protected EditText input_telp;
    @BindView(R.id.input_email) protected EditText input_email;
    @BindView(R.id.input_alamat) protected EditText input_alamat;
    @BindView(R.id.input_jk) protected EditText input_jk;
    @BindView(R.id.input_fullname) protected EditText input_fullname;
    @BindView(R.id.tv_name) protected TextView tv_name;
    String base64image = "";
    AvatarPlaceholder refreshableAvatarPlaceholder;
//    @BindView(R.id.appbar) protected AppBarLayout appbar;

    List<Integer> form_user = new ArrayList<>();
    private void addInfoUser() {
        form_user = new ArrayList<>();
        form_user.add(R.id.input_username);
        form_user.add(R.id.input_telp);
        form_user.add(R.id.input_email);
        form_user.add(R.id.input_alamat);
        form_user.add(R.id.input_jk);
        form_user.add(R.id.input_fullname);
    }

    PicassoLoader imageLoader;
    DialogPhoto dialogPhoto;
    UserModel userModel = new UserModel();
    DialogPilihan dialogPilihan;

    String photo_url = "google";
    Bitmap bitmapImage;
    Uri imageUri = null;
    Uri destinationUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        ButterKnife.bind(this);

        ToolbarLestari toolbarLestari = new ToolbarLestari(getContext(), "Profil");
        destinationUri = Uri.fromFile(new File(getCacheDir(), LestariConstant.temporary_image));
        imageLoader = new PicassoLoader();
        addInfoUser();


        avatarView.setOnClickListener(this);
        dialogPilihan = new DialogPilihan(getContext(), Lestari.pilihanJK(), "JENIS KELAMIN");
        dialogPilihan.setOnItemClick(new DialogPilihan.OnItemClick() {
            @Override
            public void onItemClick(View view, PilihanClass pilihanClass, int position) {
                userModel.setGender(pilihanClass.getId());
                input_jk.setText(pilihanClass.getName());
                input_jk.setError(null);
            }
        });

        input_jk.setOnClickListener(this);
        lay_ubah_password.setOnClickListener(this);
        initDataFromSP();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lay_ubah_password:
                startActivity(new Intent(getContext(), UbahPasswordActivity.class));
                break;
            case R.id.input_jk:
                dialogPilihan.show();
                break;
            case R.id.avatarView:
                if(!userModel.getPhoto().equalsIgnoreCase("")){
                    photo_url = userModel.getPhoto().replace("http:", "https:");
                } else {
                    photo_url = "google.com";
                }

                if(dialogPhoto==null) {
                    dialogPhoto = new DialogPhoto(getContext(), photo_url, true, new DialogPhoto.OnPickPhoto() {
                        @Override
                        public void onPickPhoto() {
                            LestariUi.pickImage(getContext(), new LestariUi.OnImagePicker() {
                                @Override
                                public void setOnPhotoPick(View v) {
                                    askCompactPermission(PermissionKey.WRITE_EXTERNAL_STORAGE, new PermissionResultInterface() {
                                        @Override
                                        public void permissionGranted() {
                                            askCompactPermission(PermissionKey.CAMERA, new PermissionResultInterface() {
                                                @Override
                                                public void permissionGranted() {
                                                    EasyImage.openCamera(ProfilActivity.this, 0);
                                                }

                                                @Override
                                                public void permissionDenied() {
                                                    Lestari.alertWarning(getContext(), "Akses kamera ditolak");
                                                }
                                            });
                                        }

                                        @Override
                                        public void permissionDenied() {
                                            Lestari.alertWarning(getContext(), "Permission Denied");
                                        }
                                    });
                                }

                                @Override
                                public void setOnGalleryPick(View v) {
                                    askCompactPermission(PermissionKey.WRITE_EXTERNAL_STORAGE, new PermissionResultInterface() {
                                        @Override
                                        public void permissionGranted() {
                                            EasyImage.openGallery(ProfilActivity.this, 0);
                                        }

                                        @Override
                                        public void permissionDenied() {
                                            //permission denied
                                            //replace with your action
                                            Lestari.alertWarning(getApplicationContext(), "Permission Denied");
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    dialogPhoto.updatePhoto(photo_url.replace("http:", "https:"));
                    dialogPhoto.show();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_save:
                validateData();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initDataFromSP(){
        userModel.setFullname(Lestari.getSPString(this, LestariConstant.SP_USER_FULLNAME));
        userModel.setUsername(Lestari.getSPString(this, LestariConstant.SP_USER_USERNAME));
        userModel.setAddress(Lestari.getSPString(this, LestariConstant.SP_USER_ADDRESS));
        userModel.setPhone(Lestari.getSPString(this, LestariConstant.SP_USER_PHONE));
        userModel.setEmail(Lestari.getSPString(this, LestariConstant.SP_USER_EMAIL));
        userModel.setGender(Lestari.getSPString(this, LestariConstant.SP_USER_GENDER));
        userModel.setPhoto(Lestari.getSPString(this, LestariConstant.SP_USER_PHOTO));

        input_username.setText(userModel.getUsername());
        input_alamat.setText(userModel.getAddress());
        input_fullname.setText(userModel.getFullname());
        tv_name.setText(userModel.getFullname());
        input_email.setText(userModel.getEmail());
        input_telp.setText(userModel.getPhone());

        switch (userModel.getGender()){
            case "male":
                input_jk.setText("Laki - Laki");
                break;
            case "female":
                input_jk.setText("Perempuan");
                break;
            default:
                input_jk.setText("Laki - Laki");
                break;
        }

        refreshableAvatarPlaceholder = new AvatarPlaceholder(userModel.getFullname());
        if(userModel.getPhoto().equalsIgnoreCase("")){
            imageLoader.loadImage(avatarView, refreshableAvatarPlaceholder, "google");
        } else {
            imageLoader.loadImage(avatarView, refreshableAvatarPlaceholder, userModel.getPhoto().replace("http:", "https:"));
        }
    }

    private void doSetUser(String URL){
        LestariRequest.POST(URL, getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                LestariUi.ShowLoadingDialog(getContext());
            }

            @Override
            public void onSuccess(JSONObject response) {
                LestariUi.HideLoadingDialog(getContext());
                try {
                    if(response.getInt("code")==LestariConstant.CODE_SUCCESS){
                        if(LoginJsonHelper.isUserValid(getContext(), response)){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Lestari.alertSuccess(getContext(), "Berhasil edit biodata pengguna");
                                }
                            });

                            initDataFromSP();
                        }else{
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Lestari.alertWarning(getContext(), "Gagal edit biodata pengguna");
                                }
                            });
                        }
                    }else if(response.getInt("code")==LestariConstant.CODE_SESSION_ERROR){
                        LestariUtil.emptyUser(getContext(), new LestariUi.OnEventChange() {
                            @Override
                            public void onChange() {
                                LestariUi.ToastShort(getContext(), getResources().getString(R.string.text_session_alert));
                                finish();
                                startActivity(new Intent(getContext(), LoginActivity.class));
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Lestari.alertWarning(getContext(), getResources().getString(R.string.register_failed)+", "+response.getString("message"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                LestariUi.HideLoadingDialog(getContext());
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("lestari_edit_user", ""+true);
                param.put("lestari_token", Lestari.getSPString(getContext(), LestariConstant.SP_USER_TOKEN));
                param.put("lestari_photo", base64image);
                param.put("lestari_fullname", userModel.getFullname());
                param.put("lestari_address", userModel.getAddress());
                param.put("lestari_phone", userModel.getPhone());
                param.put("lestari_gender", userModel.getGender());
                param.put("lestari_username", userModel.getUsername());
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> header_param = new HashMap<String, String>();
                return header_param;
            }
        });
    }

    private void handelCrop(Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            Log.d("asdEdit", "handelCrop: uri ada");
            try {
                bitmapImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                avatarView.setImageBitmap(bitmapImage);
                if (bitmapImage != null) {
                    base64image = Lestari.convertToBase64(bitmapImage);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Lestari.alertWarning(getContext(), "Gagal terima gambar");
        }
    }

    private void mulaiKropGambar(@NonNull Uri uri) {
        Log.d("asdEdit", "mulaiKropGambar: "+destinationUri);
        UCrop uCrop = UCrop.of(uri, destinationUri);
        uCrop = Lestari.ratioCrop(uCrop);
        uCrop = Lestari.configCrop(this, uCrop);
        uCrop.start(this);
    }

    private void validateData(){
        userModel.setUsername(input_username.getText().toString());
        if (Lestari.isFormValid(getContext(), getWindow().getDecorView(), form_user)) {
            if(Lestari.isUsernameValid(input_username.getText().toString())){
                if(userModel.getUsername().length()<3){
                    input_username.requestFocus();
                    input_username.setError("Wajib lebih dari 3 karakter");
                } else {
                    userModel.setFullname(input_fullname.getText().toString());
                    userModel.setUsername(input_username.getText().toString());
                    userModel.setAddress(input_alamat.getText().toString());
                    userModel.setPhone(input_telp.getText().toString());

                    switch (input_jk.getText().toString()){
                        case "Laki Laki":
                            userModel.setGender("male");
                            break;
                        case "Perempuan":
                            userModel.setGender("female");
                            break;
                    }
                    doSetUser(LestariParam.stringEditUser());
                }

            } else {
                input_username.requestFocus();
                input_username.setError("Karakter tidak boleh terdapat simbol, spasi dan huruf besar");
            }
        } else {
            Lestari.alertWarning(this, "Anda belum melengkapi semua form yang tersedia");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Log.d("asdEdit", "onActivityResult: masuk sini");
            if (requestCode == UCrop.REQUEST_CROP) {
                Log.d("asdEdit", "onActivityResult: crop");
                handelCrop(data);
//                handleCropResult(data);
            }else{
                EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
                    @Override
                    public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                        //Some error handlingn
                        e.printStackTrace();
                    }

                    @Override
                    public void onImagesPicked(List<File> imageFiles, EasyImage.ImageSource source, int type) {
                        Log.d("asdEdit", "onImagesPicked: "+imageFiles);
                        imageUri = Uri.fromFile(imageFiles.get(0));
                        mulaiKropGambar(imageUri);
                    }

                    @Override
                    public void onCanceled(EasyImage.ImageSource source, int type) {
                        //Cancel handling, you might wanna remove taken photo if it was canceled
                        if (source == EasyImage.ImageSource.CAMERA) {
                            File photoFile = EasyImage.lastlyTakenButCanceledPhoto(getContext());
                            if (photoFile != null) photoFile.delete();
                        }
                    }
                });
            }
        }
    }


}