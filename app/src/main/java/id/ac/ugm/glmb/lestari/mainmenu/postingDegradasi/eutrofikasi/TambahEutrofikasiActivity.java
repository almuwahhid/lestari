package id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.eutrofikasi;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.MainActivity;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.Degradasi;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.GaleriDegradasi;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.ListGaleriDegradasi;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.eutrofikasi.EutrofikasiClass;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogLainnya;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogPilihan;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.lestari.mainmenu.login.LoginActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.PostingDegradasiHelper;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.PostingPhotoAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogPhoto;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariParam;
import id.ac.ugm.glmb.lestari.utilities.LestariUtil;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityPermission;
import id.ac.ugm.glmb.uilib.Activity.Interfaces.PermissionResultInterface;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;
import id.ac.ugm.glmb.uilib.utils.LestariUi;
import id.ac.ugm.glmb.uilib.utils.PermissionKey;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class TambahEutrofikasiActivity extends ActivityPermission {

    @BindView(R.id.edt_jenishama) protected EditText edt_jenishama;
    @BindView(R.id.edt_luasterdampak) protected EditText edt_luasterdampak;
    @BindView(R.id.edt_deskripsi) protected EditText edt_deskripsi;
    @BindView(R.id.lay_next) protected LinearLayout lay_next;
    @BindView(R.id.recyclerView) protected RecyclerView recyclerView;

    GaleriDegradasi galeri;
    DialogPhoto dialogPhoto;
    PostingPhotoAdapter adapter;
    List<GaleriDegradasi> galeriDegradasis;
    Degradasi dataDegradasi;
    int photoPosition = 0;
    ListGaleriDegradasi listGaleriDegradasi;

    String photo_url = "google";
    Bitmap bitmapImage;
    Uri imageUri = null;
    Uri destinationUri = null;
    String base64image = "";
    Degradasi degradasi;
    EutrofikasiClass eutrofikasiClass = new EutrofikasiClass();
    boolean isEdit = false;

    List<Integer> form_user = new ArrayList<>();
    private void addInfo() {
        form_user = new ArrayList<>();
        form_user.add(R.id.edt_jenishama);
        form_user.add(R.id.edt_luasterdampak);
        form_user.add(R.id.edt_deskripsi);
    }

    private void initFromEdit(){
        edt_jenishama.setText(eutrofikasiClass.getJenis_hama());
        edt_luasterdampak.setText(eutrofikasiClass.getId());
        edt_deskripsi.setText(eutrofikasiClass.getDeskripsi());
    }

    DialogPilihan dialogPilihan;
    DialogLainnya dialogLainnya;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_eutrofikasi);
        ButterKnife.bind(this);
        destinationUri = Uri.fromFile(new File(getCacheDir(), LestariConstant.temporary_image));
        addInfo();
        ToolbarLestari toolbarLestari = new ToolbarLestari(getContext(), "Eutrofikasi");
        galeriDegradasis = PostingDegradasiHelper.emptyGallery();
        galeri = new GaleriDegradasi();

        eutrofikasiClass.setId_user(Lestari.getSPString(getContext(), LestariConstant.SP_USER_ID));
        if(getIntent().hasExtra("degradasi")){
            dataDegradasi = (Degradasi) getIntent().getSerializableExtra("degradasi");
            eutrofikasiClass.setParent_id(dataDegradasi.getId());
        }

        if(getIntent().hasExtra("eutrofikasi")){
            eutrofikasiClass = (EutrofikasiClass) getIntent().getSerializableExtra("eutrofikasi");
            isEdit = true;
            initFromEdit();
        }

        if(getIntent().hasExtra("gallery")){
            listGaleriDegradasi = (ListGaleriDegradasi) getIntent().getSerializableExtra("gallery");
            galeriDegradasis.clear();
            for (GaleriDegradasi galeriDegradasi : listGaleriDegradasi.getGaleriDegradasis()){
                galeriDegradasis.add(galeriDegradasi);
            }
            galeriDegradasis.add(new GaleriDegradasi());
        }

        adapter = new PostingPhotoAdapter(getContext(), galeriDegradasis, new PostingDegradasiHelper.OnClickGallery(){
            @Override
            public void OnShow(View v, GaleriDegradasi galeriDegradasi, int position) {
                photoPosition = position;
                if(galeriDegradasi.getPicture().equalsIgnoreCase("")){
                    galeri = new GaleriDegradasi();
                    openUploadDialog();
                } else {
                    galeri = galeriDegradasi;
                    dialogPhoto = new DialogPhoto(getContext(), galeriDegradasi.getPicture(), true, new DialogPhoto.OnPickPhoto(){
                        @Override
                        public void onPickPhoto() {
                            openUploadDialog();
                        }
                    });
                }
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(adapter);

        edt_jenishama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initDialogTipologi();
            }
        });

        lay_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateData();
            }
        });
    }

    private void validateData(){
        if (Lestari.isFormValid(getContext(), getWindow().getDecorView(), form_user)) {
            if(Lestari.isPhotoDegradasiFilled(galeriDegradasis)){
                eutrofikasiClass.setLuas_areaterdampak(edt_luasterdampak.getText().toString());
                eutrofikasiClass.setDeskripsi(edt_deskripsi.getText().toString());
                postDegradasi(LestariParam.stringDegradasi(), false);
            } else {
                Lestari.alertWarning(getContext(), getResources().getString(R.string.not_fill));
            }
        } else {
            Lestari.alertWarning(getContext(), getResources().getString(R.string.not_fill));
        }
    }

    private void updateAdapter(GaleriDegradasi galeriDegradasi){
        galeriDegradasis.set(photoPosition, galeriDegradasi);
        if(Lestari.isPhotoFull(galeriDegradasis)){
            GaleriDegradasi g = new GaleriDegradasi();
            galeriDegradasis.add(g);
        }
        adapter.notifyDataSetChanged();
    }

    private void openUploadDialog(){
        LestariUi.pickImage(getContext(), new LestariUi.OnImagePicker() {
            @Override
            public void setOnPhotoPick(View v) {
                askCompactPermission(PermissionKey.WRITE_EXTERNAL_STORAGE, new PermissionResultInterface() {
                    @Override
                    public void permissionGranted() {
                        askCompactPermission(PermissionKey.CAMERA, new PermissionResultInterface() {
                            @Override
                            public void permissionGranted() {
                                EasyImage.openCamera(TambahEutrofikasiActivity.this, 0);
                            }

                            @Override
                            public void permissionDenied() {
                                Lestari.alertWarning(getContext(), "Akses kamera ditolak");
                            }
                        });
                    }

                    @Override
                    public void permissionDenied() {
                        Lestari.alertWarning(getContext(), "Permission Denied");
                    }
                });
            }

            @Override
            public void setOnGalleryPick(View v) {
                askCompactPermission(PermissionKey.WRITE_EXTERNAL_STORAGE, new PermissionResultInterface() {
                    @Override
                    public void permissionGranted() {
                        EasyImage.openGallery(TambahEutrofikasiActivity.this, 0);
                    }

                    @Override
                    public void permissionDenied() {
                        //permission denied
                        //replace with your action
                        Lestari.alertWarning(getApplicationContext(), "Permission Denied");
                    }
                });
            }
        });
    }

    private void postDegradasi(String URL, final boolean fromPhoto){
        LestariRequest.POST(URL, getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                LestariUi.ShowLoadingDialog(getContext());
            }

            @Override
            public void onSuccess(JSONObject response) {
                LestariUi.HideLoadingDialog(getContext());
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        if(fromPhoto){
                            JSONObject data = response.getJSONObject("data");
                            eutrofikasiClass.setId(data.getString("id_degradasi"));
                            postImage(LestariParam.stringGallery());
                        } else {
                            if(isEdit)
                                LestariUi.ToastShort(getContext(), getResources().getString(R.string.text_success_edit_degradasi));
                            else {
                                LestariUi.ToastShort(getContext(), getResources().getString(R.string.text_success_tambah_degradasi));
                                Intent x = new Intent(LestariConstant.SP_REQ_PROFILE);
                                getContext().sendBroadcast(x);
                                startActivity(new Intent(getContext(), MainActivity.class));
                            }
                            finish();
                        }
                    }else if(response.getInt("code")==LestariConstant.CODE_SESSION_ERROR){
                        LestariUtil.emptyUser(getContext(), new LestariUi.OnEventChange() {
                            @Override
                            public void onChange() {
                                LestariUi.ToastShort(getContext(), getResources().getString(R.string.text_session_alert));
                                finish();
                                startActivity(new Intent(getContext(), LoginActivity.class));
                            }
                        });
                    } else {
                        Lestari.alertWarning(getContext(), getResources().getString(R.string.upload_failed)+", "+response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                LestariUi.HideLoadingDialog(getContext());
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("lestari_token_check", Lestari.getSPString(getContext(), LestariConstant.SP_USER_TOKEN));
                param.put("lestari_update", "true");
                param.put("lestari_id", eutrofikasiClass.getId());
                param.put("lestari_parent", dataDegradasi.getId());
                param.put("lestari_id_user", Lestari.getSPString(getContext(), LestariConstant.SP_USER_ID));
                param.put("lestari_deskripsi", eutrofikasiClass.getDeskripsi());
                param.put("lestari_luas_area_terdampak", eutrofikasiClass.getLuas_areaterdampak());
                param.put("lestari_jenis_hama_tanaman", eutrofikasiClass.getJenis_hama());
                param.put("lestari_jenis_degradasi", dataDegradasi.getIdjenis());
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> header_param = new HashMap<String, String>();
                return header_param;
            }
        });
    }

    private void postImage(String URL){
        LestariRequest.POST(URL, getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                LestariUi.ShowLoadingDialog(getContext());
            }

            @Override
            public void onSuccess(JSONObject response) {
                LestariUi.HideLoadingDialog(getContext());
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        updateAdapter(PostingDegradasiHelper.getParsingGaleri(response));
                    }else if(response.getInt("code")==LestariConstant.CODE_SESSION_ERROR){
                        LestariUtil.emptyUser(getContext(), new LestariUi.OnEventChange() {
                            @Override
                            public void onChange() {
                                LestariUi.ToastShort(getContext(), getResources().getString(R.string.text_session_alert));
                                finish();
                                startActivity(new Intent(getContext(), LoginActivity.class));
                            }
                        });
                    } else {
                        Lestari.alertWarning(getContext(), getResources().getString(R.string.upload_failed)+", "+response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                LestariUi.HideLoadingDialog(getContext());
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("lestari_token_check", Lestari.getSPString(getContext(), LestariConstant.SP_USER_TOKEN));
                param.put("lestari_action", "true");
                param.put("lestari_id", galeri.getId_gallery());
                param.put("lestari_id_degradasi", eutrofikasiClass.getId());
                param.put("lestari_gallery_degradasi[]", base64image);
                param.put("lestari_jenis_degradasi", dataDegradasi.getIdjenis());
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> header_param = new HashMap<String, String>();
                return header_param;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Log.d("asdEdit", "onActivityResult: masuk sini");
            if (requestCode == UCrop.REQUEST_CROP) {
                Log.d("asdEdit", "onActivityResult: crop");
                handelCrop(data);
//                handleCropResult(data);
            }else{
                EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
                    @Override
                    public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                        //Some error handlingn
                        e.printStackTrace();
                    }

                    @Override
                    public void onImagesPicked(List<File> imageFiles, EasyImage.ImageSource source, int type) {
                        Log.d("asdEdit", "onImagesPicked: "+imageFiles);
                        imageUri = Uri.fromFile(imageFiles.get(0));
                        mulaiKropGambar(imageUri);
                    }

                    @Override
                    public void onCanceled(EasyImage.ImageSource source, int type) {
                        //Cancel handling, you might wanna remove taken photo if it was canceled
                        if (source == EasyImage.ImageSource.CAMERA) {
                            File photoFile = EasyImage.lastlyTakenButCanceledPhoto(getContext());
                            if (photoFile != null) photoFile.delete();
                        }
                    }
                });
            }
        }
    }

    private void handelCrop(Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            Log.d("asdEdit", "handelCrop: uri ada");
            try {
                bitmapImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                if (bitmapImage != null) {
                    base64image = Lestari.convertToBase64(bitmapImage);
                }
                if(eutrofikasiClass.getId().equalsIgnoreCase("")){
                    postDegradasi(LestariParam.stringDegradasi(), true);
                } else {
                    postImage(LestariParam.stringGallery());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Lestari.alertWarning(getContext(), "Gagal terima gambar");
        }
    }

    private void mulaiKropGambar(@NonNull Uri uri) {
        Log.d("asdEdit", "mulaiKropGambar: "+destinationUri);
        UCrop uCrop = UCrop.of(uri, destinationUri);
        uCrop = Lestari.ratioCrop(uCrop);
        uCrop = Lestari.configCrop(this, uCrop);
        uCrop.start(this);
    }

    private void initDialogLainnya(){
        if(dialogLainnya!=null){
            dialogLainnya.show();
        } else {
            dialogLainnya = new DialogLainnya(getContext(), new DialogLainnya.OnSubmit() {
                @Override
                public void onSubmit(String data) {
                    edt_jenishama.setError(null);
                    edt_jenishama.setText(data);
                    eutrofikasiClass.setJenis_hama(data);
                }
            });
        }
    }

    private void initDialogTipologi(){
        if(dialogPilihan!=null){
            dialogPilihan.show();
        } else {
            dialogPilihan = new DialogPilihan(getContext(), TambahEutrofikasiHelper.listTipeHama(), "Tipe Hama");
            dialogPilihan.setOnItemClick(new DialogPilihan.OnItemClick() {
                @Override
                public void onItemClick(View view, PilihanClass pilihanClass, int position) {
                    if(pilihanClass.getId().equalsIgnoreCase("-")){
                        initDialogLainnya();
                    } else {
                        edt_jenishama.setError(null);
                        edt_jenishama.setText(pilihanClass.getName());
                        eutrofikasiClass.setJenis_hama(pilihanClass.getName());
                    }
                }
            });
            dialogPilihan.show();
        }
    }
}
