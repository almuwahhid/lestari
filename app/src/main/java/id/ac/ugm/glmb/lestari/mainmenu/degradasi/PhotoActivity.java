package id.ac.ugm.glmb.lestari.mainmenu.degradasi;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogSlider.DialogSlider;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.PostingDegradasiHelper;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.PostingPhotoAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogPhoto;

/**
 * Created by root on 7/24/18.
 */

public class PhotoActivity {
    Context context;
    RecyclerView recyclerView;
    PostingPhotoAdapter adapter;
    TextView tv_notfound;
    List<GaleriDegradasi> galeriDegradasis;
    DialogSlider dialogSlider;

    public PhotoActivity(Context context, RecyclerView recyclerView, TextView tv_notfound, List<GaleriDegradasi> galleries) {
        this.context = context;
        this.recyclerView = recyclerView;
        this.tv_notfound = tv_notfound;
        this.galeriDegradasis = galleries;
        initComponent();
    }

    private Context getContext(){
        return this.context;
    }

    private void initComponent(){
        adapter = new PostingPhotoAdapter(getContext(), galeriDegradasis, new PostingDegradasiHelper.OnClickGallery(){
            @Override
            public void OnShow(View v, GaleriDegradasi galeriDegradasi, int position) {

                if(dialogSlider==null){
                    dialogSlider = new DialogSlider(getContext(), galeriDegradasis);
                    dialogSlider.show(position);
                } else {
                    dialogSlider.show(position);
                }
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(adapter);
    }
}
