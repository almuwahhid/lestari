package id.ac.ugm.glmb.lestari.mainmenu.degradasi.banjir;

import java.io.Serializable;
import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.degradasi.GaleriDegradasi;

/**
 * Created by root on 7/14/18.
 */

public class BanjirClass implements Serializable{
    String id="", id_user="", time="", date="", parent_id="", deskripsi="", tanggal_kejadian="", tipe_banjir="", name = "";

    public List<GaleriDegradasi> galleries;

    public List<GaleriDegradasi> getGalleries() {
        return galleries;
    }

    public void setGalleries(List<GaleriDegradasi> galleries) {
        this.galleries = galleries;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTanggal_kejadian() {
        return tanggal_kejadian;
    }

    public void setTanggal_kejadian(String tanggal_kejadian) {
        this.tanggal_kejadian = tanggal_kejadian;
    }

    public String getTipe_banjir() {
        return tipe_banjir;
    }

    public void setTipe_banjir(String tipe_banjir) {
        this.tipe_banjir = tipe_banjir;
    }
}
