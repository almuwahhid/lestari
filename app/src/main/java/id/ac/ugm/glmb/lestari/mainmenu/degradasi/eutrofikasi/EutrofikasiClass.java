package id.ac.ugm.glmb.lestari.mainmenu.degradasi.eutrofikasi;

import java.io.Serializable;
import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.degradasi.GaleriDegradasi;

/**
 * Created by root on 7/1/18.
 */

public class EutrofikasiClass implements Serializable{
    String id="", id_user="", parent_id="", deskripsi="", luas_areaterdampak="", jenis_hama="", date="", name="";

    public List<GaleriDegradasi> galleries;

    public List<GaleriDegradasi> getGalleries() {
        return galleries;
    }

    public void setGalleries(List<GaleriDegradasi> galleries) {
        this.galleries = galleries;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getLuas_areaterdampak() {
        return luas_areaterdampak;
    }

    public void setLuas_areaterdampak(String luas_areaterdampak) {
        this.luas_areaterdampak = luas_areaterdampak;
    }

    public String getJenis_hama() {
        return jenis_hama;
    }

    public void setJenis_hama(String jenis_hama) {
        this.jenis_hama = jenis_hama;
    }
}
