package id.ac.ugm.glmb.lestari.mainmenu.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogKonfirmasiLupaPassword;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariParam;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

public class LupaPasswordActivity extends ActivityGeneral {
    @BindView(R.id.input_email)
    protected EditText input_email;
    @BindView(R.id.tv_disini)
    protected TextView tv_disini;
    @BindView(R.id.lay_kirim)
    protected RelativeLayout lay_kirim;

    DialogKonfirmasiLupaPassword dialogKonfirmasiLupaPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lupa_password);
        ButterKnife.bind(this);
        ToolbarLestari toolbarLestari = new ToolbarLestari(getContext(),"Lupa Password");

        lay_kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateData();
            }
        });
        tv_disini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), KonfirmasiLupaPasswordActivity.class));
            }
        });
    }

    private void validateData(){
        if(input_email.getText().toString().equalsIgnoreCase("")){
            input_email.setError("Email wajib diisi");
        } else {
            if(!Lestari.isValidEmail(input_email.getText().toString().trim())){
                input_email.setError("Email tidak valid");
            } else {
                doSendEmail(LestariParam.stringLupaPassword());
            }
        }
    }

    private void initDialogKonfirmasi(){
        if(dialogKonfirmasiLupaPassword==null){
            dialogKonfirmasiLupaPassword = new DialogKonfirmasiLupaPassword(getContext());
        } else {
            dialogKonfirmasiLupaPassword.show();
        }
    }

    private void doSendEmail(String URL){
        LestariRequest.POST(URL, getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                //XUtils.CreateDialog(DetailNegaraActivity.this, R.mipmap.ic_launcher_round);
                //mLoading.setVisibility(View.VISIBLE);
                LestariUi.ShowLoadingDialog(getContext());
            }

            @Override
            public void onSuccess(JSONObject response) {
                LestariUi.HideLoadingDialog(getContext());
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        Lestari.alertSuccess(getContext(), "Kode reset sudah terkirim, silahkan cek email Anda");
                        initDialogKonfirmasi();
                    }else if(response.getInt("code") == LestariConstant.CODE_CONFIRM_ERROR){
                        Lestari.alertWarning(getContext(), "Maaf, email Anda tidak terdaftar");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                LestariUi.HideLoadingDialog(getContext());
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("lestari_send_email", "true");
                param.put("lestari_email", input_email.getText().toString());
                param.put("lestari_level", "user");
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> header_param = new HashMap();
                return header_param;
            }
        });
    }
}
