package id.ac.ugm.glmb.lestari.mainmenu.degradasi;

import java.io.Serializable;
import java.util.List;

/**
 * Created by root on 7/21/18.
 */

public class ListGaleriDegradasi implements Serializable{
    List<GaleriDegradasi> galeriDegradasis;

    public List<GaleriDegradasi> getGaleriDegradasis() {
        return galeriDegradasis;
    }

    public void setGaleriDegradasis(List<GaleriDegradasi> galeriDegradasis) {
        this.galeriDegradasis = galeriDegradasis;
    }
}
