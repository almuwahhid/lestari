package id.ac.ugm.glmb.lestari.mainmenu.kontribusi;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.helper.DegradasiServiceHelper;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.Degradasi;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.ListGaleriDegradasi;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.banjir.BanjirAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.banjir.BanjirClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiAlurAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiLembarAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiParitAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiPercikAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiPipaAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiTebingSungaiAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.eutrofikasi.EutrofikasiAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.eutrofikasi.EutrofikasiClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.kebakaran.KebakaranAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.kebakaran.KebakaranClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.limbah.LimbahAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.limbah.LimbahClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.longsor.LongsorAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.longsor.LongsorClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.pengendalian.PengendalianAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.pengendalian.PengendalianClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.sampah.SampahAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.sampah.SampahClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.sedimentasi.SedimentasiAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.sedimentasi.SedimentasiClass;
import id.ac.ugm.glmb.lestari.mainmenu.login.LoginActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.banjir.TambahBanjirActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiAlurActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiLembarActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiParitActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiPercikActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiPipaActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiTebingActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.eutrofikasi.TambahEutrofikasiActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.kebakaran.TambahKebakaranActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.limbah.TambahLimbahActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.longsor.TambahLongsorActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.pengendalian.TambahPengendalianActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.sampah.TambahSampahActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.sedimentasi.TambahSedimentasiActivity;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariParam;
import id.ac.ugm.glmb.lestari.utilities.LestariUtil;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

public class DetailKontribusiActivity extends ActivityGeneral {

    @BindView(R.id.helper_loading_box)
    protected RelativeLayout helper_loading_box;
    @BindView(R.id.helper_empty)
    protected RelativeLayout helper_empty;
    @BindView(R.id.helper_noconnection)
    protected RelativeLayout helper_noconnection;

    @BindView(R.id.recyclerView) protected RecyclerView recyclerView;

    Degradasi degradasi;
    ErosiPercikAdapter erosiPercikAdapter;
    ErosiAlurAdapter erosiAlurAdapter;
    ErosiLembarAdapter erosiLembarAdapter;
    ErosiParitAdapter erosiParitAdapter;
    ErosiPipaAdapter erosiPipaAdapter;
    ErosiTebingSungaiAdapter erosiTebingSungaiAdapter;
    SampahAdapter sampahAdapter;
    BanjirAdapter banjirAdapter;
    EutrofikasiAdapter eutrofikasiAdapter;
    KebakaranAdapter kebakaranAdapter;
    LimbahAdapter limbahAdapter;
    LongsorAdapter longsorAdapter;
    SedimentasiAdapter sedimentasiAdapter;
    PengendalianAdapter pengendalianAdapter;

    List<ErosiClass> erosiClasses;
    List<SampahClass> sampahClasses;
    List<BanjirClass> banjirs;
    List<EutrofikasiClass> eutrofikasiClasses;
    List<KebakaranClass> kebakaranClasses;
    List<LimbahClass> limbahClasses;
    List<LongsorClass> longsorClasses;
    List<SedimentasiClass> sedimentasiClasses;
    List<PengendalianClass> pengendalianClasses;

    ListGaleriDegradasi listGaleriDegradasi;
    Intent intentNext;

    boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kontribusi);
        ButterKnife.bind(this);

        initNewList();
        String jenis = "";
        String subjenis = "";

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        if(getIntent().hasExtra("degradasi")){
            Log.d("testing", "onCreate: detail ok");
            degradasi = (Degradasi) getIntent().getSerializableExtra("degradasi");

            try {
                jenis = degradasi.getNamajenis().substring(0, 1).toUpperCase()+degradasi.getNamajenis().substring(1);
                if(!degradasi.getNamajenis().equalsIgnoreCase("")){
                    subjenis = degradasi.getNamaSubJenis().substring(0, 1).toUpperCase()+degradasi.getNamaSubJenis().substring(1);
                }
            } catch (IndexOutOfBoundsException e) {

            }
            ToolbarLestari toolbarLestari = new ToolbarLestari(getContext(),jenis+" "+subjenis);
        }

        doGetListDegradasi(LestariParam.stringDegradasi());
    }

    private void setEmptyAdapter(){
        erosiClasses.clear();
        sampahClasses.clear();
        banjirs.clear();
        eutrofikasiClasses.clear();
        kebakaranClasses.clear();
        limbahClasses.clear();
        longsorClasses.clear();
        sedimentasiClasses.clear();
        pengendalianClasses.clear();

        erosiPercikAdapter.notifyDataSetChanged();
        erosiAlurAdapter.notifyDataSetChanged();
        erosiLembarAdapter.notifyDataSetChanged();
        erosiParitAdapter.notifyDataSetChanged();
        erosiPipaAdapter.notifyDataSetChanged();
        erosiTebingSungaiAdapter.notifyDataSetChanged();
        banjirAdapter.notifyDataSetChanged();
        eutrofikasiAdapter.notifyDataSetChanged();
        kebakaranAdapter.notifyDataSetChanged();
        limbahAdapter.notifyDataSetChanged();
        sampahAdapter.notifyDataSetChanged();
        sedimentasiAdapter.notifyDataSetChanged();
        pengendalianAdapter.notifyDataSetChanged();
    }

    private void initNewList(){
        erosiClasses = new ArrayList<>();
        sampahClasses = new ArrayList<>();
        banjirs = new ArrayList<>();
        eutrofikasiClasses = new ArrayList<>();
        kebakaranClasses = new ArrayList<>();
        limbahClasses = new ArrayList<>();
        longsorClasses = new ArrayList<>();
        sedimentasiClasses = new ArrayList<>();
        pengendalianClasses = new ArrayList<>();

        erosiPercikAdapter = new ErosiPercikAdapter(getContext(), erosiClasses);
        erosiAlurAdapter = new ErosiAlurAdapter(getContext(), erosiClasses);
        erosiLembarAdapter = new ErosiLembarAdapter(getContext(), erosiClasses);
        erosiPipaAdapter = new ErosiPipaAdapter(getContext(), erosiClasses);
        erosiTebingSungaiAdapter = new ErosiTebingSungaiAdapter(getContext(), erosiClasses);
        erosiParitAdapter = new ErosiParitAdapter(getContext(), erosiClasses);
        banjirAdapter = new BanjirAdapter(getContext(), banjirs);

        sampahAdapter = new SampahAdapter(getContext(), sampahClasses);
        eutrofikasiAdapter = new EutrofikasiAdapter(getContext(), eutrofikasiClasses);
        kebakaranAdapter = new KebakaranAdapter(getContext(), kebakaranClasses);
        limbahAdapter = new LimbahAdapter(getContext(), limbahClasses);
        longsorAdapter = new LongsorAdapter(getContext(), longsorClasses);
        sedimentasiAdapter = new SedimentasiAdapter(getContext(), sedimentasiClasses);
        pengendalianAdapter = new PengendalianAdapter(getContext(), pengendalianClasses);
    }

    private void updateTampilan(JSONArray jsonArray){
        Log.d("subjenis", "updateTampilan: "+degradasi.getNamajenis());
        switch (degradasi.getNamajenis()){
            case LestariConstant.EROSI :
                for (ErosiClass erosiClass : DegradasiServiceHelper.listErosi(jsonArray)){
                    erosiClasses.add(erosiClass);
                }
                Log.d("idsubjenis", "updateTampilan: "+degradasi.getNamajenis());
                switch (degradasi.getNamaSubJenis()){
                    case LestariConstant.ALUR:
                        Log.d("erosiclass", "updateTampilan: "+erosiClasses.size());
                        recyclerView.setAdapter(erosiAlurAdapter);
                        erosiAlurAdapter.notifyDataSetChanged();
                        break;
                    case LestariConstant.LEMBAR:
                        recyclerView.setAdapter(erosiLembarAdapter);
                        erosiLembarAdapter.notifyDataSetChanged();
                        break;
                    case LestariConstant.PARIT:
                        recyclerView.setAdapter(erosiParitAdapter);
                        erosiParitAdapter.notifyDataSetChanged();
                        break;
                    case LestariConstant.PERCIK:
                        recyclerView.setAdapter(erosiPercikAdapter);
                        erosiPercikAdapter.notifyDataSetChanged();
                        break;
                    case LestariConstant.PIPA:
                        recyclerView.setAdapter(erosiPipaAdapter);
                        erosiPipaAdapter.notifyDataSetChanged();
                        break;
                    case LestariConstant.TEBING:
                        recyclerView.setAdapter(erosiTebingSungaiAdapter);
                        erosiTebingSungaiAdapter.notifyDataSetChanged();
                        break;
                }
                break;
            case LestariConstant.BANJIR :
                for (BanjirClass banjirClass : DegradasiServiceHelper.listBanjir(jsonArray)){
                    banjirs.add(banjirClass);
                }
                recyclerView.setAdapter(banjirAdapter);
                banjirAdapter.notifyDataSetChanged();
                break;
            case LestariConstant.EUTROFIKASI :
                for (EutrofikasiClass eutrofikasiClass : DegradasiServiceHelper.listEutrofikasi(jsonArray)){
                    eutrofikasiClasses.add(eutrofikasiClass);
                }
                recyclerView.setAdapter(eutrofikasiAdapter);
                eutrofikasiAdapter.notifyDataSetChanged();
                break;
            case LestariConstant.KEBAKARAN :
                for (KebakaranClass kebakaranClass : DegradasiServiceHelper.listKebakaran(jsonArray)){
                    kebakaranClasses.add(kebakaranClass);
                }
                recyclerView.setAdapter(kebakaranAdapter);
                kebakaranAdapter.notifyDataSetChanged();
                break;
            case LestariConstant.LIMBAH :
                for (LimbahClass classs : DegradasiServiceHelper.listLimbah(jsonArray)){
                    limbahClasses.add(classs);
                }
                recyclerView.setAdapter(limbahAdapter);
                limbahAdapter.notifyDataSetChanged();
                break;
            case LestariConstant.LONGSOR :
                for (LongsorClass classs : DegradasiServiceHelper.listLongsor(jsonArray)){
                    longsorClasses.add(classs);
                }
                recyclerView.setAdapter(longsorAdapter);
                longsorAdapter.notifyDataSetChanged();
                break;
            case LestariConstant.SAMPAH :
                for (SampahClass classs : DegradasiServiceHelper.listSampah(jsonArray)){
                    sampahClasses.add(classs);
                }
                recyclerView.setAdapter(sampahAdapter);
                sampahAdapter.notifyDataSetChanged();
                break;
            case LestariConstant.SEDIMENTASI :
                for (SedimentasiClass classs : DegradasiServiceHelper.listSedimentasi(jsonArray)){
                    sedimentasiClasses.add(classs);
                }
                recyclerView.setAdapter(sedimentasiAdapter);
                sedimentasiAdapter.notifyDataSetChanged();
                break;
            case LestariConstant.PENGENDALIAN :
                for (PengendalianClass classs : DegradasiServiceHelper.listPengendalian(jsonArray)){
                    pengendalianClasses.add(classs);
                }
                recyclerView.setAdapter(pengendalianAdapter);
//                sedimentasiAdapter.notifyDataSetChanged();
                break;
        }
    }

    private void doGetListDegradasi(String URL){
        LestariRequest.POST(URL, getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                helper_loading_box.setVisibility(View.VISIBLE);
                helper_empty.setVisibility(View.GONE);
                helper_noconnection.setVisibility(View.GONE);
                setEmptyAdapter();
            }

            @Override
            public void onSuccess(JSONObject response) {
                helper_loading_box.setVisibility(View.GONE);
                helper_empty.setVisibility(View.GONE);
                helper_noconnection.setVisibility(View.GONE);
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        JSONArray data = response.getJSONArray("data");
                        updateTampilan(data);
                    }else if(response.getInt("code")==LestariConstant.CODE_SESSION_ERROR){
                        LestariUtil.emptyUser(getContext(), new LestariUi.OnEventChange() {
                            @Override
                            public void onChange() {
                                LestariUi.ToastShort(getContext(), getResources().getString(R.string.text_session_alert));
                                finish();
                                startActivity(new Intent(getContext(), LoginActivity.class));
                            }
                        });
                    } else {
                        Lestari.alertWarning(getContext(), getResources().getString(R.string.txt_nodata)+", "+response.getString("message"));
                        helper_empty.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                helper_loading_box.setVisibility(View.GONE);
                helper_empty.setVisibility(View.GONE);
                helper_noconnection.setVisibility(View.VISIBLE);
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("lestari_token_check", Lestari.getSPString(getContext(), LestariConstant.SP_USER_TOKEN));
                param.put("lestari_detail", "true");
                param.put("lestari_kontribusi", "true");
                param.put("lestari_id", degradasi.getChild());
                param.put("lestari_jenis_degradasi", degradasi.getNamajenis());
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> header_param = new HashMap<String, String>();
                return header_param;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_kontribusi, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_edit:
                isEdit = true;
                initJenisToEdit();
                break;
            case R.id.action_delete:
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Menghapus Kontribusi");
                alert.setMessage("Apakah Anda yakin ingin menghapus kontribusi ini?");
                alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        deleteKontribusi(LestariParam.stringDegradasi());
                    }
                });

                alert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void initJenisToEdit(){
        switch (degradasi.getNamajenis()){
            case LestariConstant.EROSI:

                switch (degradasi.getNamaSubJenis()){
                    case LestariConstant.TEBING:
                        intentNext = new Intent(getContext(), TambahErosiTebingActivity.class);
                        break;
                    case LestariConstant.PERCIK:
                        intentNext = new Intent(getContext(), TambahErosiPercikActivity.class);
                        break;
                    case LestariConstant.PIPA:
                        intentNext = new Intent(getContext(), TambahErosiPipaActivity.class);
                        break;
                    case LestariConstant.LEMBAR:
                        intentNext = new Intent(getContext(), TambahErosiLembarActivity.class);
                        break;
                    case LestariConstant.ALUR:
                        intentNext = new Intent(getContext(), TambahErosiAlurActivity.class);
                        break;
                    case LestariConstant.PARIT:
                        intentNext = new Intent(getContext(), TambahErosiParitActivity.class);
                        break;
                    default:
                        intentNext = new Intent(getContext(), TambahErosiPercikActivity.class);
                        break;
                }
                if(erosiClasses.size()>0){
                    listGaleriDegradasi = new ListGaleriDegradasi();
                    listGaleriDegradasi.setGaleriDegradasis(erosiClasses.get(0).getGalleries());
                    intentNext.putExtra("erosi", erosiClasses.get(0));
                    intentNext.putExtra("gallery", listGaleriDegradasi);
                }
                break;
            case LestariConstant.BANJIR:
                intentNext = new Intent(getContext(), TambahBanjirActivity.class);
                if(banjirs.size()>0){
                    listGaleriDegradasi = new ListGaleriDegradasi();
                    listGaleriDegradasi.setGaleriDegradasis(banjirs.get(0).getGalleries());
                    intentNext.putExtra("gallery", listGaleriDegradasi);
                    intentNext.putExtra("banjir", banjirs.get(0));
                }
                break;
            case LestariConstant.EUTROFIKASI:
                intentNext = new Intent(getContext(), TambahEutrofikasiActivity.class);
                if(eutrofikasiClasses.size()>0){
                    listGaleriDegradasi = new ListGaleriDegradasi();
                    listGaleriDegradasi.setGaleriDegradasis(eutrofikasiClasses.get(0).getGalleries());
                    intentNext.putExtra("gallery", listGaleriDegradasi);
                    intentNext.putExtra("eutrofikasi", eutrofikasiClasses.get(0));
                }
                break;
            case LestariConstant.SEDIMENTASI:
                intentNext = new Intent(getContext(), TambahSedimentasiActivity.class);
                if(sedimentasiClasses.size()>0){
                    listGaleriDegradasi = new ListGaleriDegradasi();
                    listGaleriDegradasi.setGaleriDegradasis(sedimentasiClasses.get(0).getGalleries());
                    intentNext.putExtra("gallery", listGaleriDegradasi);
                    intentNext.putExtra("sedimentasi", sedimentasiClasses.get(0));
                }
                break;
            case LestariConstant.LONGSOR:
                intentNext = new Intent(getContext(), TambahLongsorActivity.class);
                if(longsorClasses.size()>0){
                    listGaleriDegradasi = new ListGaleriDegradasi();
                    listGaleriDegradasi.setGaleriDegradasis(longsorClasses.get(0).getGalleries());
                    intentNext.putExtra("gallery", listGaleriDegradasi);
                    intentNext.putExtra("longsor", longsorClasses.get(0));
                }
                break;
            case LestariConstant.LIMBAH:
                intentNext = new Intent(getContext(), TambahLimbahActivity.class);
                if(limbahClasses.size()>0){
                    listGaleriDegradasi = new ListGaleriDegradasi();
                    listGaleriDegradasi.setGaleriDegradasis(limbahClasses.get(0).getGalleries());
                    intentNext.putExtra("gallery", listGaleriDegradasi);
                    intentNext.putExtra("limbah", limbahClasses.get(0));
                }
                break;
            case LestariConstant.KEBAKARAN:
                intentNext = new Intent(getContext(), TambahKebakaranActivity.class);
                if(kebakaranClasses.size()>0){
                    listGaleriDegradasi = new ListGaleriDegradasi();
                    listGaleriDegradasi.setGaleriDegradasis(kebakaranClasses.get(0).getGalleries());
                    intentNext.putExtra("gallery", listGaleriDegradasi);
                    intentNext.putExtra("kebakaran", kebakaranClasses.get(0));
                }
                break;
            case LestariConstant.SAMPAH:
                intentNext = new Intent(getContext(), TambahSampahActivity.class);
                if(sampahClasses.size()>0){
                    listGaleriDegradasi = new ListGaleriDegradasi();
                    listGaleriDegradasi.setGaleriDegradasis(sampahClasses.get(0).getGalleries());
                    intentNext.putExtra("gallery", listGaleriDegradasi);
                    intentNext.putExtra("sampah", sampahClasses.get(0));
                }
                break;
            case LestariConstant.PENGENDALIAN:
                intentNext = new Intent(getContext(), TambahPengendalianActivity.class);
                if(pengendalianClasses.size()>0){
                    listGaleriDegradasi = new ListGaleriDegradasi();
                    listGaleriDegradasi.setGaleriDegradasis(pengendalianClasses.get(0).getGalleries());
                    intentNext.putExtra("gallery", listGaleriDegradasi);
                    intentNext.putExtra("pengendalian", pengendalianClasses.get(0));
                }
                break;
            default:
                intentNext = new Intent(getContext(), TambahErosiPercikActivity.class);
                break;
        }
        intentNext.putExtra("degradasi", degradasi);
        startActivity(intentNext);
    }

    private void deleteKontribusi(String URL){
        LestariRequest.POST(URL, getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                LestariUi.ShowLoadingDialog(getContext());
            }

            @Override
            public void onSuccess(JSONObject response) {
                LestariUi.HideLoadingDialog(getContext());
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        LestariUi.ToastShort(getContext(), getResources().getString(R.string.txt_success_delete));
                        finish();
                    }else {
                        Lestari.alertWarning(getContext(), getResources().getString(R.string.txt_error));
//                        LestariUi.ToastShort(LoginActivity.this, getResources().getString(R.string.login_failed));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                LestariUi.HideLoadingDialog(getContext());
                Lestari.alertWarning(getContext(), getResources().getString(R.string.txt_error_delete));
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
//                param.put("lestari_all", "true");
                param.put("lestari_token_check", Lestari.getSPString(getContext(), LestariConstant.SP_USER_TOKEN));

                param.put("lestari_delete", "true");
                param.put("lestari_id", ""+degradasi.getId());
                param.put("lestari_jenis_degradasi", ""+degradasi.getNamajenis());
                System.out.println(param);
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isEdit){
            isEdit = false;
            doGetListDegradasi(LestariParam.stringDegradasi());
        }
    }
}
