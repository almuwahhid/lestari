package id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.helper.DialogFilterHelper;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.Degradasi;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogLainnya;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogPilihan;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.banjir.TambahBanjirActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiAlurActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiLembarActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiParitActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiPercikActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiPipaActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiTebingActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.eutrofikasi.TambahEutrofikasiActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.kebakaran.TambahKebakaranActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.limbah.TambahLimbahActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.longsor.TambahLongsorActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.pengendalian.TambahPengendalianActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.sampah.TambahSampahActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.sedimentasi.TambahSedimentasiActivity;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariParam;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

public class TambahDegradasiActivity extends ActivityGeneral implements OnMapReadyCallback, View.OnClickListener {
    @BindView(R.id.edt_jenisd) protected EditText edt_jenisd;
    @BindView(R.id.edt_subdegradasi) protected EditText edt_subdegradasi;
    @BindView(R.id.edt_titiklokasi) protected EditText edt_titiklokasi;
    @BindView(R.id.edt_alamat) protected EditText edt_alamat;
    @BindView(R.id.edt_das) protected EditText edt_das;
    @BindView(R.id.edt_kota) protected EditText edt_kota;
    @BindView(R.id.edt_jenislokasi) protected EditText edt_jenislokasi;
    @BindView(R.id.lay_subdegradasi) protected LinearLayout lay_subdegradasi;
    @BindView(R.id.lay_next) protected LinearLayout lay_next;

    private Timer timer = new Timer();
    private long DELAY = 1000; // Milliseconds

    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    DialogPilihan dialogPilihan, dialogCity;
    Degradasi dataDegradasi;
    private static final int PLACE_PICKER_REQUEST = 1809;
    MarkerOptions markerOptions;

    Marker mCurrLocationMarker;

    List<PilihanClass> pilihanClasses, cityClasses;
    List<Integer> forms;

    LatLng mylatlng;

    DialogLainnya dialogLainnya;

    Intent intentNext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_degradasi);
        ButterKnife.bind(this);
        dataDegradasi = new Degradasi();

        ToolbarLestari toolbarLestari = new ToolbarLestari(getContext(), "Tambah Degradasi");
        pilihanClasses = new ArrayList<>();
        cityClasses = new ArrayList<>();

        setFormsToValidate();
        setMap();

        dialogPilihan = new DialogPilihan(getContext(), pilihanClasses, "Jenis Degradasi");


        lay_next.setOnClickListener(this);
        edt_kota.setOnClickListener(this);
        edt_titiklokasi.setOnClickListener(this);
        edt_jenisd.setOnClickListener(this);
        edt_subdegradasi.setOnClickListener(this);
        edt_jenislokasi.setOnClickListener(this);
    }

    private void setMap() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.animateCamera(CameraUpdateFactory.zoomTo(8));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(-3.2956551, 105.6122322)));

        initLocationNow();

        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                locationChanged(mMap.getCameraPosition().target);
                edt_titiklokasi.setText(""+ mCurrLocationMarker.getPosition().latitude+", "+mCurrLocationMarker.getPosition().longitude);
                dataDegradasi.setLat(String.valueOf(mCurrLocationMarker.getPosition().latitude));
                dataDegradasi.setLng(String.valueOf(mCurrLocationMarker.getPosition().longitude));
                timer.cancel();
                timer = new Timer();
                timer.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        edt_alamat.setError(null);
                                        edt_alamat.setText(getAddress(new LatLng(mCurrLocationMarker.getPosition().latitude, mCurrLocationMarker.getPosition().longitude)));
                                    }
                                });
                            }
                        },
                        DELAY
                );
            }
        });
    }

    private String getAddress(LatLng latlng) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            Address obj = geocoder.getFromLocation(latlng.latitude, latlng.longitude, 1).get(0);
            String add = obj.getAddressLine(0);

            Log.v("IGA", "Address$add");
            if (add.contains("-")) add = add.split("-")[0];
            return add;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException e) {

        }
        return "";
    }

    private void locationChanged(LatLng latlng){
        if(mCurrLocationMarker != null){
            mCurrLocationMarker.remove();
        }

        mCurrLocationMarker = mMap.addMarker(new MarkerOptions().title("Lokasi Anda").position(latlng).icon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_pin)));
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId){
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
         vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);

    }

    private void validateDegradasi(){
        if (Lestari.isFormValid(getContext(), getWindow().getDecorView(), forms)) {
            dataDegradasi.setAlamat(edt_alamat.getText().toString());
            dataDegradasi.setDas(edt_das.getText().toString());
            addDegradasi(LestariParam.stringDegradasi());
        } else {
            Lestari.alertWarning(getContext(), getResources().getString(R.string.not_fill));
        }
    }

    private void initLocationNow() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationServices.getFusedLocationProviderClient(getContext()).getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                mylatlng = new LatLng(location.getLatitude(), location.getLongitude());
                onFocusMap(mylatlng.latitude, mylatlng.longitude, 18);
            }
        });
    }

    private void onFocusMap(Double lat, Double lng, int x) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), Float.valueOf(x)));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lay_next:
                validateDegradasi();
                break;
            case R.id.edt_jenislokasi:
                dialogPilihan.setOnItemClick(new DialogPilihan.OnItemClick() {
                    @Override
                    public void onItemClick(View view, PilihanClass pilihanClass, int position) {
                        edt_jenislokasi.setError(null);

                        if(pilihanClass.getId().equalsIgnoreCase("-")){
                            initDialogLainnya();
                        } else {
                            edt_jenislokasi.setText(pilihanClass.getName());
                            dataDegradasi.setJenisLokasi(pilihanClass.getId());
                        }
                    }
                });

                dialogPilihan.updateList(PostingDegradasiHelper.listLokasiKejadian(getContext()));
                dialogPilihan.setTitle("Jenis Lokasi Pengamatan");
                dialogPilihan.show();
                break;
            case R.id.edt_titiklokasi:
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(TambahDegradasiActivity.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.edt_jenisd:
                dialogPilihan.setOnItemClick(new DialogPilihan.OnItemClick() {
                    @Override
                    public void onItemClick(View view, PilihanClass pilihanClass, int position) {
                        if(pilihanClass.getId().equalsIgnoreCase(LestariConstant.EROSI) || pilihanClass.getId().equalsIgnoreCase(LestariConstant.LIMBAH)){
                            lay_subdegradasi.setVisibility(View.VISIBLE);
                        } else {
                            lay_subdegradasi.setVisibility(View.GONE);
                        }

                        edt_jenisd.setError(null);
                        edt_jenisd.setText(pilihanClass.getName());
                        dataDegradasi.setIdjenis(pilihanClass.getId());
                        dataDegradasi.setNamajenis(pilihanClass.getName());

                        edt_subdegradasi.setText("");
                        dataDegradasi.setNamaSubJenis("");
                        dataDegradasi.setIdSubJenis("");
                        setFormsToValidate();
                    }
                });

                dialogPilihan.updateList(PostingDegradasiHelper.listDegradasi());
                dialogPilihan.setTitle("Jenis Degradasi");
                dialogPilihan.show();
                break;
            case R.id.edt_subdegradasi:
                if(dataDegradasi.getIdjenis().equalsIgnoreCase("")){
                    edt_jenisd.setError("Pilih jenis degradasi terlebih dahulu");
                } else {
                    if(dataDegradasi.getIdjenis().equalsIgnoreCase(LestariConstant.EROSI)){
                        dialogPilihan.updateList(PostingDegradasiHelper.listErosi());
                    } else if(dataDegradasi.getIdjenis().equalsIgnoreCase(LestariConstant.LIMBAH)){
                        dialogPilihan.updateList(PostingDegradasiHelper.listLimbah());
                    }
                    dialogPilihan.setOnItemClick(new DialogPilihan.OnItemClick() {
                        @Override
                        public void onItemClick(View view, PilihanClass pilihanClass, int position) {
                            edt_subdegradasi.setError(null);
                            edt_subdegradasi.setText(pilihanClass.getName());
                            dataDegradasi.setIdSubJenis(pilihanClass.getId());
                            dataDegradasi.setNamaSubJenis(pilihanClass.getName());
                        }
                    });

                    dialogPilihan.setTitle("Jenis "+dataDegradasi.getNamajenis());
                    dialogPilihan.show();
                }
                break;
                case R.id.edt_kota:
                    initDialogCity();
//
                break;
        }
    }

    private void setFormsToValidate(){
        forms = new ArrayList<>();
        forms.add(R.id.edt_jenisd);
        forms.add(R.id.edt_alamat);
        forms.add(R.id.edt_titiklokasi);
        forms.add(R.id.edt_kota);
        forms.add(R.id.edt_jenislokasi);
        if(dataDegradasi.getIdjenis().equalsIgnoreCase(LestariConstant.EROSI) || dataDegradasi.getIdjenis().equalsIgnoreCase(LestariConstant.LIMBAH)){
            forms.add(R.id.edt_subdegradasi);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Log.d("asdEdit", "onActivityResult: masuk sini");
            if(requestCode == PLACE_PICKER_REQUEST){
                Place place = PlacePicker.getPlace(getContext(), data);
                dataDegradasi.setLat(String.valueOf(place.getLatLng().latitude));
                dataDegradasi.setLng(String.valueOf(place.getLatLng().longitude));
                edt_alamat.setError(null);
                edt_alamat.setText(place.getAddress());
                edt_titiklokasi.setText(dataDegradasi.getLat()+", "+dataDegradasi.getLng());
                setMarker(Double.valueOf(dataDegradasi.getLat()), Double.valueOf(dataDegradasi.getLng()));
            }
        }
    }

    private void setMarker(Double lat, Double lng){
        mMap.clear();
        LatLng lokasi_tujuan = new LatLng(lat, lng);
        if(markerOptions==null){
            markerOptions = new MarkerOptions();
            markerOptions.position(lokasi_tujuan).title(dataDegradasi.getNamajenis()+" "+dataDegradasi.getNamaSubJenis());
            mMap.addMarker(markerOptions);
            mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
        } else {
            markerOptions.position(lokasi_tujuan).title(dataDegradasi.getNamajenis()+" "+dataDegradasi.getNamaSubJenis());
            mMap.addMarker(markerOptions);
            mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(lat, lng)));
    }

    private void initJenisToNext(){
        switch (dataDegradasi.getIdjenis()){
            case LestariConstant.EROSI:
                switch (dataDegradasi.getIdSubJenis()){
                    case LestariConstant.TEBING:
                        intentNext = new Intent(getContext(), TambahErosiTebingActivity.class);
                        break;
                    case LestariConstant.PERCIK:
                        intentNext = new Intent(getContext(), TambahErosiPercikActivity.class);
                        break;
                    case LestariConstant.PIPA:
                        intentNext = new Intent(getContext(), TambahErosiPipaActivity.class);
                        break;
                    case LestariConstant.LEMBAR:
                        intentNext = new Intent(getContext(), TambahErosiLembarActivity.class);
                        break;
                    case LestariConstant.ALUR:
                        intentNext = new Intent(getContext(), TambahErosiAlurActivity.class);
                        break;
                    case LestariConstant.PARIT:
                        intentNext = new Intent(getContext(), TambahErosiParitActivity.class);
                        break;
                    default:
                        intentNext = new Intent(getContext(), TambahErosiPercikActivity.class);
                        break;
                }
                break;
            case LestariConstant.BANJIR:
                intentNext = new Intent(getContext(), TambahBanjirActivity.class);
                break;
            case LestariConstant.EUTROFIKASI:
                intentNext = new Intent(getContext(), TambahEutrofikasiActivity.class);
                break;
            case LestariConstant.SEDIMENTASI:
                intentNext = new Intent(getContext(), TambahSedimentasiActivity.class);
                break;
            case LestariConstant.LONGSOR:
                intentNext = new Intent(getContext(), TambahLongsorActivity.class);
                break;
            case LestariConstant.LIMBAH:
                intentNext = new Intent(getContext(), TambahLimbahActivity.class);
                break;
            case LestariConstant.KEBAKARAN:
                intentNext = new Intent(getContext(), TambahKebakaranActivity.class);
                break;
            case LestariConstant.SAMPAH:
                intentNext = new Intent(getContext(), TambahSampahActivity.class);
                break;
            case LestariConstant.PENGENDALIAN:
                intentNext = new Intent(getContext(), TambahPengendalianActivity.class);
                break;
            default:
                intentNext = new Intent(getContext(), TambahErosiPercikActivity.class);
                break;
        }
        intentNext.putExtra("degradasi", dataDegradasi);
        startActivity(intentNext);
        finish();
    }

    private void initDialogLainnya(){
        if(dialogLainnya!=null){
            dialogLainnya.show();
        } else {
            dialogLainnya = new DialogLainnya(getContext(), new DialogLainnya.OnSubmit() {
                @Override
                public void onSubmit(String data) {
                    edt_jenislokasi.setText(data);
                    dataDegradasi.setJenisLokasi(data);
                }
            });
        }
    }

    private void addDegradasi(String URL){
        LestariRequest.POST(URL, getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                //XUtils.CreateDialog(DetailNegaraActivity.this, R.mipmap.ic_launcher_round);
                //mLoading.setVisibility(View.VISIBLE);
                LestariUi.ShowLoadingDialog(getContext());
            }

            @Override
            public void onSuccess(JSONObject response) {
                LestariUi.HideLoadingDialog(getContext());
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        JSONObject data = response.getJSONObject("data");
                        dataDegradasi.setId(data.getString("id_degradasi"));
                        initJenisToNext();
                    }else{
                        Lestari.alertWarning(getContext(), getResources().getString(R.string.upload_failed)+", "+response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                LestariUi.HideLoadingDialog(getContext());
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                //param.put("access_token", SafeTravel.stringPintu());
                param.put("lestari_token_check", Lestari.getSPString(getContext(), LestariConstant.SP_USER_TOKEN));
                param.put("lestari_jenis_degradasi", dataDegradasi.getIdjenis());
                param.put("lestari_jenis_lokasi_kejadian", dataDegradasi.getJenisLokasi());
                param.put("lestari_latitude", dataDegradasi.getLat());
                param.put("lestari_longitude", dataDegradasi.getLng());
                param.put("lestari_alamat", dataDegradasi.getAlamat());
                param.put("lestari_das", dataDegradasi.getDas());
                param.put("lestari_update", "true");
                param.put("lestari_id_user", Lestari.getSPString(getContext(), LestariConstant.SP_USER_ID));
                param.put("lestari_id", dataDegradasi.getId());
                param.put("lestari_id_kota", dataDegradasi.getId_kota());

                if(dataDegradasi.getIdjenis().equalsIgnoreCase(LestariConstant.EROSI)){
                    param.put("lestari_tipologi_erosi", dataDegradasi.getIdSubJenis());
                    param.put("lestari_jenis_erosi", dataDegradasi.getIdSubJenis());
                } else if(dataDegradasi.getIdSubJenis().equalsIgnoreCase(LestariConstant.CAIR)){
                    param.put("lestari_jenis_limbah", dataDegradasi.getIdSubJenis());
                }

                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }
        });
    }

    private void initDialogCity(){
        LestariRequest.POST(LestariParam.stringCity(), getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                LestariUi.ShowLoadingDialog(getContext());
            }

            @Override
            public void onSuccess(JSONObject response) {
                LestariUi.HideLoadingDialog(getContext());
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
//                        dialogCity = new DialogPilihan(getContext(), , "Pilihan Kota", true);
                        dialogCity = new DialogPilihan(getContext(), DialogFilterHelper.getWilayah(response.getJSONArray("data")), "Kota", true);
                        dialogCity.setOnItemClick(new DialogPilihan.OnItemClick() {
                            @Override
                            public void onItemClick(View view, PilihanClass pilihanClass, int position) {
                                edt_kota.setError(null);
                                edt_kota.setText(pilihanClass.getName());
                                dataDegradasi.setId_kota(pilihanClass.getId());
                                dataDegradasi.setNama_kota(pilihanClass.getName());
                            }
                        });
                        dialogCity.show();

                        DialogFilterHelper.getWilayah(response.getJSONArray("data"));
                    }else if(response.getInt("code")==LestariConstant.CODE_FAILED){
                        Lestari.alertWarning(getContext(), "Ada yang bermasalah");
                    }else {
                        Lestari.alertWarning(getContext(), getResources().getString(R.string.txt_error));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                LestariUi.HideLoadingDialog(getContext());
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("lestari_token_check", Lestari.getSPString(getContext(), LestariConstant.SP_USER_TOKEN));
                System.out.println(param);
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }
        });
    }
}