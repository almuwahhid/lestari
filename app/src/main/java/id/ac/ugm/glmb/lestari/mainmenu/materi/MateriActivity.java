package id.ac.ugm.glmb.lestari.mainmenu.materi;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariParam;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;

public class MateriActivity extends ActivityGeneral {
    ToolbarLestari toolbarLestari;
    @BindView(R.id.recyclerView) protected RecyclerView recyclerView;

    @BindView(R.id.helper_loading_box)
    protected RelativeLayout helper_loading_box;
    @BindView(R.id.helper_noconnection)
    protected RelativeLayout helper_noconnection;
    @BindView(R.id.lay_more)
    protected LinearLayout lay_more;
    @BindView(R.id.helper_empty)
    protected RelativeLayout helper_empty;
    @BindView(R.id.refresh_layout)
    protected SwipeRefreshLayout refresh_layout;
    @BindView(R.id.edt_search)
    protected EditText edt_search;

    List<MateriClass> classes;
    LinearLayoutManager lm;
    MateriAdapter adapter;

    String keyword = "";

    int page = 1;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi);
        ButterKnife.bind(this);

        refresh_layout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.green_500
        );

        refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                edt_search.setText("");
                keyword = "";
                page = 1;
                doGetMateri(true);
            }
        });
        toolbarLestari = new ToolbarLestari(getContext(),"Materi Degradasi");
        classes = new ArrayList<>();
        lm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(lm);
        adapter = new MateriAdapter(getContext(), classes);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = lm.getChildCount();
                    totalItemCount = lm.getItemCount();
                    pastVisiblesItems = lm.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            //doRefresh();
                            doGetMateri(false);
                        }
                    }
                }
            }
        });

        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideKeyBoard();
                    keyword = edt_search.getText().toString();
                    page = 1;
                    doGetMateri(false);
                    return true;
                }
                return false;
            }
        });

        doGetMateri(false);
    }

    private void hideKeyBoard(){
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);
    }

    /*private List<MateriClass> dataStatis(){
        List<MateriClass> dataStatis = new ArrayList<>();
        MateriClass materiClass = new MateriClass();
        materiClass.setContent(getResources().getString(R.string.lorem_ipsum));
        materiClass.setJenis_degradasi(LestariConstant.EROSI);
        materiClass.setJudul("Degradasi Limbah");
        materiClass.setYoutube("RnL8J6LO0dc");
        dataStatis.add(materiClass);

        materiClass = new MateriClass();
        materiClass.setContent(getResources().getString(R.string.lorem_ipsum));
        materiClass.setJenis_degradasi(LestariConstant.LIMBAH);
        materiClass.setYoutube("Limbah, berbahayakah?");
        dataStatis.add(materiClass);

        materiClass = new MateriClass();
        materiClass.setContent(getResources().getString(R.string.lorem_ipsum));
        materiClass.setJenis_degradasi(LestariConstant.LIMBAH);
        materiClass.setYoutube("Apa itu limbah ?");
        materiClass.setYoutube("RnL8J6LO0dc");
        dataStatis.add(materiClass);

        materiClass = new MateriClass();
        materiClass.setContent(getResources().getString(R.string.lorem_ipsum));
        materiClass.setJenis_degradasi(LestariConstant.SAMPAH);
        materiClass.setYoutube("Sampah, berbahayakah ?");
        materiClass.setYoutube("RnL8J6LO0dc");
        dataStatis.add(materiClass);

        return dataStatis;

    }*/

    private void doGetMateri(final Boolean isFromRefreshed){
        LestariRequest.POST(LestariParam.stringMateri(), getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                //XUtils.CreateDialog(DetailNegaraActivity.this, R.mipmap.ic_launcher_round);
                //mLoading.setVisibility(View.VISIBLE);
                helper_noconnection.setVisibility(View.GONE);
                helper_empty.setVisibility(View.GONE);
                if(isFromRefreshed)
                    helper_loading_box.setVisibility(View.GONE);
                else{
                    if(page==1)
                        helper_loading_box.setVisibility(View.VISIBLE);
                    else
                        lay_more.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSuccess(JSONObject response) {
                helper_loading_box.setVisibility(View.GONE);
                lay_more.setVisibility(View.GONE);
                refresh_layout.setRefreshing(false);
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        if(page==1){
                            classes.clear();
                        }
                        page++;
                        loading = true;
                        JSONArray data = response.getJSONArray("data");

                        for (MateriClass materiClass : MateriHelper.listMateri(data)){
                            classes.add(materiClass);
                        }
                        adapter.notifyDataSetChanged();
                    }else{
                        loading = false;
                        if(page==1){
                            classes.clear();
                            helper_empty.setVisibility(View.VISIBLE);
                            adapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                helper_noconnection.setVisibility(View.VISIBLE);
                helper_loading_box.setVisibility(View.GONE);
                lay_more.setVisibility(View.GONE);
                refresh_layout.setRefreshing(false);
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                //param.put("access_token", SafeTravel.stringPintu());
                    param.put("lestari_token_check", Lestari.getSPString(getContext(), LestariConstant.SP_USER_TOKEN));
                    param.put("lestari_page", ""+page);
                    param.put("lestari_limit_data", "5");
                    param.put("lestari_kata_kunci", keyword);
                    if(!keyword.equalsIgnoreCase("")){
                        param.put("lestari_cari", ""+true);
                    } else {
                        param.put("lestari_all", "true");
                    }
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }
        });
    }


}
