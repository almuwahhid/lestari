package id.ac.ugm.glmb.lestari.mainmenu.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.login.KonfirmasiLupaPasswordActivity;

/**
 * Created
 * by root on 7/29/18.
 */

public class DialogKonfirmasiLupaPassword {
    Context context;
    Dialog dialogLainnya;
    RelativeLayout lay_konfirm;
    RelativeLayout lay_kirim;

    public DialogKonfirmasiLupaPassword(final Context context) {
        this.context = context;

        dialogLainnya = new Dialog(context);

        dialogLainnya.getWindow().getAttributes().windowAnimations = id.ac.ugm.glmb.uilibs.R.style.DialogBottomAnimation;
        dialogLainnya.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLainnya.setContentView(R.layout.lay_verifikasi_terkirim);
        dialogLainnya.setCanceledOnTouchOutside(true);
        dialogLainnya.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogLainnya.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogLainnya.setCancelable(true);
        Window window = dialogLainnya.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;


        lay_kirim = dialogLainnya.findViewById(R.id.lay_kirim);
        lay_konfirm = dialogLainnya.findViewById(R.id.lay_konfirm);
        lay_konfirm.setLayoutParams(new FrameLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT));

        lay_kirim.setOnClickListener(new  View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, KonfirmasiLupaPasswordActivity.class));
                ((Activity) context).finish();
            }
        });
        show();
    }

    public void dismiss(){
        if(dialogLainnya!= null)
            this.dialogLainnya.dismiss();
    }

    public void show(){
        this.dialogLainnya.show();
    }

}
