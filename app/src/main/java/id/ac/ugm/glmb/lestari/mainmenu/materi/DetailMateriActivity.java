package id.ac.ugm.glmb.lestari.mainmenu.materi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.TambahDegradasiActivity;
import id.ac.ugm.glmb.lestari.utilities.ActivityYoutube;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;

public class DetailMateriActivity extends ActivityYoutube implements YouTubePlayer.OnInitializedListener {
    @BindView(R.id.lay_back)
    CardView lay_back;
    @BindView(R.id.lay_next)
    RelativeLayout lay_next;
    @BindView(R.id.youtube_view)
    YouTubePlayerView youtube_view;

    @BindView(R.id.webView)
    WebView webView;

    MateriClass materiClass;
    String header = "";
    String data = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_materi);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("materi")){
            materiClass = (MateriClass) getIntent().getSerializableExtra("materi");
        }

        header = "<h3>"+materiClass.getJudul()+"</h3>";
        data = materiClass.getContent();
        String html = LestariConstant.WEBVIEW_STYLE;
        String mime = "text/html";
        String encoding = "utf-8";

        webView.loadData(String.format(html, (header+data)), mime, encoding);
        lay_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        lay_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), TambahDegradasiActivity.class));
            }
        });
        youtube_view.initialize(getResources().getString(R.string.google_maps_key), this);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        String youtube = "";
        try {
            youtube = materiClass.getYoutube().split("v=")[1];
        } catch (IndexOutOfBoundsException e){
            youtube = "";
        }
        youTubePlayer.cueVideo(youtube);
//        youTubePlayer.play();
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if(youTubeInitializationResult.isUserRecoverableError()){
            youTubeInitializationResult.getErrorDialog(this, 1).show();
        }
    }
}
