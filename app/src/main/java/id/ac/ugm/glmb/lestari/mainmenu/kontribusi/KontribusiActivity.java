package id.ac.ugm.glmb.lestari.mainmenu.kontribusi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.Degradasi;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogPilihan;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariParam;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;

public class KontribusiActivity extends ActivityGeneral {

    LinearLayoutManager layoutManager;
    KontribusiAdapter adapter;
    @BindView(R.id.recyclerView) protected RecyclerView recyclerView;

    @BindView(R.id.helper_loading_box)
    protected RelativeLayout helper_loading_box;
    @BindView(R.id.lay_more)
    protected LinearLayout lay_more;
    @BindView(R.id.helper_empty)
    protected RelativeLayout helper_empty;
    @BindView(R.id.helper_noconnection)
    protected RelativeLayout helper_noconnection;
    @BindView(R.id.refresh_layout)
    protected SwipeRefreshLayout refresh_layout;
    @BindView(R.id.edt_filter)
    protected EditText edt_filter;

    List<Degradasi> classes;
    List<Boolean> sortir_data;
    DialogPilihan dialogPilihan;

    int page = 1;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kontribusi);
        ButterKnife.bind(this);
        initSortirData();
        new ToolbarLestari(getContext(), "Kontribusi Saya");

        layoutManager = new LinearLayoutManager(this);
        classes = new ArrayList<>();
        adapter = new KontribusiAdapter(getContext(), classes);
        adapter.setOnClickListener(new KontribusiAdapter.OnKontribusiClick() {
            @Override
            public void onItemClick(View v, Degradasi degradasi) {
                Intent intent = new Intent(getContext(), DetailKontribusiActivity.class);
                intent.putExtra("degradasi", degradasi);
                startActivity(intent);
            }
        });
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            //doRefresh();
                            requestDegradasi(true);
                        }
                    }
                }
            }
        });

        refresh_layout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.green_500
        );

        refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                requestDegradasi(true);
            }
        });

        edt_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initDialog();
            }
        });

        requestDegradasi(false);
    }

    private void requestDegradasi(final Boolean isFromRefreshed){
        LestariRequest.POST(LestariParam.stringDegradasi(), getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                helper_noconnection.setVisibility(View.GONE);
                helper_empty.setVisibility(View.GONE);
                if(isFromRefreshed)
                    helper_loading_box.setVisibility(View.GONE);
                else{
                    if(page==1){
                        helper_loading_box.setVisibility(View.VISIBLE);
                    }
                    else
                        lay_more.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSuccess(JSONObject response) {
                helper_loading_box.setVisibility(View.GONE);
                lay_more.setVisibility(View.GONE);
                refresh_layout.setRefreshing(false);
                try {

                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        if(page==1){
                            classes.clear();
                        }
                        page++;
                        loading = true;
                        for(Degradasi degradasi : KontribusiHelper.listKontribusi(response.getJSONArray("data"))){
                            classes.add(degradasi);
                        }
                        adapter.notifyDataSetChanged();
                    }else {
                        loading = false;
                        if(page==1){
                            classes.clear();
                            helper_empty.setVisibility(View.VISIBLE);
                            adapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                helper_loading_box.setVisibility(View.GONE);
                lay_more.setVisibility(View.GONE);
                refresh_layout.setRefreshing(false);

                if(page==1){
                    classes.clear();
                    helper_empty.setVisibility(View.VISIBLE);
                    adapter.notifyDataSetChanged();
                } else {
                    helper_noconnection.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("lestari_token_check", Lestari.getSPString(getContext(), LestariConstant.SP_USER_TOKEN));
                param.put("lestari_all", "true");
                param.put("lestari_page", ""+page);
                param.put("lestari_limit_data", "5");
                param.put("lestari_id_user", Lestari.getSPString(getContext(), LestariConstant.SP_USER_ID));

                param.put("lestari_isErosi", ""+sortir_data.get(0));
                param.put("lestari_isLongsor", ""+sortir_data.get(1));
                param.put("lestari_isSedimentasi", ""+sortir_data.get(2));
                param.put("lestari_isEutrofikasi", ""+sortir_data.get(3));
                param.put("lestari_isSampah", ""+sortir_data.get(4));
                param.put("lestari_isLimbah", ""+sortir_data.get(5));
                param.put("lestari_isBanjir", ""+sortir_data.get(6));
                param.put("lestari_isKebakaran", ""+sortir_data.get(7));
                param.put("lestari_isPengendalian", ""+sortir_data.get(8));
                System.out.println(param);
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }
        });

    }

    private void initDialog(){
        if(dialogPilihan==null){
            dialogPilihan = new DialogPilihan(getContext(), KontribusiHelper.listDegradasi(), "Sortir Berdasarkan");
            dialogPilihan.setOnItemClick(new DialogPilihan.OnItemClick() {
                @Override
                public void onItemClick(View view, PilihanClass pilihanClass, int position) {
                    if(position == 0){
                        setAllFilterTrue();
                    } else {
                        setFilter(position-1);
                    }
                    edt_filter.setText(pilihanClass.getName());
                    page = 1;
                    requestDegradasi(false);
                }
            });
            dialogPilihan.show();
        } else{
            dialogPilihan.show();
        }
    }

    private void initSortirData(){
        sortir_data = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            sortir_data.add(true);
        }
    }

    private void setFilter(int position){
        for (int i = 0; i < sortir_data.size(); i++) {
            if(i == position){
                sortir_data.set(i, true);
            } else {
                sortir_data.set(i, false);
            }
        }
    }

    private void setAllFilterTrue(){
        for (int i = 0; i < sortir_data.size(); i++) {
            sortir_data.set(i, true);
        }
    }
}
