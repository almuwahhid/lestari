package id.ac.ugm.glmb.lestari.mainmenu.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.utilities.Lestari;

/**
 * Created by root on 7/21/18.
 */

public class DialogLainnya {
    Context context;
    EditText edt_lainnya;
    Button button_lainnya;
    Dialog dialogLainnya;
    LinearLayout lay_pick;
    OnSubmit onSubmit;

    public DialogLainnya(final Context context, final OnSubmit onSubmit) {
        this.context = context;
        this.onSubmit = onSubmit;

        dialogLainnya = new Dialog(context);

        dialogLainnya.getWindow().getAttributes().windowAnimations = id.ac.ugm.glmb.uilibs.R.style.DialogBottomAnimation;
        dialogLainnya.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLainnya.setContentView(R.layout.dialog_pick_text);
        dialogLainnya.setCanceledOnTouchOutside(true);
        dialogLainnya.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogLainnya.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogLainnya.setCancelable(true);
        Window window = dialogLainnya.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
//        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;


        edt_lainnya = dialogLainnya.findViewById(R.id.edt_text);
        button_lainnya = dialogLainnya.findViewById(R.id.btn_ok);
        lay_pick = dialogLainnya.findViewById(R.id.lay_pick);
        lay_pick.setLayoutParams(new FrameLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT));

        button_lainnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_lainnya.getText().toString().equalsIgnoreCase("")){
                    Lestari.alertWarning(context, context.getResources().getString(R.string.form_empty));
                } else {
                    onSubmit.onSubmit(edt_lainnya.getText().toString());
                    dialogLainnya.dismiss();
                }
            }
        });
        show();
    }

    public void dismiss(){
        if(dialogLainnya!= null)
            this.dialogLainnya.dismiss();
    }

    public void show(){
        edt_lainnya.setText("");
        this.dialogLainnya.show();
    }

    public interface OnSubmit{
        void onSubmit(String data);
    }
}
