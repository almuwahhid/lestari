package id.ac.ugm.glmb.lestari.mainmenu.materi;

import java.io.Serializable;

/**
 * Created by root on 7/8/18.
 */

public class MateriClass implements Serializable{
    String id, judul, content, thumbnail, youtube, jenis_degradasi;

    public String getJenis_degradasi() {
        return jenis_degradasi;
    }

    public void setJenis_degradasi(String jenis_degradasi) {
        this.jenis_degradasi = jenis_degradasi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }
}
