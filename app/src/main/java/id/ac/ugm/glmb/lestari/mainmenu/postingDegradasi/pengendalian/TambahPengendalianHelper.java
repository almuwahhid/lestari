package id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.pengendalian;

import java.util.ArrayList;
import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;

/**
 * Created by root on 8/4/18.
 */

public class TambahPengendalianHelper {
    public static List<PilihanClass> listJenisPengendalian(){
        List<PilihanClass> listDegradasi = new ArrayList<>();
        PilihanClass pilihanClass = new PilihanClass();
        pilihanClass.setId("Vegetatif");
        pilihanClass.setName("Vegetatif");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setName("Agronomi");
        pilihanClass.setId("Agronomi");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Sipil Teknis");
        pilihanClass.setName("Sipil Teknis");
        listDegradasi.add(pilihanClass);

        return listDegradasi;
    }

    public static List<PilihanClass> listKegiatanPengendalian(String jenisPengendalian){
        List<PilihanClass> listDegradasi = new ArrayList<>();
        PilihanClass pilihanClass;
        switch (jenisPengendalian){
            case "Vegetatif":
                pilihanClass = new PilihanClass();
                pilihanClass.setId("Reboisasi");
                pilihanClass.setName("Reboisasi");
                listDegradasi.add(pilihanClass);

                pilihanClass = new PilihanClass();
                pilihanClass.setId("Agroforestri");
                pilihanClass.setName("Agroforestri");
                listDegradasi.add(pilihanClass);
                break;
            case "Agronomi":
                pilihanClass = new PilihanClass();
                pilihanClass.setId("Pemberian Nulsa/Geotekstil");
                pilihanClass.setName("Pemberian Nulsa/Geotekstil");
                listDegradasi.add(pilihanClass);

                pilihanClass = new PilihanClass();
                pilihanClass.setId("Pemberian Pola Tanam");
                pilihanClass.setName("Pemberian Pola Tanam");
                listDegradasi.add(pilihanClass);

                pilihanClass = new PilihanClass();
                pilihanClass.setId("Pemberian Amelioran");
                pilihanClass.setName("Pemberian Amelioran");
                listDegradasi.add(pilihanClass);

                pilihanClass = new PilihanClass();
                pilihanClass.setId("Pengayaan Tanaman");
                pilihanClass.setName("Pengayaan Tanaman");
                listDegradasi.add(pilihanClass);

                pilihanClass = new PilihanClass();
                pilihanClass.setId("Penanaman Mengikuti Kontur");
                pilihanClass.setName("Penanaman Mengikuti Kontur");
                listDegradasi.add(pilihanClass);
                break;
            case "Sipil Teknis":
                pilihanClass = new PilihanClass();
                pilihanClass.setId("Teras Guludan");
                pilihanClass.setName("Teras Guludan");
                listDegradasi.add(pilihanClass);

                pilihanClass = new PilihanClass();
                pilihanClass.setId("Teras Bangku");
                pilihanClass.setName("Teras Bangku");
                listDegradasi.add(pilihanClass);

                pilihanClass = new PilihanClass();
                pilihanClass.setId("Pengendali Jurang");
                pilihanClass.setName("Pengendali Jurang");
                listDegradasi.add(pilihanClass);

                pilihanClass = new PilihanClass();
                pilihanClass.setId("Sumur Resapan");
                pilihanClass.setName("Sumur Resapan");
                listDegradasi.add(pilihanClass);

                pilihanClass = new PilihanClass();
                pilihanClass.setId("Kolam Retensi");
                pilihanClass.setName("Kolam Retensi");
                listDegradasi.add(pilihanClass);

                pilihanClass = new PilihanClass();
                pilihanClass.setId("Dam Pengendali");
                pilihanClass.setName("Dam Pengendali");
                listDegradasi.add(pilihanClass);

                pilihanClass = new PilihanClass();
                pilihanClass.setId("Dam Penahan");
                pilihanClass.setName("Dam Penahan");
                listDegradasi.add(pilihanClass);

                pilihanClass = new PilihanClass();
                pilihanClass.setId("Rotok");
                pilihanClass.setName("Rotok");
                listDegradasi.add(pilihanClass);

                pilihanClass = new PilihanClass();
                pilihanClass.setId("Saluran Pembuangan Air");
                pilihanClass.setName("Saluran Pembuangan Air");
                listDegradasi.add(pilihanClass);

                pilihanClass = new PilihanClass();
                pilihanClass.setId("Gully Plug");
                pilihanClass.setName("Gully Plug");
                listDegradasi.add(pilihanClass);
                break;

        }

        /*pilihanClass = new PilihanClass();
        pilihanClass.setName("Agronomi");
        pilihanClass.setId("Agronomi");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Sipil Teknis");
        pilihanClass.setName("Sipil Teknis");
        listDegradasi.add(pilihanClass);*/

        return listDegradasi;
    }
}
