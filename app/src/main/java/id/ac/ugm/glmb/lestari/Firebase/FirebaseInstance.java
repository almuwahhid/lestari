package id.ac.ugm.glmb.lestari.Firebase;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class FirebaseInstance extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
//        storeToken(refreshedToken);
    }

    /*private void storeToken(String token) {
        FirebaseToken.getInstance(getApplicationContext()).setToken(token);
    }*/
}
