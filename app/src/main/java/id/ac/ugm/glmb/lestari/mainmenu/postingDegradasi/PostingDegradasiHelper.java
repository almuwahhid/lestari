package id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi;

import android.content.Context;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.degradasi.GaleriDegradasi;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;

/**
 * Created by root on 7/13/18.
 */

public class PostingDegradasiHelper {
    public static List<PilihanClass> listErosi(){
        List<PilihanClass> listLimbah = new ArrayList<>();
        PilihanClass pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.PERCIK);
        pilihanClass.setName("Erosi Percik");
        listLimbah.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.LEMBAR);
        pilihanClass.setName("Erosi Lembar");
        listLimbah.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.ALUR);
        pilihanClass.setName("Erosi Alur");
        listLimbah.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.PARIT);
        pilihanClass.setName("Erosi Parit");
        listLimbah.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.TEBING);
        pilihanClass.setName("Erosi tebing");
        listLimbah.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.PIPA);
        pilihanClass.setName("Erosi Pipa");
        listLimbah.add(pilihanClass);

        return listLimbah;
    }

    public static List<PilihanClass> listLimbah(){
        List<PilihanClass> listLimbah = new ArrayList<>();
        PilihanClass pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.CAIR);
        pilihanClass.setName("Limbah Cair");
        listLimbah.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.PADAT);
        pilihanClass.setName("Limbah Padat");
        listLimbah.add(pilihanClass);

        return listLimbah;
    }

    public static List<PilihanClass> listDegradasi(){
        List<PilihanClass> listDegradasi = new ArrayList<>();
        PilihanClass pilihanClass;

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.BANJIR);
        pilihanClass.setName("Banjir");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.EROSI);
        pilihanClass.setName("Erosi");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.EUTROFIKASI);
        pilihanClass.setName("Eutrofikasi");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.KEBAKARAN);
        pilihanClass.setName("Kebakaran");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.LIMBAH);
        pilihanClass.setName("Limbah");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.LONGSOR);
        pilihanClass.setName("Longsor");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.SAMPAH);
        pilihanClass.setName("Sampah");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.SEDIMENTASI);
        pilihanClass.setName("Sedimentasi");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.PENGENDALIAN);
        pilihanClass.setName("Pengendalian");
        listDegradasi.add(pilihanClass);

        return listDegradasi;
    }

    public static List<PilihanClass> listLokasiKejadian(Context context){
        List<PilihanClass> listDegradasi = new ArrayList<>();
        PilihanClass pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.EROSI);
        pilihanClass.setName("Lahan");
        pilihanClass.setId("Lahan");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Pertanian");
        pilihanClass.setName("Pertanian");
        pilihanClass.setState(Lestari.getSPBoolean(context, LestariConstant.SP_KEBAKARAN));
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Permukiman");
        pilihanClass.setName("Permukiman");
        pilihanClass.setState(Lestari.getSPBoolean(context, LestariConstant.SP_KEBAKARAN));
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Hutan");
        pilihanClass.setName("Hutan");
        pilihanClass.setState(Lestari.getSPBoolean(context, LestariConstant.SP_KEBAKARAN));
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Sungai");
        pilihanClass.setName("Sungai");
        pilihanClass.setState(Lestari.getSPBoolean(context, LestariConstant.SP_KEBAKARAN));
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Danau");
        pilihanClass.setName("Danau");
        pilihanClass.setState(Lestari.getSPBoolean(context, LestariConstant.SP_KEBAKARAN));
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("-");
        pilihanClass.setName("Lainnya");
        pilihanClass.setState(Lestari.getSPBoolean(context, LestariConstant.SP_KEBAKARAN));
        listDegradasi.add(pilihanClass);

        return listDegradasi;
    }

    public static List<GaleriDegradasi> emptyGallery(){
        List<GaleriDegradasi> emptyGallery = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            GaleriDegradasi galeriDegradasi = new GaleriDegradasi();
            emptyGallery.add(galeriDegradasi);
        }
        return emptyGallery;
    }

    public static GaleriDegradasi getParsingGaleri(JSONObject jsonObject){
        GaleriDegradasi getParsingGaleri = new GaleriDegradasi();
        try {
            JSONArray data = jsonObject.getJSONArray("data");
            for (int i = 0; i < data.length(); i++) {
                JSONObject j = data.getJSONObject(i);
//                getParsingGaleri.setPicture(j.getString("picture").replace("http://", "https://"));
                getParsingGaleri.setPicture(j.getString("picture"));
                getParsingGaleri.setId_degradasi(j.getString("id_degradasi"));
                getParsingGaleri.setId_gallery(j.getString("id_gallery"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return getParsingGaleri;
    }

    public interface OnClickGallery{
        void OnShow(View v, GaleriDegradasi galeriDegradasi, int position);
    }
}
