package id.ac.ugm.glmb.lestari.utilities;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.das.ListPolyLineClass;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.das.PolyLineClass;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.helper.MapHelper;
import id.ac.ugm.glmb.uilib.utils.InputStreamVolleyRequest;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

/**
 * Created by root on 8/6/18.
 */

public class MainService extends Service {

    String id_degradasi = "", jenis_degradasi = "";
    IntentFilter filter;
    ListPolyLineClass datapolyLineClasses;

    double lat, lng;

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("telo", "onReceive: terima receiver");
            String action = intent.getAction();
            if (action.equals("lestari.Download")) {
                id_degradasi = intent.getStringExtra("id_degradasi");
                jenis_degradasi = intent.getStringExtra("jenis_degradasi");
                downloadDegradasi(LestariParam.stringDegradasi());
            } else if (action.equals(LestariConstant.SP_REQ_DAS_INTERNET)) {
                lat = intent.getDoubleExtra("lat", 0);
                lng = intent.getDoubleExtra("lng", 0);
                Log.d("teslat", "onReceive: "+lat+" "+lng);
                if(datapolyLineClasses!=null) {
                    Log.d("jumlahnya", "onReceive: "+filterDas(datapolyLineClasses).getClasses().size());
                    Intent intent1 = new Intent(LestariConstant.SP_REQ_DAS);
                    intent1.putExtra("polyline", filterDas(datapolyLineClasses));
                    sendBroadcast(intent1);
                } else {
                    doGetDas();
                }
            }   else if (action.equals(LestariConstant.SP_REQ_PROFILE)) {
                doUpdateProfile();
            }
        }
    };

    private void doUpdateProfile(){
        LestariRequest.POST(LestariParam.stringProfile(), getApplicationContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {

            }

            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");

                    Lestari.setSPString(getApplicationContext(), LestariConstant.SP_USER_JML_DEGRADASI, data.getString("jumlah_degradasi"));
                    Lestari.setSPString(getApplicationContext(), LestariConstant.SP_USER_JML_KONTRIBUSI, data.getString("jumlah_kontribusi"));
                    Intent intent1 = new Intent(LestariConstant.SP_UPDATE_PROFILE);
                    sendBroadcast(intent1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {

            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("lestari_token_check", Lestari.getSPString(getApplicationContext(), LestariConstant.SP_USER_TOKEN));
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }
        });
    }

    private ListPolyLineClass filterDas(ListPolyLineClass datapolyLineClasses){
        ListPolyLineClass filterDas = new ListPolyLineClass();
        List<PolyLineClass> polyLineClassList = new ArrayList<>();
        for (int i = 0; i < datapolyLineClasses.getClasses().size(); i++) {
            PolyLineClass polyLineClass = datapolyLineClasses.getClasses().get(i);
            double l1 = Double.parseDouble(polyLineClass.getTitiks().get(0).getLat());
            double l2 = Double.parseDouble(polyLineClass.getTitiks().get(0).getLng());
//            Log.d("service", "filterDas: "+l1+" "+l2);
            if(Haversine.distance(lat, lng, l1, l2)<200){
                polyLineClassList.add(polyLineClass);
            }
            filterDas.setClasses(polyLineClassList);
        }
        return filterDas;
    }

    private void makeFolder(){
        File file = new File(Environment.getExternalStorageDirectory(), "Lestari");
        if(!file.exists()){
            file.mkdirs();
            Log.d("asd", "makeFolder: "+Environment.getExternalStorageDirectory());
        }
    }

    private void downloadDegradasi(String URL){
        makeFolder();
        LestariRequest.POSTDownload(URL, getApplicationContext(), new LestariRequest.OnDownloadRequest() {
            @Override
            public void onPreExecuted() {
                showSedangMengunduh(getApplicationContext(), Integer.valueOf(id_degradasi), jenis_degradasi);
            }


            @Override
            public void onSuccess(byte[] response, InputStreamVolleyRequest request) {
                LestariUi.ToastShort(getApplicationContext(), "File berhasil diunduh");
                Log.d("gedeData", "onSuccess: "+response.length);

                HashMap<String, Object> map = new HashMap<String, Object>();
                try {
                    if (response!=null) {

                        //Read file name from headers (We have configured API to send file name in "Content-Disposition" header in following format: "File-Name:File-Format" example "MyDoc:pdf"

                        String content = request.responseHeaders.get("Content-Disposition")
                                .toString();
                        StringTokenizer st = new StringTokenizer(content, "=");
                        String[] arrTag = st.toArray();

                        String filename = arrTag[1];
                        filename = filename.replace(":", ".");
                        Log.d("DEBUG::FILE NAME", filename);

                        try{
                            long lenghtOfFile = response.length;

                            //covert reponse to input stream
                            InputStream input = new ByteArrayInputStream(response);

                            //Create a file on desired path and write stream data to it
                            File path = Environment.getExternalStorageDirectory();
                            File file = new File(path, filename);
                            map.put("resume_path", file.toString());
                            BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
                            byte data[] = new byte[1024];

                            long total = 0;

                            int count;
                            while ((count = input.read(data)) != -1) {
                                total += count;
                                output.write(data, 0, count);
                            }

                            output.flush();

                            output.close();
                            input.close();
                        }catch(IOException e){
                            e.printStackTrace();

                        }
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Log.d("KEY_ERROR", "UNABLE TO DOWNLOAD FILE");
                    e.printStackTrace();
                }



                /*File dir = Environment.getExternalStorageDirectory();
                File assist = new File(Environment.getExternalStorageDirectory()+"/"+ "Lestari");
                File datta = null;
                try {
                    datta = File.createTempFile(
                            "testing",  *//* prefix *//*
                            ".txt",         *//* suffix *//*
                            assist      *//* directory *//*
                    );
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    InputStream fis = new FileInputStream(datta);

                    long length = datta.length();
                    if (length > Integer.MAX_VALUE) {
                        Log.e("Sorry", "cannnottt   readddd");
                    }
                    byte[] bytes = new byte[(int) length];
                    int offset = 0;
                    int numRead = 0;
                    while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0) {
                        offset += numRead;
                    }

                    File dd = new File(Environment.getExternalStorageDirectory()+"/"+ "Lestari","mydemo.txt");
                    OutputStream op = new FileOutputStream(dd);
                    op.write(bytes);
                    op.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                hideSedangMengunduh(Integer.valueOf(id_degradasi));
            }

            @Override
            public void onFailure(String error) {
                LestariUi.ToastShort(getApplicationContext(), "File gagal diunduh");
                hideSedangMengunduh(Integer.valueOf(id_degradasi));
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<>();
                param.put("lestari_download", "true");
                param.put("lestari_id", id_degradasi);
                param.put("lestari_token_check", Lestari.getSPString(getApplicationContext(), LestariConstant.SP_USER_TOKEN));
                param.put("lestari_jenis_degradasi", jenis_degradasi);
                param.put("type_login", ""+1);
                System.out.println(param);
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                return null;
            }
        });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        makeFolder();
        filter = new IntentFilter();
        filter.addAction("lestari.Download");
//        filter.addAction(LestariConstant.SP_REQ_DAS);
        filter.addAction(LestariConstant.SP_REQ_DAS_INTERNET);
        filter.addAction(LestariConstant.SP_REQ_PROFILE);
        registerReceiver(receiver, filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    private void doGetDas(){
        /*polyLineClasses = new ListPolyLineClass();
        polyLineClasses = MapHelper.listPolyLine(MapHelper.loadJSONFromAsset(getContext()));
        showDas();
        isDasShow = true;*/
        Log.d("das", "otw: otw");
        LestariRequest.POST(LestariParam.das(), getApplicationContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {

            }

            @Override
            public void onSuccess(JSONObject response) {
                try {
                    Log.d("das", "onSuccess: its success");
                    datapolyLineClasses = new ListPolyLineClass();
                    datapolyLineClasses = MapHelper.listPolyLine(response.getJSONArray("geometries"));
                    Log.d("das", "onSuccess: "+datapolyLineClasses.getClasses().size());
                    Log.d("jumlahnya", "onReceive: "+filterDas(datapolyLineClasses).getClasses().size());
                    Intent intent1 = new Intent(LestariConstant.SP_REQ_DAS);
                    intent1.putExtra("polyline", filterDas(datapolyLineClasses));
                    sendBroadcast(intent1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {

            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }
        });
    }

    private void hideSedangMengunduh(int id){
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(id);
    }

    public void showSedangMengunduh(Context ctx, int id, String content) {
        String NOTIFICATION_CHANNEL_ID = "lestari_channel";
        NotificationManager notificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notifikasi Lestari", NotificationManager.IMPORTANCE_DEFAULT);
            // Configure the notification channel.
            notificationChannel.setDescription("Lestari");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.GREEN);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ctx, NOTIFICATION_CHANNEL_ID);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            mBuilder.setPriority(Notification.PRIORITY_HIGH);
        }
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);
        mBuilder.setOnlyAlertOnce(true);
        Notification notification = mBuilder.setSmallIcon(R.drawable.notif_img).setTicker("Download File").setWhen(0)
                .setAutoCancel(true)
                .setContentTitle("Download File")
                .setSmallIcon(R.drawable.notif_img)
                .setLargeIcon(BitmapFactory.decodeResource(ctx.getResources(), R.drawable.notif_img))
                .setContentText("Sedang mengunduh file "+ content)
                .build();

        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        notificationManager.notify(id, notification);
    }
}