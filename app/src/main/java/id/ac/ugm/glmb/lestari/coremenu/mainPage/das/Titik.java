package id.ac.ugm.glmb.lestari.coremenu.mainPage.das;

import java.io.Serializable;

/**
 * Created by root on 7/31/18.
 */

public class Titik implements Serializable{
    private String lat = "", lng = "";

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
