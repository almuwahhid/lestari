package id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.helper.MapHelper;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.PhotoActivity;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

public class ErosiTebingSungaiAdapter extends RecyclerView.Adapter<ErosiTebingSungaiAdapter.MyHolder>  {
    Context context;
    List<ErosiClass> classes;

    public ErosiTebingSungaiAdapter(Context context, List<ErosiClass> classes) {
        this.context = context;
        this.classes = classes;
    }

    public ErosiTebingSungaiAdapter() {
    }

    @NonNull
    @Override
    public ErosiTebingSungaiAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_erosi_tebing_sungai_adapter, parent, false);
        MyHolder rcv = new MyHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(@NonNull ErosiTebingSungaiAdapter.MyHolder holder, int position) {
        ErosiClass erosiClass = classes.get(position);
        holder.tv_lebar.setText(Lestari.formattingNumber(erosiClass.getLebar())+" m");
        holder.tv_panjang.setText(Lestari.formattingNumber(erosiClass.getLebar())+" m\u00B2");
        holder.tv_kedalaman.setText(Lestari.formattingNumber(erosiClass.getLebar())+" m");
        holder.tv_berat.setText(Lestari.formattingNumber(erosiClass.getLebar())+" gr/cm\u00B3");
        holder.tv_kehilangantanah.setText(MapHelper.kehilanganTanahTebing(erosiClass));

        holder.tv_deskripsi.setText(erosiClass.getDeskripsi());
        holder.tv_name.setText(erosiClass.getName());
        new PhotoActivity(context, holder.recyclerView, holder.tv_nodata, erosiClass.getGalleries());
        try{
            String tgl = erosiClass.getDate().split(" ")[0];
            int year = Integer.valueOf(tgl.split("-")[0]);
            int month = Integer.valueOf(tgl.split("-")[1])-1;
            int date = Integer.valueOf(tgl.split("-")[2]);

            holder.tv_date.setText(date+" "+ LestariUi.monthName(month)+" "+year+", "+erosiClass.getDate().split(" ")[1]);
        } catch (IndexOutOfBoundsException e){
            holder.tv_date.setText("");
        } catch (NumberFormatException e){
            holder.tv_date.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return classes.size();
    }


    public class MyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_lebar)
        TextView tv_lebar;@BindView(R.id.tv_panjang)
        TextView tv_panjang;@BindView(R.id.tv_kedalaman)
        TextView tv_kedalaman;@BindView(R.id.tv_berat)
        TextView tv_berat;@BindView(R.id.tv_kehilangantanah)
        TextView tv_kehilangantanah;

        @BindView(R.id.tv_deskripsi)
        TextView tv_deskripsi;
        @BindView(R.id.tv_nodata)
        TextView tv_nodata;
        @BindView(R.id.tv_date)
        TextView tv_date;
        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;
        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
