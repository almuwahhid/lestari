package id.ac.ugm.glmb.lestari.mainmenu.pemberitahuan;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import id.ac.ugm.glmb.lestari.R;

public class PemberitahuanAdapter extends RecyclerView.Adapter<PemberitahuanAdapter.MyHolder>{

    Context context;
    List<PemberitahuanClass> classList;

    public PemberitahuanAdapter() {
    }

    public PemberitahuanAdapter(Context context, List<PemberitahuanClass> classList) {
        this.context = context;
        this.classList = classList;
    }

    @NonNull
    @Override
    public PemberitahuanAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_pemberitahuan_adapter, parent, false);
        MyHolder rcv = new MyHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(@NonNull PemberitahuanAdapter.MyHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        public MyHolder(View itemView) {
            super(itemView);
        }
    }
}
