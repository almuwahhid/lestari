package id.ac.ugm.glmb.lestari.coremenu.mainPage.helper;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import id.ac.ugm.glmb.lestari.ClusteringMap.ClusterItem;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.Degradasi;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;

/**
 * Created by root on 7/23/18.
 */

public class MarkerCluster implements ClusterItem {
    private final LatLng mPosition;
    private String mTitle;
    private Degradasi mSnippet;
    private BitmapDescriptor icon;
    Gson gson;
    Context context;

    public MarkerCluster(double lat, double lng, Context context) {
        mPosition = new LatLng(lat, lng);
        this.context = context;
        gson = new Gson();
    }

    public MarkerCluster(double lat, double lng, String title, Degradasi snippet, Context context) {
        mPosition = new LatLng(lat, lng);
        this.context = context;
        mTitle = title;
        mSnippet = snippet;
        gson = new Gson();
    }


    @Override
    public double getLatitude() {
        return mPosition.latitude;
    }

    @Override
    public double getLongitude() {
        return mPosition.longitude;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getSnippet() {
        String stringSnippet = gson.toJson(mSnippet);
        return stringSnippet;
    }

    public BitmapDescriptor getIcon() {
        switch (mTitle) {
            case LestariConstant.LONGSOR:
                icon = BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_longsor, ((Activity) getContext())));
                break;

            case LestariConstant.EROSI:
                icon = BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_erosi, ((Activity) getContext())));
                break;

            case LestariConstant.EUTROFIKASI:
                icon = BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_eutrofikasi, ((Activity) getContext())));
                break;

            case LestariConstant.KEBAKARAN:
                icon = BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_kebakaran, ((Activity) getContext())));

                break;

            case LestariConstant.SAMPAH:
                icon = BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_sampah, ((Activity) getContext())));
                break;

            case LestariConstant.LIMBAH:
                icon = BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_limbah, ((Activity) getContext())));
                break;

            case LestariConstant.SEDIMENTASI:
                icon = BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_sedimentasi, ((Activity) getContext())));
                break;

            case LestariConstant.BANJIR:
                icon = BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_banjir, ((Activity) getContext())));
                break;
            case LestariConstant.PENGENDALIAN:
                icon = BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_pengendalian, ((Activity) getContext())));
                break;
        }
        return icon;
    }

    private Context getContext(){
        return context;
    }
}
