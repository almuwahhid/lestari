package id.ac.ugm.glmb.lestari.utilities;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import id.ac.ugm.glmb.lestari.R;

/**
 * Created by root on 6/27/18.
 */

public class ToolbarLestari {
    Context context;
    TextView toolbar_title;
    ImageView ic_back;
    Toolbar toolbar;
    String title;

    public ToolbarLestari(Context context, String title) {
        this.context = context;
        this.title = title;
        initToolbar();
    }

    private void initToolbar(){
        toolbar = ((Activity)context).findViewById(R.id.toolbar);
        ((AppCompatActivity)context).setSupportActionBar(toolbar);
        ((AppCompatActivity)context).getSupportActionBar().setDisplayOptions(
                ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
        ((AppCompatActivity)context).getSupportActionBar().setTitle(title);
        /*toolbar_title = ((Activity)context).findViewById(R.id.toolbar_title);
        ic_back = ((Activity)context).findViewById(R.id.ic_back);
        toolbar_title.setText(title);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) context).finish();
            }
        });*/


    }
}
