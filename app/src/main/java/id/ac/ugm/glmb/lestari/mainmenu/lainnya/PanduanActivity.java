package id.ac.ugm.glmb.lestari.mainmenu.lainnya;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import id.ac.ugm.glmb.lestari.R;

public class PanduanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panduan);
    }
}
