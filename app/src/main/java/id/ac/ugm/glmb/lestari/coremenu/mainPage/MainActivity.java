package id.ac.ugm.glmb.lestari.coremenu.mainPage;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import agency.tango.android.avatarview.AvatarPlaceholder;
import agency.tango.android.avatarview.loader.PicassoLoader;
import agency.tango.android.avatarview.views.AvatarView;
import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.ClusteringMap.ClusterManager;
import id.ac.ugm.glmb.lestari.ClusteringMap.DefaultIconGenerator;
import id.ac.ugm.glmb.lestari.ClusteringMap.IconGenerator;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.das.ListPolyLineClass;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.das.PolyLineClass;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.das.Titik;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.helper.DegradasiServiceHelper;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.helper.MapHelper;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.helper.MarkerCluster;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.helper.MenuClass;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.helper.MenuHelper;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.Degradasi;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.banjir.BanjirAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.banjir.BanjirClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiAlurAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiLembarAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiParitAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiPercikAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiPipaAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiTebingSungaiAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.eutrofikasi.EutrofikasiAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.eutrofikasi.EutrofikasiClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.kebakaran.KebakaranAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.kebakaran.KebakaranClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.limbah.LimbahAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.limbah.LimbahClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.longsor.LongsorAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.longsor.LongsorClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.pengendalian.PengendalianAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.pengendalian.PengendalianClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.sampah.SampahAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.sampah.SampahClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.sedimentasi.SedimentasiAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.sedimentasi.SedimentasiClass;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogDegradasi;
import id.ac.ugm.glmb.lestari.mainmenu.login.LoginActivity;
import id.ac.ugm.glmb.lestari.mainmenu.pengaturan.PengaturanActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.TambahDegradasiActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.banjir.TambahBanjirActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiAlurActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiLembarActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiParitActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiPercikActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiPipaActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.erosi.TambahErosiTebingActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.eutrofikasi.TambahEutrofikasiActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.kebakaran.TambahKebakaranActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.limbah.TambahLimbahActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.longsor.TambahLongsorActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.sampah.TambahSampahActivity;
import id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.sedimentasi.TambahSedimentasiActivity;
import id.ac.ugm.glmb.lestari.mainmenu.profil.ProfilActivity;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogPhoto;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariParam;
import id.ac.ugm.glmb.lestari.utilities.LestariUtil;
import id.ac.ugm.glmb.lestari.utilities.MainService;
import id.ac.ugm.glmb.uilib.Activity.ActivityPermission;
import id.ac.ugm.glmb.uilib.Activity.Interfaces.PermissionResultInterface;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;
import id.ac.ugm.glmb.uilib.utils.LestariUi;
import id.ac.ugm.glmb.uilib.utils.PermissionKey;

public class MainActivity extends ActivityPermission implements OnMapReadyCallback, OnSuccessListener<Location>, View.OnClickListener {
    @BindView(R.id.navigation)
    protected NavigationView navigation;
    @BindView(R.id.rv_menu)
    protected RecyclerView rv_menu;
    @BindView(R.id.avatarView)
    protected AvatarView avatarView;
    @BindView(R.id.tv_name)
    protected TextView tv_name;
    @BindView(R.id.lay_profil)
    protected LinearLayout lay_profil;
    @BindView(R.id.tv_kontribusi)
    protected TextView tv_kontribusi;
    @BindView(R.id.tv_degradasi)
    protected TextView tv_degradasi;
    @BindView(R.id.card_nav)
    protected CardView card_nav;
    @BindView(R.id.card_filter)
    protected CardView card_filter;
    @BindView(R.id.card_form_search)
    protected CardView card_form_search;
    @BindView(R.id.card_search)
    protected CardView card_search;
    @BindView(R.id.drawer_layout)
    protected DrawerLayout drawerLayout;
    @BindView(R.id.card_add)
    protected CardView card_add;
    @BindView(R.id.card_kontribusi)
    protected CardView card_kontribusi;
    @BindView(R.id.pb_map)
    protected ProgressBar pb_map;
    @BindView(R.id.edt_search)
    protected EditText edt_search;

    @BindView(R.id.img_close)
    protected ImageView img_close;
    @BindView(R.id.img_setting)
    protected ImageView img_setting;
    @BindView(R.id.img_pilihan)
    protected ImageView img_pilihan;
    @BindView(R.id.img_direction)
    protected ImageView img_direction;

    @BindView(R.id.lay_bottomsheet_degradasi)
    protected RelativeLayout lay_bottomsheet_degradasi;
    @BindView(R.id.img_bottomsheet_controller)
    protected ImageView img_bottomsheet_controller;
    @BindView(R.id.tv_bottomsheet_title)
    protected TextView tv_bottomsheet_title;
    @BindView(R.id.lay_bottomsheet_direction)
    protected LinearLayout lay_bottomsheet_direction;
    @BindView(R.id.tv_bottomsheet_direction)
    protected TextView tv_bottomsheet_direction;
    @BindView(R.id.rv_bottomsheet)
    protected RecyclerView rv_bottomsheet;
    @BindView(R.id.lay_header_bottomsheet)
    protected LinearLayout lay_header_bottomsheet;
    @BindView(R.id.tv_bottomsheet_jenislokasi)
    protected TextView tv_bottomsheet_jenislokasi;

    @BindView(R.id.helper_loading_box)
    protected RelativeLayout helper_loading_box;
    @BindView(R.id.helper_empty)
    protected RelativeLayout helper_empty;
    @BindView(R.id.helper_noconnection)
    protected RelativeLayout helper_noconnection;

    @BindView(R.id.pb_das)
    protected ProgressBar pb_das;
    @BindView(R.id.img_das)
    protected ImageView img_das;
    @BindView(R.id.card_das)
    protected CardView card_das;
    @BindView(R.id.lay_menunggudas)
    protected RelativeLayout lay_menunggudas;
    ListPolyLineClass polyLineClasses;
    boolean isDasShow = false;
    boolean flag_filter = false;

    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;

    DialogDegradasi dialogDegradasi;
    JSONArray coordinates;
    Gson gson;

    String keyword = "";
    String photo_url = "";
    DialogPhoto dialogPhoto;

    //    BottomSheet Assets
    private BottomSheetBehavior bottomSheetBehavior;
    ErosiPercikAdapter erosiPercikAdapter;
    ErosiAlurAdapter erosiAlurAdapter;
    ErosiLembarAdapter erosiLembarAdapter;
    ErosiParitAdapter erosiParitAdapter;
    ErosiPipaAdapter erosiPipaAdapter;
    ErosiTebingSungaiAdapter erosiTebingSungaiAdapter;
    SampahAdapter sampahAdapter;
    BanjirAdapter banjirAdapter;
    EutrofikasiAdapter eutrofikasiAdapter;
    KebakaranAdapter kebakaranAdapter;
    LimbahAdapter limbahAdapter;
    LongsorAdapter longsorAdapter;
    SedimentasiAdapter sedimentasiAdapter;
    PengendalianAdapter pengendalianAdapter;

    List<ErosiClass> erosiClasses;
    List<SampahClass> sampahClasses;
    List<BanjirClass> banjirs;
    List<EutrofikasiClass> eutrofikasiClasses;
    List<KebakaranClass> kebakaranClasses;
    List<LimbahClass> limbahClasses;
    List<LongsorClass> longsorClasses;
    List<SedimentasiClass> sedimentasiClasses;
    List<PengendalianClass> pengendalianClasses;

    Degradasi degradasi;
    String nama_jenis = "", id_jenis = "", id_subjenis = "";

    List<Degradasi> degradasis;

    //    Search Asset
    Animation out_anim, in_anim;
    Boolean isSearch = false;

    //    Menu Asset
    List<MenuClass> menuClassList;
    NavMenuAdapter adapter_navigation;
    PicassoLoader imageLoader;

    //    Filter Asset
    DialogFilter dialogFilter;

    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private ClusterManager<MarkerCluster> mClusterManager;

    Intent intentNext;
    LatLng myltlng;

    IntentFilter filter;


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(navigation)) {
            drawerLayout.closeDrawer(navigation);
        } else if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setHideable(true);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        } else {
            super.onBackPressed();
        }
    }

    private FusedLocationProviderClient mFusedLocationClient;
    private Location myLocation;

    private void setMap() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void initNewList() {
        erosiClasses = new ArrayList<>();
        sampahClasses = new ArrayList<>();
        banjirs = new ArrayList<>();
        eutrofikasiClasses = new ArrayList<>();
        kebakaranClasses = new ArrayList<>();
        limbahClasses = new ArrayList<>();
        longsorClasses = new ArrayList<>();
        sedimentasiClasses = new ArrayList<>();
        pengendalianClasses = new ArrayList<>();

        erosiPercikAdapter = new ErosiPercikAdapter(getContext(), erosiClasses);
        erosiAlurAdapter = new ErosiAlurAdapter(getContext(), erosiClasses);
        erosiLembarAdapter = new ErosiLembarAdapter(getContext(), erosiClasses);
        erosiPipaAdapter = new ErosiPipaAdapter(getContext(), erosiClasses);
        erosiTebingSungaiAdapter = new ErosiTebingSungaiAdapter(getContext(), erosiClasses);
        erosiParitAdapter = new ErosiParitAdapter(getContext(), erosiClasses);
        banjirAdapter = new BanjirAdapter(getContext(), banjirs);

        sampahAdapter = new SampahAdapter(getContext(), sampahClasses);
        eutrofikasiAdapter = new EutrofikasiAdapter(getContext(), eutrofikasiClasses);
        kebakaranAdapter = new KebakaranAdapter(getContext(), kebakaranClasses);
        limbahAdapter = new LimbahAdapter(getContext(), limbahClasses);
        longsorAdapter = new LongsorAdapter(getContext(), longsorClasses);
        sedimentasiAdapter = new SedimentasiAdapter(getContext(), sedimentasiClasses);
        pengendalianAdapter = new PengendalianAdapter(getContext(), pengendalianClasses);
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("telo", "onReceive: terima receiver");
            String action = intent.getAction();
            if (action.equals(LestariConstant.SP_REQ_DAS)) {
                Log.d("telogoreng", "onReceive: ");
                ListPolyLineClass listPolyLineClass = (ListPolyLineClass) intent.getSerializableExtra("polyline");
                initPolygon(listPolyLineClass);

                hideProsesDas();
                isDasShow = true;
                initDasButton();
            } else if (action.equals(LestariConstant.SP_FAILED_DAS)) {
                hideProsesDas();
                isDasShow = false;
                initDasButton();
            }
        }
    };


    private void initPolygon(ListPolyLineClass polyLineClasses){
        for (PolyLineClass polyLineClass : polyLineClasses.getClasses()){
            try {
                Log.d("dataz", "onReceive: ");
                PolygonOptions polygonOptions = new PolygonOptions();
                polygonOptions.strokeColor(Color.RED);
                polygonOptions.strokeWidth(2.0f);
                for (Titik titik : polyLineClass.getTitiks()){
                    Log.d("ehehe", "onReceive: "+Double.valueOf(titik.getLat())+" - "+new LatLng(Double.valueOf(titik.getLat()), Double.valueOf(titik.getLng())).longitude);
                    polygonOptions.add(new LatLng(Double.valueOf(titik.getLat()), Double.valueOf(titik.getLng())));
                }
                mMap.addPolygon(polygonOptions);
            } catch (NumberFormatException e){
                Log.d("errorz", "onReceive: ");
            }
        }
    }

    private void initDasButton(){
        if(isDasShow){
            img_das.setImageResource(R.drawable.ic_layers_clear_black_24dp);
        } else {
            img_das.setImageResource(R.drawable.ic_layers_black_24dp);
        }
    }

    private void initDas(){
        showProsesDas();
        if(myLocation!=null){
            Intent x = new Intent(LestariConstant.SP_REQ_DAS_INTERNET);
            x.putExtra("lat", myLocation.getLatitude());
            x.putExtra("lng", myLocation.getLongitude());
            getContext().sendBroadcast(x);
        } else {
            hideProsesDas();
            Lestari.alertWarning(getContext(), "GPS tidak aktif, Anda tidak dapat melakukan permintaan jalur DAS");
        }
    }

    private void showProsesDas(){
        pb_das.setVisibility(View.VISIBLE);
        card_das.setVisibility(View.GONE);
        lay_menunggudas.setVisibility(View.VISIBLE);
    }

    private void hideProsesDas(){
        pb_das.setVisibility(View.GONE);
        card_das.setVisibility(View.VISIBLE);
        lay_menunggudas.setVisibility(View.GONE);
    }

    private void hideDas(){
        mMap.clear();
        setAllMarker();
        isDasShow = false;
        initDasButton();
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myltlng, (float) Math.floor(mMap.getCameraPosition().zoom + 1)), 200, null);
    }


    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if(!Lestari.isMyServiceRunning(getContext(), MainService.class)){
            startService(new Intent(getContext(), MainService.class));
        }

        filter = new IntentFilter();
        filter.addAction(LestariConstant.SP_REQ_DAS);

        if(!Lestari.getSPBoolean(getContext(), LestariConstant.SP_INTRO_DAS)){
            Lestari.showIntroCase(MainActivity.this, card_das, getResources().getString(R.string.intro_das_title), getResources().getString(R.string.intro_das_content), false, new Lestari.OnEventChange() {
                @Override
                public void onAfterEvent() {
                    Lestari.setSPBoolean(getContext(), LestariConstant.SP_INTRO_DAS, true);
                }
            });
        }

        degradasis = new ArrayList<>();
        gson = new Gson();
        initNewList();
        setMap();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            LestariUi.showAlertCostum(getContext(), "GPS not active", "Aktifkan GPS agar fitur dapat berjalan dengan baik", "Aktifkan", R.drawable.gps_not_active, new LestariUi.OnClickListener() {
                @Override
                public void onClick(View v) {
                    askCompactPermission(PermissionKey.ACCESS_COARSE_LOCATION, new PermissionResultInterface() {
                        @Override
                        public void permissionGranted() {
                            try {
                                askCompactPermission(PermissionKey.ACCESS_FINE_LOCATION, new PermissionResultInterface() {
                                    @Override
                                    public void permissionGranted() {
                                        try {
                                            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
                                            mFusedLocationClient.getLastLocation()
                                                    .addOnSuccessListener(MainActivity.this, MainActivity.this);
                                        } catch (SecurityException ex) {
                                            ex.printStackTrace();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void permissionDenied() {

                                    }
                                });
                            } catch (SecurityException ex) {
                                ex.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void permissionDenied() {

                        }
                    });
                }
            });
        } else {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(MainActivity.this, MainActivity.this);
        }

        initRecyclerMenu();
        initBottomSheetView();


        card_nav.setOnClickListener(this);
        img_close.setOnClickListener(this);
        img_setting.setOnClickListener(this);
        img_pilihan.setOnClickListener(this);
        card_search.setOnClickListener(this);
        card_add.setOnClickListener(this);
        card_filter.setOnClickListener(this);
        card_kontribusi.setOnClickListener(this);
        card_das.setOnClickListener(this);
        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideKeyBoard();
                    keyword = edt_search.getText().toString();
                    if (mFusedLocationClient != null) {
                        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        }
                        mFusedLocationClient.getLastLocation()
                                .addOnSuccessListener(MainActivity.this, MainActivity.this);

                    }
                    return true;
                }
                return false;
            }
        });
        imageLoader = new PicassoLoader();


        avatarView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!Lestari.getSPString(getContext(), LestariConstant.SP_USER_PHOTO).equalsIgnoreCase("")){
                    photo_url = Lestari.getSPString(getContext(), LestariConstant.SP_USER_PHOTO).replace("http:", "https:");
                } else {
                    photo_url = "google.com";
                }

                if(dialogPhoto==null) {
                    dialogPhoto = new DialogPhoto(getContext(), photo_url, false, new DialogPhoto.OnPickPhoto() {
                        @Override
                        public void onPickPhoto() {

                        }
                    });
                } else {
                    dialogPhoto.updatePhoto(photo_url.replace("http:", "https:"));
                    dialogPhoto.show();
                }

            }
        });
        lay_profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), ProfilActivity.class));
            }
        });
    }


    private void hideKeyBoard(){
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edt_search.getWindowToken(), 0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(flag_filter){
            flag_filter = false;
            unregisterReceiver(receiver);
            hideProsesDas();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("photo", "onResume: " + Lestari.getSPString(getContext(), LestariConstant.SP_USER_PHOTO));
        AvatarPlaceholder refreshableAvatarPlaceholder = new AvatarPlaceholder(Lestari.getSPString(getContext(), LestariConstant.SP_USER_FULLNAME));
        try{
            imageLoader.loadImage(avatarView, refreshableAvatarPlaceholder, Lestari.getSPString(getContext(), LestariConstant.SP_USER_PHOTO).replace("http:", "https:"));
        }catch (IllegalArgumentException e){
            imageLoader.loadImage(avatarView, refreshableAvatarPlaceholder, "google.com");
        }
        tv_name.setText(Lestari.getSPString(getContext(), LestariConstant.SP_USER_FULLNAME));
        tv_kontribusi.setText(Lestari.getSPString(getContext(), LestariConstant.SP_USER_JML_KONTRIBUSI)+" Kontribusi");
        tv_degradasi.setText(Lestari.getSPString(getContext(), LestariConstant.SP_USER_JML_DEGRADASI)+" Degradasi");

        if(!flag_filter){
            flag_filter = true;
            registerReceiver(receiver, filter);
        }
    }

    private void setMarker(Location location, String a, Degradasi degradasi) {
        LatLng lokasi_tujuan = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();

        markerOptions.position(lokasi_tujuan).snippet(gson.toJson(degradasi));
        switch (a) {
            case LestariConstant.LONGSOR:
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_longsor, ((Activity) getContext()))));
                break;

            case LestariConstant.EROSI:
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_erosi, ((Activity) getContext()))));
                break;

            case LestariConstant.EUTROFIKASI:
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_eutrofikasi, ((Activity) getContext()))));
                break;

            case LestariConstant.KEBAKARAN:
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_erosi, ((Activity) getContext()))));

                break;

            case LestariConstant.SAMPAH:
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_erosi, ((Activity) getContext()))));
                break;

            case LestariConstant.LIMBAH:
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_limbah, ((Activity) getContext()))));
                break;

            case LestariConstant.SEDIMENTASI:
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_sedimentasi, ((Activity) getContext()))));
                break;

            case LestariConstant.BANJIR:
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(Lestari.getBitmap(R.drawable.ic_banjir, ((Activity) getContext()))));
                break;
        }
        mMap.addMarker(markerOptions);
    }

    private void initRoute() {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mClusterManager = new ClusterManager<MarkerCluster>(this, mMap);
//        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setOnCameraIdleListener(mClusterManager);
        mClusterManager.setCallbacks(new ClusterManager.Callbacks<MarkerCluster>() {
            @Override
            public boolean onClusterClick(@NonNull id.ac.ugm.glmb.lestari.ClusteringMap.Cluster<MarkerCluster> cluster) {
                if(cluster.getItems().size()<4){
                    Log.d("clusterClick", "onClusterClick: "+MapHelper.isZoomable(cluster.getItems()));
                    if(MapHelper.isZoomable(cluster.getItems())){
                        try {
                            LatLng ltlng = new LatLng(cluster.getLatitude(), cluster.getLongitude());
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ltlng, (float) Math.floor(mMap.getCameraPosition().zoom + 1)), 300, null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        dialogDegradasi = new DialogDegradasi(getContext(), cluster.getItems(), new DialogDegradasi.OnItemClick() {
                            @Override
                            public void onItemClick(View view, MarkerCluster markerCluster, int position) {
                                dialogDegradasi.dismiss();
                                degradasi = gson.fromJson(markerCluster.getSnippet(), Degradasi.class);
                                id_jenis = degradasi.getId();
                                nama_jenis = markerCluster.getTitle();
                                id_subjenis = degradasi.getNamaSubJenis();
                                doGetListDegradasi(LestariParam.stringDegradasi());


                                initRecyclerForAdapter(gson.fromJson(markerCluster.getSnippet(), Degradasi.class));

//                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                                bottomSheetBehavior.setHideable(false);
                                bottomSheetBehavior.setPeekHeight(lay_header_bottomsheet.getHeight());
                            }
                        });
                        dialogDegradasi.show();
                    }
                } else {
                    try {
                        LatLng ltlng = new LatLng(cluster.getLatitude(), cluster.getLongitude());
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ltlng, (float) Math.floor(mMap.getCameraPosition().zoom + 1)), 300, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return true;
            }

            @Override
            public boolean onClusterItemClick(@NonNull MarkerCluster clusterItem) {

                degradasi = gson.fromJson(clusterItem.getSnippet(), Degradasi.class);
                id_jenis = degradasi.getId();
                nama_jenis = clusterItem.getTitle();
                id_subjenis = degradasi.getNamaSubJenis();
                doGetListDegradasi(LestariParam.stringDegradasi());

                initRecyclerForAdapter(gson.fromJson(clusterItem.getSnippet(), Degradasi.class));

//                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                bottomSheetBehavior.setHideable(false);
                bottomSheetBehavior.setPeekHeight(lay_header_bottomsheet.getHeight());

                if(!Lestari.getSPBoolean(getContext(), LestariConstant.SP_INTRO_KONTRIBUSI_REDIRECT)){
                    Lestari.showIntroCase(MainActivity.this, img_direction, getResources().getString(R.string.intro_redirect_degradasi_title), getResources().getString(R.string.intro_redirect_degradasi_content), false, new Lestari.OnEventChange() {
                        @Override
                        public void onAfterEvent() {
                            Lestari.setSPBoolean(getContext(), LestariConstant.SP_INTRO_KONTRIBUSI_REDIRECT, true);
                        }
                    });
                }
                return true;
            }
        });
        final DefaultIconGenerator d = new DefaultIconGenerator(getContext());
        IconGenerator<MarkerCluster> markerClusterIconGenerator = new IconGenerator<MarkerCluster>() {
            @NonNull
            @Override
            public BitmapDescriptor getClusterIcon(@NonNull id.ac.ugm.glmb.lestari.ClusteringMap.Cluster<MarkerCluster> cluster) {
                return d.getClusterIcon(cluster);
            }

            @NonNull
            @Override
            public BitmapDescriptor getClusterItemIcon(@NonNull MarkerCluster clusterItem) {
                return clusterItem.getIcon();
            }
        };
        mClusterManager.setIconGenerator(markerClusterIconGenerator);



        /*coordinates = MapHelper.loadJSONFromAsset(getContext());
        for (int i = 0; i < 10; i++) {
            try {
                JSONObject koordinat = coordinates.getJSONObject(i);
                JSONArray koordinats = koordinat.getJSONArray("coordinates");
                if(MapHelper.polyLine(koordinats)!=null){
                    mMap.addPolygon(MapHelper.polyLine(koordinats));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }*/
    }

    @Override
    public void onSuccess(Location location) {
        if (location != null) {
            myLocation = location;
            LatLng lokasi_saya = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLng(lokasi_saya));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(9));
            requestDegradasi(LestariParam.stringDegradasi());
//            GoTro.rediretToMap(getContext(), myLocation, location_tujuan);
        }
    }

    private void showPopup(final View v, final Degradasi degradasi) {
        PopupMenu popup = new PopupMenu(getContext(), v);
        // Inflate the menu from xml
        if(degradasi.getId_user().equalsIgnoreCase(Lestari.getSPString(getContext(), LestariConstant.SP_USER_ID))){
            popup.getMenuInflater().inflate(R.menu.menu_download_delete, popup.getMenu());
        } else {
            popup.getMenuInflater().inflate(R.menu.menu_download, popup.getMenu());
        }

        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                        alert.setTitle("Menghapus Degradasi");
                        alert.setMessage("Apakah Anda yakin ingin menghapus degradasi ini?");
                        alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                deleteDegradasi(LestariParam.stringDegradasi());
                            }
                        });

                        alert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alert.show();
                        return true;
                    case R.id.action_download:
                        String url = LestariParam.a+"download.php?lestari_jenis_degradasi="+nama_jenis+"&lestari_id="+degradasi.getId();
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);

                        /*askCompactPermission(PermissionKey.WRITE_EXTERNAL_STORAGE, new PermissionResultInterface() {
                            @Override
                            public void permissionGranted() {
                                askCompactPermission(PermissionKey.READ_EXTERNAL_STORAGE, new PermissionResultInterface() {
                                    @Override
                                    public void permissionGranted() {
                                        if(!Lestari.isMyServiceRunning(getContext(), MainService.class)){
                                            getContext().startService(new Intent(getContext(), MainService.class));
                                        }
                                        Intent intent = new Intent("lestari.Download");
                                        intent.putExtra("id_degradasi", degradasi.getId());
                                        intent.putExtra("jenis_degradasi", nama_jenis);
                                        sendBroadcast(intent);
                                    }

                                    @Override
                                    public void permissionDenied() {

                                    }
                                });
                            }

                            @Override
                            public void permissionDenied() {

                            }
                        });*/
                        return true;
                    default:
                        return false;
                }
            }
        });
        // Handle dismissal with: popup.setOnDismissListener(...);
        // Show the menu
        popup.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.card_nav:
                drawerLayout.openDrawer(navigation);
                break;
            case R.id.img_setting:
                drawerLayout.closeDrawer(navigation);
                startActivity(new Intent(getContext(), PengaturanActivity.class));
                break;
            case R.id.img_pilihan:
                showPopup(img_pilihan, degradasi);
                break;
            case R.id.card_search:
                showSearchBar();
                break;
            case R.id.card_add:
                startActivity(new Intent(getContext(), TambahDegradasiActivity.class));
                break;
            case R.id.img_close:
                hideSearchBar();
                break;
            case R.id.card_kontribusi:
                if(degradasi!=null){
                    if(!degradasi.getIdjenis().equalsIgnoreCase(LestariConstant.PENGENDALIAN)){
                        initJenisToNext();
                    }
                }
                break;
            case R.id.card_das:
                if(isDasShow){
                    hideDas();
                } else {
                    initDas();
                }
                break;
            case R.id.card_filter:
                if (dialogFilter != null) {
                    dialogFilter.show();
                } else {
                    dialogFilter = new DialogFilter(getContext(), new DialogFilter.OnTerapkan() {
                        @Override
                        public void onTerapkan() {
                            edt_search.setText("");
                            keyword = "";
                            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                return;
                            }
                            if(mFusedLocationClient!=null){
                                mFusedLocationClient.getLastLocation()
                                        .addOnSuccessListener(MainActivity.this, MainActivity.this);

                            }
                        }
                    });
                }
                hideSearchBar();
                break;
        }
    }

    private void initJenisToNext(){
        switch (degradasi.getIdjenis()){
            case LestariConstant.EROSI:
                switch (degradasi.getNamaSubJenis()){
                    case LestariConstant.TEBING:
                        intentNext = new Intent(getContext(), TambahErosiTebingActivity.class);
                        break;
                    case LestariConstant.PERCIK:
                        intentNext = new Intent(getContext(), TambahErosiPercikActivity.class);
                        break;
                    case LestariConstant.PIPA:
                        intentNext = new Intent(getContext(), TambahErosiPipaActivity.class);
                        break;
                    case LestariConstant.LEMBAR:
                        intentNext = new Intent(getContext(), TambahErosiLembarActivity.class);
                        break;
                    case LestariConstant.ALUR:
                        intentNext = new Intent(getContext(), TambahErosiAlurActivity.class);
                        break;
                    case LestariConstant.PARIT:
                        intentNext = new Intent(getContext(), TambahErosiParitActivity.class);
                        break;
                    default:
                        intentNext = new Intent(getContext(), TambahErosiPercikActivity.class);
                        break;
                }
                break;
            case LestariConstant.BANJIR:
                intentNext = new Intent(getContext(), TambahBanjirActivity.class);
                break;
            case LestariConstant.EUTROFIKASI:
                intentNext = new Intent(getContext(), TambahEutrofikasiActivity.class);
                break;
            case LestariConstant.SEDIMENTASI:
                intentNext = new Intent(getContext(), TambahSedimentasiActivity.class);
                break;
            case LestariConstant.LONGSOR:
                intentNext = new Intent(getContext(), TambahLongsorActivity.class);
                break;
            case LestariConstant.LIMBAH:
                intentNext = new Intent(getContext(), TambahLimbahActivity.class);
                break;
            case LestariConstant.KEBAKARAN:
                intentNext = new Intent(getContext(), TambahKebakaranActivity.class);
                break;
            case LestariConstant.SAMPAH:
                intentNext = new Intent(getContext(), TambahSampahActivity.class);
                break;
            default:
                intentNext = new Intent(getContext(), TambahErosiPercikActivity.class);
                break;
        }
        intentNext.putExtra("degradasi", degradasi);
        startActivity(intentNext);
    }

    private void initRecyclerMenu(){
        menuClassList = MenuHelper.menuDefault();
        adapter_navigation = new NavMenuAdapter(getContext(), menuClassList, new NavMenuAdapter.OnAction() {
            @Override
            public void onAction(Context context) {
                drawerLayout.closeDrawer(navigation);
            }
        });
        rv_menu.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_menu.setAdapter(adapter_navigation);
    }

    private void updateRecyclerMenu(int total){
        menuClassList.clear();
        for(MenuClass menuClass : MenuHelper.menuCostum(total)){
            menuClassList.add(menuClass);
        }
        adapter_navigation.notifyDataSetChanged();
    }

    private void showSearchBar(){
        isSearch = true;
        in_anim = AnimationUtils.loadAnimation(getContext(), R.anim.pull_in_right);
        card_form_search.setAnimation(in_anim);
        card_form_search.setVisibility(View.VISIBLE);
    }

    private void hideSearchBar(){
        isSearch = false;
        out_anim = AnimationUtils.loadAnimation(getContext(), R.anim.push_out_right);
        if(!edt_search.getText().toString().equalsIgnoreCase("")){
            edt_search.setText("");
            hideKeyBoard();
        } else {
            card_form_search.setAnimation(out_anim);
            card_form_search.setVisibility(View.GONE);
        }
    }

    private void initBottomSheetView(){
        bottomSheetBehavior = BottomSheetBehavior.from(lay_bottomsheet_degradasi);
        bottomSheetBehavior.setPeekHeight(0);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                bottomSheetBehavior.setPeekHeight(0);
//                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                if(newState==BottomSheetBehavior.STATE_EXPANDED){
//                    img_bottomsheet_controller.setImageResource(R.drawable.ic_arrow_drop_down_black_24dp);
                    card_kontribusi.setVisibility(View.VISIBLE);
                    card_kontribusi.animate().scaleX(1).scaleY(1).setDuration(300).start();

                    if(!Lestari.getSPBoolean(getContext(), LestariConstant.SP_INTRO_KONTRIBUSI_ADD)){
                        Lestari.showIntroCase(MainActivity.this, card_kontribusi, getResources().getString(R.string.intro_tambah_kontribusi_title), getResources().getString(R.string.intro_tambah_kontribusi_content), false, new Lestari.OnEventChange() {
                            @Override
                            public void onAfterEvent() {
                                Lestari.setSPBoolean(getContext(), LestariConstant.SP_INTRO_KONTRIBUSI_ADD, true);
                            }
                        });
                    }
                } else {
                    card_kontribusi.animate().scaleX(0).scaleY(0).setDuration(300).start();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//                Log.d("tesBottom", "onSlideOffset: "+slideOffset);
                /*bottomSheetBehavior.setPeekHeight(0);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);*/
            }
        });

        rv_bottomsheet.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_bottomsheet.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                int action = e.getAction();
                switch (action) {
                    case MotionEvent.ACTION_MOVE:
                        rv.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    private void initRecyclerForAdapter(Degradasi degradasi){
        final Location location = new Location("");
        location.setLatitude(Double.valueOf(degradasi.getLat()));
        location.setLongitude(Double.valueOf(degradasi.getLng()));


        lay_bottomsheet_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Lestari.rediretToMap(getContext(), myLocation, location);
            }
        });
        String jenis = "";
        String subjenis = "";
        try {
            jenis = degradasi.getIdjenis().substring(0, 1).toUpperCase()+degradasi.getIdjenis().substring(1).toUpperCase();
            if(!degradasi.getIdjenis().equalsIgnoreCase("")){
                subjenis = degradasi.getNamaSubJenis().substring(0, 1).toUpperCase()+degradasi.getNamaSubJenis().substring(1).toUpperCase();
            }
        } catch (IndexOutOfBoundsException e) {

        }
        tv_bottomsheet_title.setText(jenis+" "+subjenis);
        tv_bottomsheet_direction.setText(degradasi.getAlamat());
        String jn_lokasi = "-";
        String das = "";

        if(!degradasi.getJenisLokasi().equalsIgnoreCase("")){
            jn_lokasi = degradasi.getJenisLokasi();
        }

        if(!degradasi.getDas().equalsIgnoreCase("")){
            das = "-"+degradasi.getDas();
        }

        tv_bottomsheet_jenislokasi.setText("("+jn_lokasi+das+")");

    }

    List<MarkerCluster> list;
    private void setAllMarker(){
        list = new ArrayList<>();
        for (int i = 0; i < degradasis.size(); i++) {
            try {
                MarkerCluster offsetItem = new MarkerCluster(Double.valueOf(degradasis.get(i).getLat()), Double.valueOf(degradasis.get(i).getLng()), degradasis.get(i).getIdjenis(), degradasis.get(i), getContext());
                list.add(offsetItem);
            } catch (NullPointerException e){

            } catch (NumberFormatException e){

            }
        }
        mClusterManager.setItems(list);

        if(degradasis.size() > 0) {
            rangeAllPlaces(degradasis);
        }
    }

    private void rangeAllPlaces(List<Degradasi> degradasis) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Degradasi marker : degradasis) {
            builder.include(new LatLng(Double.valueOf(marker.getLat()), Double.valueOf(marker.getLng())));
        }
        LatLngBounds bounds = builder.build();
        int padding = 0;

        try {
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
        } catch (java.lang.Exception e) {

        }
    }

    private void requestDegradasi(String URL){
        LestariRequest.POST(URL, getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                pb_map.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(JSONObject response) {
                pb_map.setVisibility(View.GONE);
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        degradasis.clear();
                        mMap.clear();

                        for(Degradasi degradasi : MapHelper.listDegradasi(response)){
                            degradasis.add(degradasi);
                        }

                        setAllMarker();

                        /*try {
                            myltlng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myltlng, (float) Math.floor(mMap.getCameraPosition().zoom + 1)), 200, null);
                        } catch (NullPointerException e){

                        }*/
                    }else {
                        Lestari.alertWarning(getContext(), getResources().getString(R.string.txt_no_data));
//                        LestariUi.ToastShort(LoginActivity.this, getResources().getString(R.string.login_failed));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                pb_map.setVisibility(View.GONE);
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                //param.put("access_token", SafeTravel.stringPintu());
                param.put("lestari_titik_lokasi", ""+myLocation.getLatitude()+","+myLocation.getLongitude());
                param.put("lestari_token_check", Lestari.getSPString(getContext(), LestariConstant.SP_USER_TOKEN));

                param.put("lestari_isErosi", ""+Lestari.getSPBooleanTrue(getContext(), LestariConstant.SP_EROSI));
                param.put("lestari_isLongsor", ""+Lestari.getSPBooleanTrue(getContext(), LestariConstant.SP_LONGSOR));
                param.put("lestari_isSedimentasi", ""+Lestari.getSPBooleanTrue(getContext(), LestariConstant.SP_SEDIMENTASI));
                param.put("lestari_isEutrofikasi", ""+Lestari.getSPBooleanTrue(getContext(), LestariConstant.SP_EUTROFIKASI));
                param.put("lestari_isSampah", ""+Lestari.getSPBooleanTrue(getContext(), LestariConstant.SP_SAMPAH));
                param.put("lestari_isLimbah", ""+Lestari.getSPBooleanTrue(getContext(), LestariConstant.SP_LIMBAH));
                param.put("lestari_isBanjir", ""+Lestari.getSPBooleanTrue(getContext(), LestariConstant.SP_BANJIR));
                param.put("lestari_isKebakaran", ""+Lestari.getSPBooleanTrue(getContext(), LestariConstant.SP_KEBAKARAN));
                param.put("lestari_isPengendalian", ""+Lestari.getSPBooleanTrue(getContext(), LestariConstant.SP_PENGENDALIAN));
                param.put("lestari_city", ""+Lestari.getSPString(getContext(), LestariConstant.SP_CITY_FILTER));
                param.put("lestari_kata_kunci", ""+keyword);
                param.put("degradasi_only", "true");

                if(keyword.equalsIgnoreCase("")){
                    param.put("lestari_all", "true");
                } else {
                    param.put("lestari_cari", "true");
                }

                if(Lestari.getSPBoolean(getContext(), LestariConstant.SP_FILTER_USER))
                    param.put("lestari_id_user", Lestari.getSPString(getContext(), LestariConstant.SP_USER_ID));
                System.out.println(param);
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }
        });
    }

    private void doGetListDegradasi(String URL){
        LestariRequest.POST(URL, getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                helper_loading_box.setVisibility(View.VISIBLE);
                helper_empty.setVisibility(View.GONE);
                helper_noconnection.setVisibility(View.GONE);
                setEmptyAdapter();
            }

            @Override
            public void onSuccess(JSONObject response) {
                helper_loading_box.setVisibility(View.GONE);
                helper_empty.setVisibility(View.GONE);
                helper_noconnection.setVisibility(View.GONE);
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        JSONArray data = response.getJSONArray("data");
                        updateTampilan(data);
                    }else if(response.getInt("code")==LestariConstant.CODE_SESSION_ERROR){
                        LestariUtil.emptyUser(getContext(), new LestariUi.OnEventChange() {
                            @Override
                            public void onChange() {
                                LestariUi.ToastShort(getContext(), getResources().getString(R.string.text_session_alert));
                                finish();
                                startActivity(new Intent(getContext(), LoginActivity.class));
                            }
                        });
                    } else {
                        Lestari.alertWarning(getContext(), getResources().getString(R.string.txt_nodata)+", "+response.getString("message"));
                        helper_empty.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                helper_loading_box.setVisibility(View.GONE);
                helper_empty.setVisibility(View.GONE);
                helper_noconnection.setVisibility(View.VISIBLE);
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("lestari_token_check", Lestari.getSPString(getContext(), LestariConstant.SP_USER_TOKEN));
                param.put("lestari_detail", "true");
//                param.put("lestari_kontribusi", "true");
                param.put("lestari_id", id_jenis);
                param.put("lestari_jenis_degradasi", nama_jenis);
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> header_param = new HashMap<String, String>();
                return header_param;
            }
        });
    }

    private void setEmptyAdapter(){
        erosiClasses.clear();
        sampahClasses.clear();
        banjirs.clear();
        eutrofikasiClasses.clear();
        kebakaranClasses.clear();
        limbahClasses.clear();
        longsorClasses.clear();
        sedimentasiClasses.clear();
        pengendalianClasses.clear();

        erosiPercikAdapter.notifyDataSetChanged();
        erosiAlurAdapter.notifyDataSetChanged();
        erosiLembarAdapter.notifyDataSetChanged();
        erosiParitAdapter.notifyDataSetChanged();
        erosiPipaAdapter.notifyDataSetChanged();
        erosiTebingSungaiAdapter.notifyDataSetChanged();
        banjirAdapter.notifyDataSetChanged();
        eutrofikasiAdapter.notifyDataSetChanged();
        kebakaranAdapter.notifyDataSetChanged();
        limbahAdapter.notifyDataSetChanged();
        sampahAdapter.notifyDataSetChanged();
        sedimentasiAdapter.notifyDataSetChanged();
        pengendalianAdapter.notifyDataSetChanged();
    }

    private void updateTampilan(JSONArray jsonArray){
        Log.d("subjenis", "updateTampilan: "+nama_jenis);
        switch (nama_jenis){
            case LestariConstant.EROSI :
                for (ErosiClass erosiClass : DegradasiServiceHelper.listErosi(jsonArray)){
                    erosiClasses.add(erosiClass);
                }
                Log.d("idsubjenis", "updateTampilan: "+id_subjenis);
                switch (id_subjenis){
                    case LestariConstant.ALUR:
                        Log.d("erosiclass", "updateTampilan: "+erosiClasses.size());
                        rv_bottomsheet.setAdapter(erosiAlurAdapter);
                        erosiAlurAdapter.notifyDataSetChanged();
                        break;
                    case LestariConstant.LEMBAR:
                        rv_bottomsheet.setAdapter(erosiLembarAdapter);
                        erosiLembarAdapter.notifyDataSetChanged();
                        break;
                    case LestariConstant.PARIT:
                        rv_bottomsheet.setAdapter(erosiParitAdapter);
                        erosiParitAdapter.notifyDataSetChanged();
                        break;
                    case LestariConstant.PERCIK:
                        rv_bottomsheet.setAdapter(erosiPercikAdapter);
                        erosiPercikAdapter.notifyDataSetChanged();
                        break;
                    case LestariConstant.PIPA:
                        rv_bottomsheet.setAdapter(erosiPipaAdapter);
                        erosiPipaAdapter.notifyDataSetChanged();
                        break;
                    case LestariConstant.TEBING:
                        rv_bottomsheet.setAdapter(erosiTebingSungaiAdapter);
                        erosiTebingSungaiAdapter.notifyDataSetChanged();
                        break;
                }
                break;
            case LestariConstant.BANJIR :
                for (BanjirClass banjirClass : DegradasiServiceHelper.listBanjir(jsonArray)){
                    banjirs.add(banjirClass);
                }
                rv_bottomsheet.setAdapter(banjirAdapter);
                banjirAdapter.notifyDataSetChanged();
                break;
            case LestariConstant.EUTROFIKASI :
                for (EutrofikasiClass eutrofikasiClass : DegradasiServiceHelper.listEutrofikasi(jsonArray)){
                    eutrofikasiClasses.add(eutrofikasiClass);
                }
                rv_bottomsheet.setAdapter(eutrofikasiAdapter);
                eutrofikasiAdapter.notifyDataSetChanged();
                break;
            case LestariConstant.KEBAKARAN :
                for (KebakaranClass kebakaranClass : DegradasiServiceHelper.listKebakaran(jsonArray)){
                    kebakaranClasses.add(kebakaranClass);
                }
                rv_bottomsheet.setAdapter(kebakaranAdapter);
                kebakaranAdapter.notifyDataSetChanged();
                break;
            case LestariConstant.LIMBAH :
                for (LimbahClass classs : DegradasiServiceHelper.listLimbah(jsonArray)){
                    limbahClasses.add(classs);
                }
                rv_bottomsheet.setAdapter(limbahAdapter);
                limbahAdapter.notifyDataSetChanged();
                break;
            case LestariConstant.LONGSOR :
                for (LongsorClass classs : DegradasiServiceHelper.listLongsor(jsonArray)){
                    longsorClasses.add(classs);
                }
                rv_bottomsheet.setAdapter(longsorAdapter);
                longsorAdapter.notifyDataSetChanged();
                break;
            case LestariConstant.SAMPAH :
                for (SampahClass classs : DegradasiServiceHelper.listSampah(jsonArray)){
                    sampahClasses.add(classs);
                }
                rv_bottomsheet.setAdapter(sampahAdapter);
                sampahAdapter.notifyDataSetChanged();
                break;
            case LestariConstant.SEDIMENTASI :
                for (SedimentasiClass classs : DegradasiServiceHelper.listSedimentasi(jsonArray)){
                    sedimentasiClasses.add(classs);
                }
                rv_bottomsheet.setAdapter(sedimentasiAdapter);
                sedimentasiAdapter.notifyDataSetChanged();
                break;
            case LestariConstant.PENGENDALIAN :
                for (PengendalianClass classs : DegradasiServiceHelper.listPengendalian(jsonArray)){
                    pengendalianClasses.add(classs);
                }
                rv_bottomsheet.setAdapter(pengendalianAdapter);
                pengendalianAdapter.notifyDataSetChanged();
                break;
        }
    }

    private void deleteDegradasi(String URL){
        LestariRequest.POST(URL, getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                LestariUi.ShowLoadingDialog(getContext());
            }

            @Override
            public void onSuccess(JSONObject response) {
                LestariUi.HideLoadingDialog(getContext());
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        LestariUi.ToastShort(getContext(), getResources().getString(R.string.txt_success_delete));
                        bottomSheetBehavior.setHideable(true);
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        if(mFusedLocationClient!=null){
                            mFusedLocationClient.getLastLocation()
                                    .addOnSuccessListener(MainActivity.this, MainActivity.this);

                        }
                    }else {
                        Lestari.alertWarning(getContext(), getResources().getString(R.string.txt_error));
//                        LestariUi.ToastShort(LoginActivity.this, getResources().getString(R.string.login_failed));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                LestariUi.HideLoadingDialog(getContext());
                Lestari.alertWarning(getContext(), getResources().getString(R.string.txt_error_delete));
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                //param.put("access_token", SafeTravel.stringPintu());
                param.put("lestari_token_check", Lestari.getSPString(getContext(), LestariConstant.SP_USER_TOKEN));

                param.put("lestari_delete", "true");
                param.put("lestari_id", ""+degradasi.getId());
                param.put("lestari_jenis_degradasi", ""+nama_jenis);
                System.out.println(param);
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }
        });
    }

}
