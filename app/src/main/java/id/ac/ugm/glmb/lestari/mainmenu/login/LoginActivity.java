package id.ac.ugm.glmb.lestari.mainmenu.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.MainActivity;
import id.ac.ugm.glmb.lestari.mainmenu.lainnya.AppIntroActivity;
import id.ac.ugm.glmb.lestari.mainmenu.register.RegisterActivity;
import id.ac.ugm.glmb.lestari.mainmenu.register.RegisterExternalActivity;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariParam;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

public class LoginActivity extends ActivityGeneral implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    @BindView(R.id.card_login) protected CardView card_login;
    @BindView(R.id.tv_daftar) protected TextView tv_daftar;
    @BindView(R.id.edt_username) protected EditText edt_username;
    @BindView(R.id.edt_password) protected EditText edt_password;
    @BindView(R.id.tv_lupapassword) protected TextView tv_lupapassword;

    @BindView(R.id.lay_login_facebook) protected CardView lay_login_facebook;
    @BindView(R.id.lay_login_google) protected CardView lay_login_google;

    String username, password;

    private static final int RC_SIGN_IN = 9001;

    private GoogleApiClient mGoogleApiClient;
    private LoginModel loginModel;
    GoogleSignInClient mGoogleSignInClient;
    GoogleSignInAccount acct;
    Intent signInIntent;

    private CallbackManager mCallbackManager;

    int LOGIN_TYPE = 0, FB_TYPE = 1, GOOGLE_TYPE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


        initLoginGoogle();
        initLoginFacebok();

        YoYo.with(Techniques.FadeIn)
                .duration(1500)
                .repeat(0)
                .playOn(findViewById(R.id.img_logo));

        card_login.setOnClickListener(this);
        tv_daftar.setOnClickListener(this);
        tv_lupapassword.setOnClickListener(this);
        lay_login_facebook.setOnClickListener(this);
        lay_login_google.setOnClickListener(this);
    }

    private void initLoginGoogle(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(LestariParam.google_credential())
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void initLoginFacebok(){
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("facebook", "Facebook onScuccess: ");
                handelFacebook(loginResult);
            }

            @Override
            public void onCancel() {
                Log.d("facebook", "Facebook onCancel: ");
                // [START_EXCLUDE]
                // [END_EXCLUDE]
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("facebook", "Facebook onError: ");
                // [START_EXCLUDE]
                // [END_EXCLUDE]
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.card_login:
                validateData();
                break;
            case R.id.tv_daftar:
                startActivity(new Intent(getContext(), RegisterActivity.class));
                break;
            case R.id.tv_lupapassword:
                startActivity(new Intent(getContext(), LupaPasswordActivity.class));
                break;
            case R.id.lay_login_facebook:
                LOGIN_TYPE = FB_TYPE;
                LoginManager.getInstance()
                        .logInWithReadPermissions(LoginActivity.this, Arrays.asList("email", "public_profile"));
                break;
            case R.id.lay_login_google:
                LOGIN_TYPE = GOOGLE_TYPE;
                signOutGoogle();
                break;
        }
    }

    private void handelFacebook(LoginResult  loginResult) {
//        Log.d("facebook", "handelFacebook: "+token);
        // [START_EXCLUDE silent]
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.v("LoginActivity", response.toString());
                        initFbToModel(object);
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void doLoginExternal(String URL){
        LestariRequest.POST(URL, LoginActivity.this, new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                LestariUi.ShowLoadingDialog(LoginActivity.this);
            }

            @Override
            public void onSuccess(JSONObject response) {
                LestariUi.HideLoadingDialog(LoginActivity.this);
                try {
                    if(response.getInt("code")==LestariConstant.CODE_SUCCESS){
                        if(LoginJsonHelper.isUserValid(LoginActivity.this, response)){
                            LestariUi.ToastShort(LoginActivity.this, getResources().getString(R.string.login_success));
                            FirebaseMessaging.getInstance().subscribeToTopic(Lestari.getSPString(getContext(), LestariConstant.SP_USER_ID));
                            if(Lestari.getSPBoolean(getContext(), LestariConstant.SP_INTRO)){
                                startActivity(new Intent(LoginActivity.this, MainActivity.class).putExtra("isLogin", true));
                            } else {
                                startActivity(new Intent(LoginActivity.this, AppIntroActivity.class));
                            }
                            finish();
                        }else{
                            Lestari.alertWarning(LoginActivity.this, getResources().getString(R.string.login_failed));
                        }
                    }else if(response.getInt("code")==LestariConstant.CODE_FAILED){
                        Lestari.alertWarning(LoginActivity.this, "Anda belum terdaftar");
                        Intent intent = new Intent(getContext(), RegisterExternalActivity.class);
                        intent.putExtra("loginModel", loginModel);
                        startActivity(intent);
                    }else if(response.getInt("code")==LestariConstant.CODE_CONFIRM_ERROR){
                        Lestari.alertWarning(LoginActivity.this, "Anda belum mengkonfirmasi email yang didaftarkan");
                    }else if(response.getInt("code")==6){
                        Lestari.alertWarning(LoginActivity.this, "Akun Anda belum divalidasi oleh Admin kami, mohon tunggu");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                LestariUi.HideLoadingDialog(LoginActivity.this);
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                //param.put("access_token", SafeTravel.stringPintu());
                param.put("lestari_type_hash", loginModel.getuId());
                param.put("lestari_email", loginModel.getEmail());
                param.put("type_login", ""+1);
                System.out.println(param);
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }
        });
    }

    private void doLogin(String URL){
        LestariRequest.POST(URL, LoginActivity.this, new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                LestariUi.ShowLoadingDialog(LoginActivity.this);
            }

            @Override
            public void onSuccess(JSONObject response) {
                LestariUi.HideLoadingDialog(LoginActivity.this);
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        if(LoginJsonHelper.isUserValid(LoginActivity.this, response)){
                            LestariUi.ToastShort(LoginActivity.this, getResources().getString(R.string.login_success));
                            FirebaseMessaging.getInstance().subscribeToTopic(Lestari.getSPString(getContext(), LestariConstant.SP_USER_ID));
                            if(Lestari.getSPBoolean(getContext(), LestariConstant.SP_INTRO)){
                                startActivity(new Intent(LoginActivity.this, MainActivity.class).putExtra("isLogin", true));
                            } else {
                                startActivity(new Intent(LoginActivity.this, AppIntroActivity.class));
                            }
                            finish();
                        }else{
                            Lestari.alertWarning(getContext(), getResources().getString(R.string.login_failed));
                        }
                    }else if(response.getInt("code")==LestariConstant.CODE_FAILED){
                        Lestari.alertWarning(getContext(), "Password/username Anda salah");
                    }else if(response.getInt("code")==LestariConstant.CODE_CONFIRM_ERROR){
                        Lestari.alertWarning(getContext(), "Anda belum mengkonfirmasi email yang didaftarkan");
                    }else {
                        Lestari.alertWarning(getContext(), getResources().getString(R.string.login_failed));
//                        LestariUi.ToastShort(LoginActivity.this, getResources().getString(R.string.login_failed));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                LestariUi.HideLoadingDialog(LoginActivity.this);
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                //param.put("access_token", SafeTravel.stringPintu());
                param.put("lestari_username", username);
                param.put("lestari_password", password);
                param.put("level", "user");
                param.put("type_login", ""+1);
                System.out.println(param);
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }
        });
    }

    private void validateData(){
        if(edt_username.getText().toString().length()==0){
            edt_username.setError(getResources().getString(R.string.email_empty));
        }else if(edt_password.getText().toString().length()==0){
            edt_password.setError(getResources().getString(R.string.password_empty));
        }else{
            username = edt_username.getText().toString();
            password = edt_password.getText().toString();
            doLogin(LestariParam.stringLogin());
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                Log.d("google login", "onActivityResult: success");
                GoogleSignInAccount account = result.getSignInAccount();
                acct = GoogleSignIn.getLastSignedInAccount(LoginActivity.this);
                initDataToModel(account);

            } else {
                Log.d("Google", "Gagal "+result.getStatus().getStatusMessage());
                Log.d("gagal", "onActivityResult: success");
                Lestari.alertWarning(getContext(), getResources().getString(R.string.txt_network_error));
            }
        } else{
            if(LOGIN_TYPE == FB_TYPE){
                mCallbackManager.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private void initDataToModel(GoogleSignInAccount account){
        loginModel = new LoginModel();
        loginModel.setuId(account.getId());
        loginModel.setEmail(account.getEmail());
        loginModel.setName(account.getDisplayName());
        loginModel.setPhoto(String.valueOf(account.getPhotoUrl()));

        Log.d("google", "initDataToModel: "+account.getEmail());
        doLoginExternal(LestariParam.stringLoginEksternal());
    }

    private void initFbToModel(JSONObject object){
        loginModel = new LoginModel();
        try {
            loginModel.setuId(object.getString("id"));
            loginModel.setEmail(object.getString("email"));
            loginModel.setName(object.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doLoginExternal(LestariParam.stringLoginEksternal());
    }

    private void googleLogin(){
        signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOutGoogle() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        googleLogin();
                    }
                });
    }
}
