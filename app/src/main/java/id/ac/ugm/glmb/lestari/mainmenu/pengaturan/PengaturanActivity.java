package id.ac.ugm.glmb.lestari.mainmenu.pengaturan;

import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.google.firebase.messaging.FirebaseMessaging;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;

public class PengaturanActivity extends ActivityGeneral {
    @BindView(R.id.switch_pemberitahuan)
    protected Switch switch_pemberitahuan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan);
        ButterKnife.bind(this);

        ToolbarLestari toolbarLestari = new ToolbarLestari(getContext(), "Pengaturan");
        initSwitch();

        switch_pemberitahuan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!Lestari.getSPBooleanTrue(getContext(), LestariConstant.SP_PENGATURAN_NOTIFIKASI)) {
                    Lestari.setSPBoolean(PengaturanActivity.this, LestariConstant.SP_PENGATURAN_NOTIFIKASI, true);
                    switch_pemberitahuan.setChecked(true);
                    FirebaseMessaging.getInstance().subscribeToTopic(Lestari.getSPString(getContext(), LestariConstant.SP_USER_ID));
                }else{
                    Lestari.setSPBoolean(PengaturanActivity.this, LestariConstant.SP_PENGATURAN_NOTIFIKASI, false);
                    switch_pemberitahuan.setChecked(false);
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(Lestari.getSPString(getContext(), LestariConstant.SP_USER_ID));
                }
            }
        });
    }

    private void initSwitch(){
        if (Lestari.getSPBooleanTrue(this, LestariConstant.sp_notation)) {
            switch_pemberitahuan.setChecked(true);
        }else{
            switch_pemberitahuan.setChecked(false);
        }
    }
}
