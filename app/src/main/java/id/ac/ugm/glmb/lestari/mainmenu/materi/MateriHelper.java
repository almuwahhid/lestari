package id.ac.ugm.glmb.lestari.mainmenu.materi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 7/25/18.
 */

public class MateriHelper {
    public static List<MateriClass> listMateri(JSONArray jsonArray){
        List<MateriClass> listMateri = new ArrayList<>();
        try{
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                MateriClass materiClass = new MateriClass();
                materiClass.setId(jsonObject.getString("id_materi"));
                materiClass.setJudul(jsonObject.getString("judul"));
                materiClass.setContent(jsonObject.getString("isi"));
                materiClass.setYoutube(jsonObject.getString("url_youtube"));
                listMateri.add(materiClass);
            }
        }catch (JSONException e){

        }
        return listMateri;
    }
}
