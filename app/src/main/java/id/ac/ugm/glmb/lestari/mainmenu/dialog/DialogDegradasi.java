package id.ac.ugm.glmb.lestari.mainmenu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.DialogListAdapter;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.helper.MarkerCluster;

/**
 * Created by root on 8/1/18.
 */

public class DialogDegradasi {

    Context context;
    Dialog dialog;
    List<MarkerCluster> lists;
    RecyclerView recyclerView;

    DialogListAdapter listAdapter;

    OnItemClick onItemClick;

    public DialogDegradasi(Context context, List<MarkerCluster> lists, OnItemClick onItemClick) {
        this.context = context;
        this.lists = lists;
        this.onItemClick = onItemClick;

        initComponent();
    }

    private void initComponent(){
        dialog = new Dialog(context);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimHorizontal;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.lay_list);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        initContext();
    }

    private void initContext(){
        listAdapter = new DialogListAdapter(context, lists);

        recyclerView = dialog.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(listAdapter);
        listAdapter.setOnClickListener(new DialogListAdapter.OnClickListener() {
            @Override
            public void onClick(View v, MarkerCluster markerCluster, int position) {
                onItemClick.onItemClick(v, markerCluster, position);
                dismiss();
            }
        });
    }

    public void dismiss(){
        if(dialog!= null)
            this.dialog.dismiss();
    }

    public void show(){
        this.dialog.show();
    }

    public interface OnItemClick{
        void onItemClick(View view, MarkerCluster markerCluster, int position);
    }
}
