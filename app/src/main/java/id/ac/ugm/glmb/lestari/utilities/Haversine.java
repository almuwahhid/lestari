package id.ac.ugm.glmb.lestari.utilities;

import android.util.Log;

/**
 * Created by root on 8/1/18.
 */

public class Haversine {
    private static final int EARTH_RADIUS = 6371; // Approx Earth radius in KM

    public static double distance(double startLat, double startLong,
                                  double endLat, double endLong) {

        double dLat  = Math.toRadians((endLat - startLat));
        double dLong = Math.toRadians((endLong - startLong));

        startLat = Math.toRadians(startLat);
        endLat   = Math.toRadians(endLat);

        double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        /*Log.d("haversine", "haversin: "+(EARTH_RADIUS * c));
        Log.d("haversine", "lat: "+startLat+" "+endLat);
        Log.d("haversine", "lng: "+startLong+" "+endLong);*/
        return EARTH_RADIUS * c; // <-- d
    }

    public static double haversin(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }
}
