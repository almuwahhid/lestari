package id.ac.ugm.glmb.lestari.mainmenu.degradasi.pengendalian;

import java.io.Serializable;
import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.degradasi.GaleriDegradasi;

/**
 * Created by root on 7/28/18.
 */

public class PengendalianClass implements Serializable{
    String id="", id_user="", time="", date="", parent_id="", deskripsi="", jenisPengendalian="", kegiatanPengendalian="", name ="", tahun_pelaksanaan = "";

    public String getTahun_pelaksanaan() {
        return tahun_pelaksanaan;
    }

    public void setTahun_pelaksanaan(String tahun_pelaksanaan) {
        this.tahun_pelaksanaan = tahun_pelaksanaan;
    }

    public List<GaleriDegradasi> galleries;

    public List<GaleriDegradasi> getGalleries() {
        return galleries;
    }

    public void setGalleries(List<GaleriDegradasi> galleries) {
        this.galleries = galleries;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getJenisPengendalian() {
        return jenisPengendalian;
    }

    public void setJenisPengendalian(String jenisPengendalian) {
        this.jenisPengendalian = jenisPengendalian;
    }

    public String getKegiatanPengendalian() {
        return kegiatanPengendalian;
    }

    public void setKegiatanPengendalian(String kegiatanPengendalian) {
        this.kegiatanPengendalian = kegiatanPengendalian;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
