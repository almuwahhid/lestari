package id.ac.ugm.glmb.lestari.mainmenu.materi;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;

public class MateriAdapter extends RecyclerView.Adapter<MateriAdapter.MyHolder>  {

    Context context;
    List<MateriClass> classes;

    public MateriAdapter(Context context, List<MateriClass> classes) {
        this.context = context;
        this.classes = classes;
    }

    public MateriAdapter() {

    }

    @NonNull
    @Override
    public MateriAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_materi_adapter, parent, false);
        MyHolder rcv = new MyHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(@NonNull MateriAdapter.MyHolder holder, int position) {
        final MateriClass materiClass = classes.get(position);
        holder.tv_title.setText(materiClass.getJudul());
        holder.tv_content.setText(materiClass.getContent());
        String youtube = "";
        try {
            youtube = materiClass.getYoutube().split("v=")[1];
        } catch (IndexOutOfBoundsException e){
            youtube = "";
        }
        Picasso.with(context)
                .load("https://img.youtube.com/vi/"+youtube+"/0.jpg")
                .placeholder(R.drawable.loading)
                .error(R.drawable.no_data_available)
                .into(holder.img_thumbnail);

        String html = LestariConstant.WEBVIEW_STYLE_MATERI;
        String mime = "text/html";
        String encoding = "utf-8";

        holder.wv_content.loadData(String.format(html, materiClass.getContent()), mime, encoding);

        holder.wv_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailMateriActivity.class);
                intent.putExtra("materi", materiClass);
                context.startActivity(intent);
            }
        });

        holder.card_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailMateriActivity.class);
                intent.putExtra("materi", materiClass);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return classes.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title)
        TextView tv_title;
        @BindView(R.id.card_layout)
        CardView card_layout;
        @BindView(R.id.wv_content)
        WebView wv_content;

        @BindView(R.id.tv_content)
        TextView tv_content;
        @BindView(R.id.img_thumbnail)
        ImageView img_thumbnail;
        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
