package id.ac.ugm.glmb.lestari.coremenu.mainPage.helper;

import android.app.Activity;
import android.content.Intent;

import id.ac.ugm.glmb.lestari.coremenu.mainPage.NavMenuAdapter;

/**
 * Created by root on 6/18/18.
 */

public class MenuClass {
    int drawable_item;
    String text;
    Boolean isAction;
    int total;
    NavMenuAdapter.OnAction onAction;

    public MenuClass(int drawable_item, String text, Boolean isAction, int total, NavMenuAdapter.OnAction onAction) {
        this.drawable_item = drawable_item;
        this.text = text;
        this.isAction = isAction;
        this.total = total;
        this.onAction = onAction;
    }

    public NavMenuAdapter.OnAction getOnAction() {
        return onAction;
    }

    public int getDrawable_item() {
        return drawable_item;
    }

    public String getText() {
        return text;
    }

    public Boolean getAction() {
        return isAction;
    }

    public int getTotal() {
        return total;
    }
}
