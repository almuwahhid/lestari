package id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.GaleriDegradasi;

public class PostingPhotoAdapter extends RecyclerView.Adapter<PostingPhotoAdapter.MyHolder> {
    Context context;
    List<GaleriDegradasi> list;
    PostingDegradasiHelper.OnClickGallery onClickGallery;

    public PostingPhotoAdapter(Context context, List<GaleriDegradasi> list, PostingDegradasiHelper.OnClickGallery onClickGallery) {
        this.context = context;
        this.list = list;
        this.onClickGallery = onClickGallery;
    }

    public PostingPhotoAdapter() {

    }

    @Override
    public PostingPhotoAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_posting_photo_adapter, parent, false);
        MyHolder rcv = new MyHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final PostingPhotoAdapter.MyHolder holder, final int position) {
        final GaleriDegradasi galeriDegradasi = list.get(position);
        if(!galeriDegradasi.getPicture().equalsIgnoreCase("")){
            holder.pb.setVisibility(View.VISIBLE);
            holder.img_plus.setVisibility(View.GONE);
            Picasso.with(context)
                    .load(galeriDegradasi.getPicture())
                    .into(holder.img, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.pb.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }

        holder.lay_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickGallery.OnShow(v, galeriDegradasi, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.pb)
        ProgressBar pb;

        @BindView(R.id.img)
        ImageView img;
        @BindView(R.id.lay_upload)
        RelativeLayout lay_upload;
        @BindView(R.id.img_plus)
        ImageView img_plus;

        public MyHolder(View itemView) {
            super(itemView);
            this.setIsRecyclable(false);
            ButterKnife.bind(this, itemView);
        }
    }
}
