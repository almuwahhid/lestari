# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-dontwarn butterknife.internal.**
-dontwarn butterknife.Views$InjectViewProcessor
-dontwarn com.squareup.**
-dontwarn com.viewpagerindicator.**
-dontwarn android.support.v7.**

# bottom navigation
-keep public class android.support.design.widget.BottomNavigationView { *; }
-keep public class android.support.design.internal.BottomNavigationMenuView { *; }
-keep public class android.support.design.internal.BottomNavigationPresenter { *; }
-keep public class android.support.design.internal.BottomNavigationItemView { *; }

#butterknife
# Retain generated class which implement Unbinder.
-keep public class * implements butterknife.Unbinder { public <init>(**, android.view.View); }

# Prevent obfuscation of types which use ButterKnife annotations since the simple name
# is used to reflectively look up the generated ViewBinding.
-keep class butterknife.*
-keepclasseswithmembernames class * { @butterknife.* <methods>; }
-keepclasseswithmembernames class * { @butterknife.* <fields>; }


-keep class android.support.v7.** { *; }
-keep class android.support.design.widget.** { *; }
-keep class com.google.** { *; }
-keep class com.android.** { *; }

-keep interface android.support.v7.** { *; }
-keep interface android.support.design.widget.** { *; }


-keep class **$$ViewInjector { *; }
-keep class com.viewpagerindicator.** { *; }

-keepattributes *Annotation*,SourceFile,LineNumberTable

-keepclassmembers class android.support.design.internal.BottomNavigationMenuView {
    boolean mShiftingMode;
}

# - Keep daimajia
-keep class com.daimajia.** { *; }
-dontwarn com.daimajia.**
-keepnames class com.daimajia.**
-keep interface com.daimajia.easing.* { *; }

-dontwarn com.yalantis.ucrop**
-keep class com.yalantis.ucrop** { *; }
-keep interface com.yalantis.ucrop** { *; }