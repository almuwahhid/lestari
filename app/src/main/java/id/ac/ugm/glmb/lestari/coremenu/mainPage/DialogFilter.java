package id.ac.ugm.glmb.lestari.coremenu.mainPage;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.helper.DialogFilterHelper;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.helper.MenuHelper;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogPilihan;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariParam;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

/**
 * Created by root on 6/18/18.
 */

public class DialogFilter {
    Context context;
    Dialog dialog;
    View view;
    ImageView img_close, img_detail;
    LinearLayout lay_filter;
    RelativeLayout lay_jenisdegradasi, lay_city, lay_terapkan;
    Boolean showDegradasi = false;
    List<PilihanClass> classes;
    CheckBox cb_checklist, cb_city, cb_degradasisaya;
    RecyclerView recyclerView;
    TextView tv_city;

    DialogFilterAdapter adapter;
    OnTerapkan onTerapkan;
    DialogPilihan dialogPilihan;

    public DialogFilter(Context context, OnTerapkan onTerapkan) {
        this.context = context;
        this.onTerapkan = onTerapkan;
        this.view = View.inflate(context, R.layout.filter_dialog, null);
//        ButterKnife.bind(this, view);
        initComponent();
    }

    private void initComponent(){
        dialog = new Dialog(context);

        classes = new ArrayList<>();
        for(PilihanClass pilihanClass:MenuHelper.listDegradasi(context)){
            classes.add(pilihanClass);
        }
        adapter = new DialogFilterAdapter(context, classes);
        adapter.setOnClickListener(new DialogFilterAdapter.OnClickListener() {
            @Override
            public void onClick(PilihanClass pilihanClass, int position, boolean val) {
                Log.d("click", "onClick: "+pilihanClass.getId());
                switch (pilihanClass.getId()){
                    case LestariConstant.EROSI:
                        Lestari.setSPBoolean(context, LestariConstant.SP_EROSI, val);
                        break;
                    case LestariConstant.LONGSOR:
                        Lestari.setSPBoolean(context, LestariConstant.SP_LONGSOR, val);
                        break;
                    case LestariConstant.SEDIMENTASI:
                        Lestari.setSPBoolean(context, LestariConstant.SP_SEDIMENTASI, val);
                        break;
                    case LestariConstant.EUTROFIKASI:
                        Lestari.setSPBoolean(context, LestariConstant.SP_EUTROFIKASI, val);
                        break;
                    case LestariConstant.SAMPAH:
                        Lestari.setSPBoolean(context, LestariConstant.SP_SAMPAH, val);
                        break;
                    case LestariConstant.LIMBAH:
                        Lestari.setSPBoolean(context, LestariConstant.SP_LIMBAH, val);
                        break;
                    case LestariConstant.BANJIR:
                        Lestari.setSPBoolean(context, LestariConstant.SP_BANJIR, val);
                        break;
                    case LestariConstant.KEBAKARAN:
                        Lestari.setSPBoolean(context, LestariConstant.SP_KEBAKARAN, val);
                        break;
                    case LestariConstant.PENGENDALIAN:
                        Lestari.setSPBoolean(context, LestariConstant.SP_PENGENDALIAN, val);
                        break;
                }
                pilihanClass.setState(val);
                classes.set(position, pilihanClass);
                adapter.notifyDataSetChanged();
                checkIsJenisDegradasi();
            }
        });


        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTopAnimation;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setContentView(R.layout.filter_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP;
//        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);


        img_close = dialog.findViewById(R.id.img_close);
        tv_city = dialog.findViewById(R.id.tv_city);
        img_detail = dialog.findViewById(R.id.img_detail);
        lay_filter = dialog.findViewById(R.id.lay_filter);
        lay_jenisdegradasi = dialog.findViewById(R.id.lay_jenisdegradasi);
        cb_checklist = dialog.findViewById(R.id.cb_checklist);
        cb_city = dialog.findViewById(R.id.cb_city);
        cb_degradasisaya = dialog.findViewById(R.id.cb_degradasisaya);
        lay_city = dialog.findViewById(R.id.lay_city);
        lay_terapkan = dialog.findViewById(R.id.lay_terapkan);
        recyclerView = dialog.findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);

        if(!Lestari.getSPString(context, LestariConstant.SP_CITY_FILTER_NAME).equalsIgnoreCase("")){
            tv_city.setText(Lestari.getSPString(context, LestariConstant.SP_CITY_FILTER_NAME));
            lay_city.setVisibility(View.VISIBLE);
            cb_city.setChecked(true);
        }

        checkIsJenisDegradasi();
        cb_degradasisaya.setChecked(Lestari.getSPBoolean(context, LestariConstant.SP_FILTER_USER));

        cb_city.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    lay_city.setVisibility(View.VISIBLE);
                } else {
                    tv_city.setText("Pilih Kota");
                    Lestari.setSPString(context, LestariConstant.SP_CITY_FILTER, "");
                    Lestari.setSPString(context, LestariConstant.SP_CITY_FILTER_NAME, "");
                    lay_city.setVisibility(View.GONE);
                }
            }
        });

//        compile 'com.google.firebase:firebase-core:16.0.0'
        cb_degradasisaya.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Lestari.setSPBoolean(context, LestariConstant.SP_FILTER_USER, isChecked);
            }
        });

        cb_checklist.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean val) {
                for (int i = 0; i < classes.size(); i++) {
                    PilihanClass pilihanClass = classes.get(i);
                    pilihanClass.setState(val);
                    classes.set(i, pilihanClass);
                }
                adapter.notifyDataSetChanged();

                Lestari.setSPBoolean(context, LestariConstant.SP_EROSI, val);
                Lestari.setSPBoolean(context, LestariConstant.SP_LONGSOR, val);
                Lestari.setSPBoolean(context, LestariConstant.SP_SEDIMENTASI, val);
                Lestari.setSPBoolean(context, LestariConstant.SP_EUTROFIKASI, val);
                Lestari.setSPBoolean(context, LestariConstant.SP_SAMPAH, val);
                Lestari.setSPBoolean(context, LestariConstant.SP_LIMBAH, val);
                Lestari.setSPBoolean(context, LestariConstant.SP_BANJIR, val);
                Lestari.setSPBoolean(context, LestariConstant.SP_KEBAKARAN, val);
                Lestari.setSPBoolean(context, LestariConstant.SP_PENGENDALIAN, val);
            }
        });

        lay_terapkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                onTerapkan.onTerapkan();
            }
        });

        lay_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialogPilihan==null){
                    initDialogCity();
                } else {
                    dialogPilihan.show();
                }
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        lay_filter.setLayoutParams(new FrameLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT));

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        img_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDegradasi = !showDegradasi;
                if(showDegradasi){
                    img_detail.setImageResource(R.drawable.ic_remove_black_24dp);
                    lay_jenisdegradasi.setVisibility(View.VISIBLE);
                } else {
                    img_detail.setImageResource(R.drawable.ic_add);
                    lay_jenisdegradasi.setVisibility(View.GONE);
                }
            }
        });
        show();
    }

    public void show(){
        if(dialog!=null){
            dialog.show();
        }
    }

    private void initDialogCity(){
        LestariRequest.POST(LestariParam.stringCity(), context, new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                LestariUi.ShowLoadingDialog(context);
            }

            @Override
            public void onSuccess(JSONObject response) {
                LestariUi.HideLoadingDialog(context);
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        dialogPilihan = new DialogPilihan(context, DialogFilterHelper.getWilayah(response.getJSONArray("data")), "Pilihan Kota", true);
                        dialogPilihan.setOnItemClick(new DialogPilihan.OnItemClick() {
                            @Override
                            public void onItemClick(View view, PilihanClass pilihanClass, int position) {
                                Lestari.setSPString(context, LestariConstant.SP_CITY_FILTER_NAME, pilihanClass.getName());
                                Lestari.setSPString(context, LestariConstant.SP_CITY_FILTER, pilihanClass.getId());
                                tv_city.setText(pilihanClass.getName());
                            }
                        });
                        dialogPilihan.show();

                        DialogFilterHelper.getWilayah(response.getJSONArray("data"));
                    }else if(response.getInt("code")==LestariConstant.CODE_FAILED){
                        Lestari.alertWarning(context, "Password/username Anda salah");
                    }else {
                        Lestari.alertWarning(context, context.getResources().getString(R.string.txt_error));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                LestariUi.HideLoadingDialog(context);
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("lestari_token_check", Lestari.getSPString(context, LestariConstant.SP_USER_TOKEN));
                System.out.println(param);
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }
        });
    }

    private void checkIsJenisDegradasi(){
        if(isAllDegradasi()){
            cb_checklist.setChecked(true);
        }
    }

    private boolean isAllDegradasi(){
        if(Lestari.getSPBooleanTrue(context, LestariConstant.SP_EROSI) &&
                Lestari.getSPBooleanTrue(context, LestariConstant.SP_LONGSOR) &&
                Lestari.getSPBooleanTrue(context, LestariConstant.SP_SEDIMENTASI) &&
                Lestari.getSPBooleanTrue(context, LestariConstant.SP_EUTROFIKASI) &&
                Lestari.getSPBooleanTrue(context, LestariConstant.SP_SAMPAH) &&
                Lestari.getSPBooleanTrue(context, LestariConstant.SP_LIMBAH) &&
                Lestari.getSPBooleanTrue(context, LestariConstant.SP_BANJIR) &&
                Lestari.getSPBooleanTrue(context, LestariConstant.SP_PENGENDALIAN) &&
                Lestari.getSPBooleanTrue(context, LestariConstant.SP_KEBAKARAN))
            return true;
        else
            return false;
    }

    public void dismiss(){
        if(dialog!=null){
            showDegradasi = false;
            dialog.dismiss();
        }
    }

    interface OnTerapkan{
        void onTerapkan();
    }

}
