package id.ac.ugm.glmb.lestari.mainmenu;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import butterknife.BindView;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.banjir.BanjirAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.banjir.BanjirClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiAlurAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiLembarAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiParitAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiPercikAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiPipaAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiTebingSungaiAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.eutrofikasi.EutrofikasiAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.eutrofikasi.EutrofikasiClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.kebakaran.KebakaranAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.kebakaran.KebakaranClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.limbah.LimbahAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.limbah.LimbahClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.longsor.LongsorAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.longsor.LongsorClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.sampah.SampahAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.sampah.SampahClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.sedimentasi.SedimentasiAdapter;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.sedimentasi.SedimentasiClass;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;

public class DetailDegradasiActivity extends ActivityGeneral {

    ErosiAlurAdapter erosiAlurAdapter;
    ErosiLembarAdapter erosiLembarAdapter;
    ErosiParitAdapter erosiParitAdapter;
    ErosiPercikAdapter erosiPercikAdapter;
    ErosiPipaAdapter erosiPipaAdapter;
    ErosiTebingSungaiAdapter erosiTebingSungaiAdapter;
    ErosiClass erosiClass;

    LongsorAdapter longsorAdapter;
    LongsorClass longsorClass;

    SedimentasiAdapter sedimentasiAdapter;
    SedimentasiClass sedimentasiClass;

    EutrofikasiAdapter eutrofikasiAdapter;
    EutrofikasiClass eutrofikasiClass;

    BanjirAdapter banjirAdapter;
    BanjirClass banjirClass;

    LimbahAdapter limbahAdapter;
    LimbahClass limbahClass;

    KebakaranAdapter kebakaranAdapter;
    KebakaranClass kebakaranClass;

    SampahAdapter sampahAdapter;
    SampahClass sampahClass;
    @BindView(R.id.card_login) protected RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_degradasi);

        ToolbarLestari toolbarLestari = new ToolbarLestari(getContext(), "Detail");
    }
}
