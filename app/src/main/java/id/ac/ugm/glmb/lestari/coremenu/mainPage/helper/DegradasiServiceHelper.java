package id.ac.ugm.glmb.lestari.coremenu.mainPage.helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.degradasi.GaleriDegradasi;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.banjir.BanjirClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.erosi.ErosiClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.eutrofikasi.EutrofikasiClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.kebakaran.KebakaranClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.limbah.LimbahClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.longsor.LongsorClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.pengendalian.PengendalianClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.sampah.SampahClass;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.sedimentasi.SedimentasiClass;

/**
 * Created by root on 7/24/18.
 */

public class DegradasiServiceHelper {
    public static List<ErosiClass> listErosi(JSONArray array){
        List<ErosiClass> listErosiAlur = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                ErosiClass erosiClass = new ErosiClass();
                erosiClass.setId(jsonObject.getString("id_degradasi"));
                erosiClass.setParent_id(jsonObject.getString("parent"));
                erosiClass.setDate(jsonObject.getString("date"));
                erosiClass.setDeskripsi(jsonObject.getString("deskripsi"));
                erosiClass.setName(jsonObject.getString("fullname"));

                erosiClass.setKetinggian_pedestial(jsonObject.getString("ketinggian_pedestial"));
                erosiClass.setBerat_volume(jsonObject.getString("berat_volume"));
                erosiClass.setKedalaman(jsonObject.getString("kedalaman"));
                erosiClass.setBahan_kasar(jsonObject.getString("bahan_kasar"));
                erosiClass.setLebar(jsonObject.getString("lebar"));
                erosiClass.setPanjang(jsonObject.getString("panjang"));
                erosiClass.setArea_rangkupan(jsonObject.getString("area_rangkupan"));
                erosiClass.setKehilangan_tanah(jsonObject.getString("kehilangan_tanah"));
                erosiClass.setLebar_atas(jsonObject.getString("lebar_atas"));
                erosiClass.setLebar_bawah(jsonObject.getString("lebar_bawah"));
                erosiClass.setDebit_rembesan(jsonObject.getString("debit_rembesan"));
                erosiClass.setDebit_rembesan(jsonObject.getString("debit_rembesan"));

                JSONArray foto = jsonObject.getJSONArray("foto");
                erosiClass.setGalleries(listGallery(foto));

                listErosiAlur.add(erosiClass);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listErosiAlur;
    }

    public static List<EutrofikasiClass> listEutrofikasi(JSONArray array){
        List<EutrofikasiClass> listEutrofikasi = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                EutrofikasiClass eutrofikasiClass = new EutrofikasiClass();

                eutrofikasiClass.setId(jsonObject.getString("id_degradasi"));
                eutrofikasiClass.setParent_id(jsonObject.getString("parent"));
                eutrofikasiClass.setDate(jsonObject.getString("date"));
                eutrofikasiClass.setDeskripsi(jsonObject.getString("deskripsi"));
                eutrofikasiClass.setName(jsonObject.getString("fullname"));

                eutrofikasiClass.setLuas_areaterdampak(jsonObject.getString("luas_area_terdampak"));
                eutrofikasiClass.setJenis_hama(jsonObject.getString("jenis_hama_tanaman"));

                JSONArray foto = jsonObject.getJSONArray("foto");
                eutrofikasiClass.setGalleries(listGallery(foto));
                listEutrofikasi.add(eutrofikasiClass);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listEutrofikasi;
    }

    public static List<KebakaranClass> listKebakaran(JSONArray array){
        List<KebakaranClass> listEutrofikasi = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                KebakaranClass eutrofikasiClass = new KebakaranClass();

                eutrofikasiClass.setId(jsonObject.getString("id_degradasi"));
                eutrofikasiClass.setParent_id(jsonObject.getString("parent"));
                eutrofikasiClass.setDate(jsonObject.getString("date"));
                eutrofikasiClass.setDeskripsi(jsonObject.getString("deskripsi"));
                eutrofikasiClass.setName(jsonObject.getString("fullname"));

                eutrofikasiClass.setTanggal_kejadian(jsonObject.getString("tanggal_kejadian"));

                JSONArray foto = jsonObject.getJSONArray("foto");
                eutrofikasiClass.setGalleries(listGallery(foto));
                listEutrofikasi.add(eutrofikasiClass);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listEutrofikasi;
    }

    public static List<BanjirClass> listBanjir(JSONArray array){
        List<BanjirClass> listEutrofikasi = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                BanjirClass eutrofikasiClass = new BanjirClass();

                eutrofikasiClass.setId(jsonObject.getString("id_degradasi"));
                eutrofikasiClass.setParent_id(jsonObject.getString("parent"));
                eutrofikasiClass.setDate(jsonObject.getString("date"));
                eutrofikasiClass.setDeskripsi(jsonObject.getString("deskripsi"));
                eutrofikasiClass.setName(jsonObject.getString("fullname"));

                eutrofikasiClass.setTanggal_kejadian(jsonObject.getString("tanggal_kejadian"));
                eutrofikasiClass.setTipe_banjir(jsonObject.getString("tipe_banjir"));

                JSONArray foto = jsonObject.getJSONArray("foto");
                eutrofikasiClass.setGalleries(listGallery(foto));
                listEutrofikasi.add(eutrofikasiClass);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listEutrofikasi;
    }

    public static List<LimbahClass> listLimbah(JSONArray array){
        List<LimbahClass> listEutrofikasi = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                LimbahClass eutrofikasiClass = new LimbahClass();

                eutrofikasiClass.setId(jsonObject.getString("id_degradasi"));
                eutrofikasiClass.setParent_id(jsonObject.getString("parent"));
                eutrofikasiClass.setDate(jsonObject.getString("date"));
                eutrofikasiClass.setDeskripsi(jsonObject.getString("deskripsi"));
                eutrofikasiClass.setName(jsonObject.getString("fullname"));

                eutrofikasiClass.setJenis_limbah(jsonObject.getString("jenis_limbah"));
                eutrofikasiClass.setWarna(jsonObject.getString("warna"));
                eutrofikasiClass.setBusa(jsonObject.getString("busa"));
                eutrofikasiClass.setKekeruhan(jsonObject.getString("kekeruhan"));
                eutrofikasiClass.setRasa(jsonObject.getString("rasa"));
                eutrofikasiClass.setBau(jsonObject.getString("bau"));
                eutrofikasiClass.setSuhu(jsonObject.getString("suhu"));

                JSONArray foto = jsonObject.getJSONArray("foto");
                eutrofikasiClass.setGalleries(listGallery(foto));
                listEutrofikasi.add(eutrofikasiClass);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listEutrofikasi;
    }

    public static List<LongsorClass> listLongsor(JSONArray array){
        List<LongsorClass> listEutrofikasi = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                LongsorClass eutrofikasiClass = new LongsorClass();

                eutrofikasiClass.setId(jsonObject.getString("id_degradasi"));
                eutrofikasiClass.setParent_id(jsonObject.getString("parent"));
                eutrofikasiClass.setDate(jsonObject.getString("date"));
                eutrofikasiClass.setDeskripsi(jsonObject.getString("deskripsi"));
                eutrofikasiClass.setName(jsonObject.getString("fullname"));

                eutrofikasiClass.setTipologi(jsonObject.getString("tipologi_longsor"));
                eutrofikasiClass.setPanjang(jsonObject.getString("panjang"));
                eutrofikasiClass.setLebar(jsonObject.getString("lebar"));
                eutrofikasiClass.setKedalaman(jsonObject.getString("kedalaman"));

                JSONArray foto = jsonObject.getJSONArray("foto");
                eutrofikasiClass.setGalleries(listGallery(foto));
                listEutrofikasi.add(eutrofikasiClass);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listEutrofikasi;
    }

    public static List<SampahClass> listSampah(JSONArray array){
        List<SampahClass> listEutrofikasi = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                SampahClass eutrofikasiClass = new SampahClass();

                eutrofikasiClass.setId(jsonObject.getString("id_degradasi"));
                eutrofikasiClass.setParent_id(jsonObject.getString("parent"));
                eutrofikasiClass.setDate(jsonObject.getString("date"));
                eutrofikasiClass.setDeskripsi(jsonObject.getString("deskripsi"));
                eutrofikasiClass.setName(jsonObject.getString("fullname"));

                eutrofikasiClass.setJenisSampah(jsonObject.getString("jenis_sampah"));

                JSONArray foto = jsonObject.getJSONArray("foto");
                eutrofikasiClass.setGalleries(listGallery(foto));
                listEutrofikasi.add(eutrofikasiClass);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listEutrofikasi;
    }

    public static List<SedimentasiClass> listSedimentasi(JSONArray array){
        List<SedimentasiClass> listEutrofikasi = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                SedimentasiClass eutrofikasiClass = new SedimentasiClass();

                eutrofikasiClass.setId(jsonObject.getString("id_degradasi"));
                eutrofikasiClass.setParent_id(jsonObject.getString("parent"));
                eutrofikasiClass.setDate(jsonObject.getString("date"));
                eutrofikasiClass.setDeskripsi(jsonObject.getString("deskripsi"));
                eutrofikasiClass.setName(jsonObject.getString("fullname"));

                eutrofikasiClass.setPosisi(jsonObject.getString("posisi"));
                eutrofikasiClass.setKetebalan(jsonObject.getString("ketebalan"));

                JSONArray foto = jsonObject.getJSONArray("foto");
                eutrofikasiClass.setGalleries(listGallery(foto));
                listEutrofikasi.add(eutrofikasiClass);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listEutrofikasi;
    }

    public static List<PengendalianClass> listPengendalian(JSONArray array){
        List<PengendalianClass> listEutrofikasi = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                PengendalianClass eutrofikasiClass = new PengendalianClass();

                eutrofikasiClass.setId(jsonObject.getString("id_degradasi"));
                eutrofikasiClass.setParent_id(jsonObject.getString("parent"));
                eutrofikasiClass.setDate(jsonObject.getString("date"));
                eutrofikasiClass.setDeskripsi(jsonObject.getString("deskripsi"));
                eutrofikasiClass.setName(jsonObject.getString("fullname"));

                eutrofikasiClass.setJenisPengendalian(jsonObject.getString("jenis_pengendalian"));
                eutrofikasiClass.setKegiatanPengendalian(jsonObject.getString("nama_kegiatan"));
                eutrofikasiClass.setTahun_pelaksanaan(jsonObject.getString("date_pengendalian"));

                JSONArray foto = jsonObject.getJSONArray("foto");
                eutrofikasiClass.setGalleries(listGallery(foto));
                listEutrofikasi.add(eutrofikasiClass);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listEutrofikasi;
    }

    public static List<GaleriDegradasi> listGallery(JSONArray jsonArray){
        List<GaleriDegradasi> listGallery = new ArrayList<>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                GaleriDegradasi galeriDegradasi = new GaleriDegradasi();
                galeriDegradasi.setId_gallery(jsonObject.getString("id_gallery"));
//                galeriDegradasi.setPicture(jsonObject.getString("picture").replaceAll("http://", "https://"));
                galeriDegradasi.setPicture(jsonObject.getString("picture"));
                galeriDegradasi.setId_degradasi(jsonObject.getString("id_degradasi"));
                listGallery.add(galeriDegradasi);
            }
        }catch (JSONException e){

        }
        return listGallery;
    }


}
