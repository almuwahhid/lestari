package id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.eutrofikasi;

import java.util.ArrayList;
import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;

/**
 * Created by root on 7/22/18.
 */

public class TambahEutrofikasiHelper {
    public static List<PilihanClass> listTipeHama(){
        List<PilihanClass> listDegradasi = new ArrayList<>();
        PilihanClass pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.EROSI);
        pilihanClass.setName("Hama A");
        pilihanClass.setId("Hama A");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Hama B");
        pilihanClass.setName("Hama B");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Hama C");
        pilihanClass.setName("Hama C");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Hama D");
        pilihanClass.setName("Hama D");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("-");
        pilihanClass.setName("Lainnya");
        listDegradasi.add(pilihanClass);

        return listDegradasi;
    }
}
