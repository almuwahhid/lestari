package id.ac.ugm.glmb.lestari.mainmenu.degradasi;

import java.io.Serializable;

/**
 * Created by root on 7/18/18.
 */

public class GaleriDegradasi implements Serializable{
    String id_gallery = "", id_degradasi = "", picture = "";

    public String getId_gallery() {
        return id_gallery;
    }

    public void setId_gallery(String id_gallery) {
        this.id_gallery = id_gallery;
    }

    public String getId_degradasi() {
        return id_degradasi;
    }

    public void setId_degradasi(String id_degradasi) {
        this.id_degradasi = id_degradasi;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
