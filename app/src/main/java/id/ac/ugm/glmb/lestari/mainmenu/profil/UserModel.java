package id.ac.ugm.glmb.lestari.mainmenu.profil;

/**
 * Created by root on 7/8/18.
 */

public class UserModel {
    String id_user;
    String username;
    String photo;
    String fullname;
    String address;
    String gender;
    String phone;
    String id_kel;
    String nama_kel;
    String id_kab;
    String nama_kab;
    String id_prov;
    String nama_prov;
    String id_kec;
    String nama_kec;
    String verified;
    String token_login;
    String birthday;
    String hash_id;
    String email;
    String latlng;
    String jumlah_degradasi;
    String jumlah_kontribusi;

    public String getJumlah_degradasi() {
        return jumlah_degradasi;
    }

    public void setJumlah_degradasi(String jumlah_degradasi) {
        this.jumlah_degradasi = jumlah_degradasi;
    }

    public String getJumlah_kontribusi() {
        return jumlah_kontribusi;
    }

    public void setJumlah_kontribusi(String jumlah_kontribusi) {
        this.jumlah_kontribusi = jumlah_kontribusi;
    }

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId_kel() {
        return id_kel;
    }

    public void setId_kel(String id_kel) {
        this.id_kel = id_kel;
    }

    public String getNama_kel() {
        return nama_kel;
    }

    public void setNama_kel(String nama_kel) {
        this.nama_kel = nama_kel;
    }

    public String getId_kab() {
        return id_kab;
    }

    public void setId_kab(String id_kab) {
        this.id_kab = id_kab;
    }

    public String getNama_kab() {
        return nama_kab;
    }

    public void setNama_kab(String nama_kab) {
        this.nama_kab = nama_kab;
    }

    public String getId_prov() {
        return id_prov;
    }

    public void setId_prov(String id_prov) {
        this.id_prov = id_prov;
    }

    public String getNama_prov() {
        return nama_prov;
    }

    public void setNama_prov(String nama_prov) {
        this.nama_prov = nama_prov;
    }

    public String getId_kec() {
        return id_kec;
    }

    public void setId_kec(String id_kec) {
        this.id_kec = id_kec;
    }

    public String getNama_kec() {
        return nama_kec;
    }

    public void setNama_kec(String nama_kec) {
        this.nama_kec = nama_kec;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public String getToken_login() {
        return token_login;
    }

    public void setToken_login(String token_login) {
        this.token_login = token_login;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getHash_id() {
        return hash_id;
    }

    public void setHash_id(String hash_id) {
        this.hash_id = hash_id;
    }
}
