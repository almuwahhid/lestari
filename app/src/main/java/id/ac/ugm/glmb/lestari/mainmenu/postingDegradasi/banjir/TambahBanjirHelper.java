package id.ac.ugm.glmb.lestari.mainmenu.postingDegradasi.banjir;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;

/**
 * Created by root on 7/21/18.
 */

public class TambahBanjirHelper {
    public static List<PilihanClass> listTipeBanjir(){
        List<PilihanClass> listDegradasi = new ArrayList<>();
        PilihanClass pilihanClass = new PilihanClass();
        pilihanClass.setId(LestariConstant.EROSI);
        pilihanClass.setName("Banjir Bandang");
        pilihanClass.setId("Banjir Bandang");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Banjir Rob");
        pilihanClass.setName("Banjir Rob");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Banjir Kota/Genangan");
        pilihanClass.setName("Banjir Kota/Genangan");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("Banjir Sungai");
        pilihanClass.setName("Banjir Sungai");
        listDegradasi.add(pilihanClass);

        pilihanClass = new PilihanClass();
        pilihanClass.setId("-");
        pilihanClass.setName("Lainnya");
        listDegradasi.add(pilihanClass);

        return listDegradasi;
    }
}
