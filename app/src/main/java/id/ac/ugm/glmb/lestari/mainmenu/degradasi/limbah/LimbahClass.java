package id.ac.ugm.glmb.lestari.mainmenu.degradasi.limbah;

import java.io.Serializable;
import java.util.List;

import id.ac.ugm.glmb.lestari.mainmenu.degradasi.GaleriDegradasi;

/**
 * Created by root on 7/1/18.
 */

public class LimbahClass implements Serializable{
    String id="", id_user="", time="", date="", parent_id="", deskripsi="", busa="", kekeruhan="", bau="", rasa="", suhu="", warna="", name ="", jenis_limbah = "";

    public List<GaleriDegradasi> galleries;

    public List<GaleriDegradasi> getGalleries() {
        return galleries;
    }

    public void setGalleries(List<GaleriDegradasi> galleries) {
        this.galleries = galleries;
    }

    public String getJenis_limbah() {
        return jenis_limbah;
    }

    public void setJenis_limbah(String jenis_limbah) {
        this.jenis_limbah = jenis_limbah;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getBusa() {
        return busa;
    }

    public void setBusa(String busa) {
        this.busa = busa;
    }

    public String getKekeruhan() {
        return kekeruhan;
    }

    public void setKekeruhan(String kekeruhan) {
        this.kekeruhan = kekeruhan;
    }

    public String getBau() {
        return bau;
    }

    public void setBau(String bau) {
        this.bau = bau;
    }

    public String getRasa() {
        return rasa;
    }

    public void setRasa(String rasa) {
        this.rasa = rasa;
    }

    public String getSuhu() {
        return suhu;
    }

    public void setSuhu(String suhu) {
        this.suhu = suhu;
    }

    public String getWarna() {
        return warna;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }
}
