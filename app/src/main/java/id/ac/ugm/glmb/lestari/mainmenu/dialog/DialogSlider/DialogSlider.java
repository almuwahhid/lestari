package id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogSlider;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.GaleriDegradasi;
import id.ac.ugm.glmb.lestari.utilities.CircleIndicator;

public class DialogSlider {
    @BindView(R.id.viewpager) protected ViewPager viewpager;
    @BindView(R.id.circleIndicator) protected CircleIndicator circleIndicator;
    @BindView(R.id.img_close) protected ImageView img_close;
    @BindView(R.id.lay_photo) protected RelativeLayout lay_photo;

    Context context;
    List<GaleriDegradasi> galeriDegradasis;
    Dialog dialog;
    View layoutView;
    SlideShowAdapter adapter;
    int index = 0;

    public DialogSlider(Context context, List<GaleriDegradasi> galeriBencanas) {
        this.context = context;
        this.galeriDegradasis = galeriBencanas;
        layoutView = View.inflate(context, R.layout.activity_dialog_slider, null);
        initDialog();
    }

    private void initDialog(){
        ButterKnife.bind(this, layoutView);
        dialog = new Dialog(context);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTopAnimation;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(layoutView);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP;
        window.setAttributes(wlp);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        lay_photo.setLayoutParams(new FrameLayout.LayoutParams(width, height));

        adapter = new SlideShowAdapter(context, galeriDegradasis);
        viewpager.setAdapter(adapter);
        circleIndicator.setViewPager(viewpager);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public void show(int index){
        this.index = index;
        viewpager.setCurrentItem(index, true);
        dialog.show();
    }

    public void dismiss(){
        dialog.dismiss();
    }
}
