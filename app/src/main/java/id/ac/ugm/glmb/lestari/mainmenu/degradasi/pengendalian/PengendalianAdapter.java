package id.ac.ugm.glmb.lestari.mainmenu.degradasi.pengendalian;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.PhotoActivity;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

public class PengendalianAdapter extends RecyclerView.Adapter<PengendalianAdapter.MyHolder>  {
    Context context;
    List<PengendalianClass> classes;
    Boolean isDetail = false;

    public PengendalianAdapter(Context context, List<PengendalianClass> classes, Boolean isDetail) {
        this.context = context;
        this.classes = classes;
        this.isDetail = isDetail;
    }

    public PengendalianAdapter(Context context, List<PengendalianClass> classes) {
        this.context = context;
        this.classes = classes;
    }

    public PengendalianAdapter() {

    }

    @NonNull
    @Override
    public PengendalianAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_pengendalian_adapter, parent, false);
        MyHolder rcv = new MyHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(@NonNull PengendalianAdapter.MyHolder holder, int position) {
        PengendalianClass kelas = classes.get(position);
        new PhotoActivity(context, holder.recyclerView, holder.tv_nodata, kelas.getGalleries());
        holder.tv_deskripsi.setText(kelas.getDeskripsi());
        holder.tv_name.setText(kelas.getName());
        try{
            String tgl = kelas.getDate().split(" ")[0];
            int year = Integer.valueOf(tgl.split("-")[0]);
            int month = Integer.valueOf(tgl.split("-")[1])-1;
            int date = Integer.valueOf(tgl.split("-")[2]);
            holder.tv_date.setText(date+" "+ LestariUi.monthName(month)+" "+year+", "+kelas.getDate().split(" ")[1]);
        } catch (IndexOutOfBoundsException e){
            holder.tv_date.setText("");
        } catch (NumberFormatException e){
            holder.tv_date.setText("");
        }

        holder.tv_tahunkegiatan.setText(kelas.getTahun_pelaksanaan());
        holder.tv_kegiatanPengendalian.setText(kelas.getKegiatanPengendalian());
        holder.tv_jenis.setText(kelas.getJenisPengendalian());
    }

    @Override
    public int getItemCount() {
        return classes.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;

        @BindView(R.id.tv_deskripsi)
        TextView tv_deskripsi;
        @BindView(R.id.tv_nodata)
        TextView tv_nodata;
        @BindView(R.id.tv_date)
        TextView tv_date;
        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.tv_jenis)
        TextView tv_jenis;

        @BindView(R.id.tv_tahunkegiatan)
        TextView tv_tahunkegiatan;
        @BindView(R.id.tv_kegiatanPengendalian)
        TextView tv_kegiatanPengendalian;
        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
