package id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogSlider;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.GaleriDegradasi;

public class SlideShowAdapter extends PagerAdapter {

    private Context context;
    private List<GaleriDegradasi> sliders;
    private LayoutInflater inflater;

    public SlideShowAdapter(Context context, List<GaleriDegradasi> sliders) {
        this.context = context;
        this.sliders = sliders;
        this.inflater = LayoutInflater.from(context);
    }

    public SlideShowAdapter() {
    }

    @Override
    public int getCount() {
        return sliders.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        GaleriDegradasi galeri = sliders.get(position);
        View imageLayout = inflater.inflate(R.layout.layout_slideshow_adapter, container, false);
        final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.img_content);
        final RelativeLayout helper_loading_box = (RelativeLayout) imageLayout.findViewById(R.id.helper_loading_box);
        final RelativeLayout helper_noconnection = (RelativeLayout) imageLayout.findViewById(R.id.helper_noconnection);

        helper_loading_box.setVisibility(View.VISIBLE);
        helper_noconnection.setVisibility(View.GONE);
        Picasso.with(context)
                .load(galeri.getPicture())
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        helper_loading_box.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        helper_loading_box.setVisibility(View.GONE);
                        helper_noconnection.setVisibility(View.VISIBLE);
                    }
                });
        container.addView(imageLayout, 0);
        return imageLayout;
    }
}
