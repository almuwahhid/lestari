package id.ac.ugm.glmb.lestari.mainmenu.kontribusi;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.mainmenu.degradasi.Degradasi;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

public class KontribusiAdapter extends RecyclerView.Adapter<KontribusiAdapter.MyHolder> {
    Context context;
    List<Degradasi> list;

    OnKontribusiClick onClickListener;

    public KontribusiAdapter(Context context, List<Degradasi> list) {
        this.context = context;
        this.list = list;
    }

    public KontribusiAdapter() {
    }

    @Override
    public KontribusiAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_kontribusi_adapter, parent, false);
        MyHolder rcv = new MyHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(KontribusiAdapter.MyHolder holder, int position) {
        final Degradasi degradasi = list.get(position);
        holder.card_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onItemClick(v, degradasi);
            }
        });
        holder.tv_lokasi.setText(degradasi.getAlamat());

        String nama = "";
        String subnama = "";
        try {
            nama = degradasi.getNamajenis().substring(0, 1).toUpperCase() + degradasi.getNamajenis().substring(1);
            subnama = degradasi.getNamaSubJenis().substring(0, 1).toUpperCase() + degradasi.getNamaSubJenis().substring(1);
        }catch (IndexOutOfBoundsException e){
            holder.tv_jenis.setText("");
        }

        holder.tv_jenis.setText(nama +" "+subnama);

        try{
            String tgl = degradasi.getDate().split(" ")[0];
            int year = Integer.valueOf(tgl.split("-")[0]);
            int month = Integer.valueOf(tgl.split("-")[1])-1;
            int date = Integer.valueOf(tgl.split("-")[2]);
            holder.tv_date.setText(date+" "+ LestariUi.monthName(month)+" "+year+", "+degradasi.getDate().split(" ")[1]);
        } catch (IndexOutOfBoundsException e){
            holder.tv_date.setText("");
        } catch (NumberFormatException e){
            holder.tv_date.setText("");
        }

        switch (degradasi.getStatus()){
            case "0":
                holder.lay_accepted.setVisibility(View.GONE);
                holder.lay_pending.setVisibility(View.VISIBLE);
                holder.lay_rejected.setVisibility(View.GONE);
                break;
            case "1":
                holder.lay_accepted.setVisibility(View.VISIBLE);
                holder.lay_rejected.setVisibility(View.GONE);
                holder.lay_pending.setVisibility(View.GONE);
                break;
            case "2":
                holder.lay_accepted.setVisibility(View.GONE);
                holder.lay_rejected.setVisibility(View.VISIBLE);
                holder.lay_pending.setVisibility(View.GONE);
                break;
        }

        initIcon(holder.img_jenis, degradasi.getNamajenis());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_jenis)
        TextView tv_jenis;
        @BindView(R.id.tv_lokasi)
        TextView tv_lokasi;
        @BindView(R.id.tv_date)
        TextView tv_date;
        @BindView(R.id.img_jenis)
        ImageView img_jenis;
        @BindView(R.id.card_layout)
        CardView card_layout;
        @BindView(R.id.lay_accepted)
        LinearLayout lay_accepted;
        @BindView(R.id.lay_rejected)
        LinearLayout lay_rejected;
        @BindView(R.id.lay_pending)
        LinearLayout lay_pending;
        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void initIcon(ImageView imageView, String idjenis){
        switch (idjenis){
            case LestariConstant.EROSI:
                imageView.setImageResource(R.drawable.ic_erosi);
                break;
            case LestariConstant.BANJIR:
                imageView.setImageResource(R.drawable.ic_banjir);
                break;
            case LestariConstant.EUTROFIKASI:
                imageView.setImageResource(R.drawable.ic_eutrofikasi);
                break;
            case LestariConstant.SEDIMENTASI:
                imageView.setImageResource(R.drawable.ic_sedimentasi);
                break;
            case LestariConstant.LONGSOR:
                imageView.setImageResource(R.drawable.ic_longsor);
                break;
            case LestariConstant.LIMBAH:
                imageView.setImageResource(R.drawable.ic_limbah);
                break;
            case LestariConstant.KEBAKARAN:
                imageView.setImageResource(R.drawable.ic_kebakaran);
                break;
            case LestariConstant.SAMPAH:
                imageView.setImageResource(R.drawable.ic_sampah);
                break;
            case LestariConstant.PENGENDALIAN:
                imageView.setImageResource(R.drawable.ic_pengendalian);
                break;
            default:
                imageView.setImageResource(R.drawable.ic_erosi);
                break;
        }
    }

    public void setOnClickListener(OnKontribusiClick onClickListener){
        this.onClickListener = onClickListener;
    }

    interface OnKontribusiClick{
        void onItemClick(View v, Degradasi degradasi);
    }
}
