package id.ac.ugm.glmb.lestari.mainmenu.register;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.coremenu.mainPage.MainActivity;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.DialogPilihan;
import id.ac.ugm.glmb.lestari.mainmenu.dialog.PilihanClass;
import id.ac.ugm.glmb.lestari.mainmenu.lainnya.AppIntroActivity;
import id.ac.ugm.glmb.lestari.mainmenu.login.LoginJsonHelper;
import id.ac.ugm.glmb.lestari.mainmenu.login.LoginModel;
import id.ac.ugm.glmb.lestari.mainmenu.syaratKetentuan.DisclaimerActivity;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariParam;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

import static id.ac.ugm.glmb.lestari.utilities.Lestari.pilihanJK;

public class RegisterExternalActivity extends ActivityGeneral implements View.OnClickListener{
    @BindView(R.id.input_email) protected EditText input_email;
    @BindView(R.id.input_token) protected EditText input_token;
    @BindView(R.id.input_username) protected EditText input_username;
    @BindView(R.id.input_fullname) protected EditText input_fullname;
    @BindView(R.id.input_jk) protected EditText input_jk;
    @BindView(R.id.input_password) protected EditText input_password;
    @BindView(R.id.input_password_ulangi) protected EditText input_password_ulangi;
    @BindView(R.id.card_register) protected CardView card_register;
    @BindView(R.id.cb_checklist) protected CheckBox cb_checklist;
    @BindView(R.id.tv_syarat) protected TextView tv_syarat;

    String username, password, fullname, email, gender;

    List<Integer> forms = new ArrayList<>();
    ToolbarLestari toolbarLestari;
    DialogPilihan dialogPilihan;

    LoginModel loginModel;
    int isTrue = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_external);
        ButterKnife.bind(this);

        if(getIntent().hasExtra("loginModel")){
            loginModel = (LoginModel) getIntent().getSerializableExtra("loginModel");
            initDataFromIntent();
        }

        toolbarLestari = new ToolbarLestari(getContext(), "Register");
        dialogPilihan = new DialogPilihan(getContext(), pilihanJK(), "JENIS KELAMIN");
        dialogPilihan.setOnItemClick(new DialogPilihan.OnItemClick() {
            @Override
            public void onItemClick(View view, PilihanClass pilihanClass, int position) {
                gender = pilihanClass.getId();
                input_jk.setText(pilihanClass.getName());
                input_jk.setError(null);
            }
        });

        setFormsToValidate();

        card_register.setOnClickListener(this);
        input_jk.setOnClickListener(this);
        tv_syarat.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.card_register:
                validateData();
                break;
            case R.id.input_jk:
                dialogPilihan.show();
                break;
            case R.id.tv_syarat:
                startActivity(new Intent(getContext(), DisclaimerActivity.class));
                break;
        }
    }

    private void setFormsToValidate(){
        forms.add(R.id.input_email);
        forms.add(R.id.input_username);
        forms.add(R.id.input_fullname);
        forms.add(R.id.input_jk);
        forms.add(R.id.input_password);
        forms.add(R.id.input_password_ulangi);
    }

    private void validateData(){
        if(Lestari.isFormValid(this, getWindow().getDecorView(), forms)){
            if(!Lestari.isValidEmail(input_email.getText().toString().trim())){
                input_email.setError("Email tidak valid");
            }else if(!input_password.getText().toString().equalsIgnoreCase(input_password_ulangi.getText().toString())){
                input_password_ulangi.setError("Password harus sama");
            }else{
                if(cb_checklist.isChecked()){
                    fullname = input_fullname.getText().toString();
                    username = input_username.getText().toString();
                    email = input_email.getText().toString();
                    password = input_password.getText().toString();
                    doRegister(LestariParam.stringRegister());
                } else {
                    Lestari.alertWarning(getContext(), "Anda belum menyetujui syarat & ketentuan yang berlaku");
                }
            }
        }
    }

    private void initDataFromIntent(){
        Log.d("google", "initDataFromIntent: "+loginModel.getEmail());
        if (loginModel.getEmail().equalsIgnoreCase("")) {
            input_email.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent),
                    PorterDuff.Mode.SRC_ATOP);
            input_email.setEnabled(true);
            isTrue = 0;
        }else {
            input_email.getBackground().setColorFilter(getResources().getColor(R.color.grey_500),
                    PorterDuff.Mode.SRC_ATOP);
            input_email.setEnabled(false);
            isTrue = 1;
//            edt_email = new StyleFormNonActive(getContext());
        }
        input_email.setText(loginModel.getEmail());

        input_fullname.setText(loginModel.getName());
        input_token.setText(loginModel.getuId());
    }

    private void doRegister(String URL){
        LestariRequest.POST(URL, getContext(), new LestariRequest.OnPostRequest() {
            @Override
            public void onPreExecuted() {
                LestariUi.ShowLoadingDialog(getContext());
            }

            @Override
            public void onSuccess(JSONObject response) {
                LestariUi.HideLoadingDialog(getContext());
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        if(isTrue == 1){
                            if(LoginJsonHelper.isUserValid(getContext(), response)){
                                LestariUi.ToastShort(getContext(), getResources().getString(R.string.register_success));
                                FirebaseMessaging.getInstance().subscribeToTopic(Lestari.getSPString(getContext(), LestariConstant.SP_USER_ID));
                                if(Lestari.getSPBoolean(getContext(), LestariConstant.SP_INTRO)){
                                    startActivity(new Intent(getContext(), MainActivity.class).putExtra("isLogin", true));
                                } else {
                                    startActivity(new Intent(getContext(), AppIntroActivity.class));
                                }
                                finish();
                            }else{
                                Lestari.alertWarning(getContext(), getResources().getString(R.string.register_failed));
                            }
                        } else {
                            LestariUi.ToastShort(getContext(), getResources().getString(R.string.register_success_auth));
                            finish();
                        }
                    }else{
                        Lestari.alertWarning(getContext(), getResources().getString(R.string.register_failed)+", "+response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String error) {
                LestariUi.HideLoadingDialog(getContext());
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("lestari_token_sosmed", loginModel.getuId());
                param.put("lestari_register_user", ""+true);
                param.put("lestari_username", username);
                param.put("lestari_password", password);
                param.put("lestari_fullname", fullname);
                param.put("lestari_gender", gender);
                param.put("lestari_register_from", "mobile");
                param.put("lestari_level", "user");
                param.put("lestari_email", ""+email);
                param.put("hash_id", "");
                param.put("lestari_email_auth", ""+isTrue);
                Log.d("asdParams", "requestParam: "+param);
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                return param;
            }
        });
    }
}
