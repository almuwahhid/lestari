package id.ac.ugm.glmb.lestari.mainmenu.profil;

import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ugm.glmb.lestari.R;
import id.ac.ugm.glmb.lestari.utilities.Lestari;
import id.ac.ugm.glmb.lestari.utilities.LestariConstant;
import id.ac.ugm.glmb.lestari.utilities.LestariParam;
import id.ac.ugm.glmb.lestari.utilities.ToolbarLestari;
import id.ac.ugm.glmb.uilib.Activity.ActivityGeneral;
import id.ac.ugm.glmb.uilib.utils.LestariRequest;
import id.ac.ugm.glmb.uilib.utils.LestariUi;

public class UbahPasswordActivity extends ActivityGeneral {
    @BindView(R.id.card_update) protected CardView card_update;
    @BindView(R.id.input_password_lama) protected EditText input_password_lama;
    @BindView(R.id.input_password_baru) protected EditText input_password_baru;
    @BindView(R.id.input_password_ulangi) protected EditText input_password_ulangi;

    List<Integer> formLupaPassword = new ArrayList<>();
    private void addFormLupaPassword() {
        formLupaPassword = new ArrayList<>();
        formLupaPassword.add(R.id.input_password_lama);
        formLupaPassword.add(R.id.input_password_baru);
        formLupaPassword.add(R.id.input_password_ulangi);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_password);
        ButterKnife.bind(this);

        ToolbarLestari toolbarLestari = new ToolbarLestari(getContext(), "Ubah Password");
        addFormLupaPassword();

        card_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Lestari.isFormValid(getContext(), getWindow().getDecorView(), formLupaPassword)) {
                    if(input_password_baru.getText().toString().equalsIgnoreCase(input_password_ulangi.getText().toString())){
                        doUbahPassword(LestariParam.stringEditPassword());
                    }else {
                        input_password_ulangi.setError("Password tidak sesuai");
                    }
                }else {
                    Lestari.alertWarning(getContext(), "Anda belum mengisi semua form yang tersedia");
                }
            }
        });
    }

    private void doUbahPassword(String URL){
        LestariRequest.POST(URL, getContext(), new LestariRequest.OnPostRequest(){
            @Override
            public void onPreExecuted() {
                LestariUi.ShowLoadingDialog(getContext());
            }

            @Override
            public void onSuccess(JSONObject response) {
                LestariUi.HideLoadingDialog(getContext());
                try {
                    if(response.getInt("code")== LestariConstant.CODE_SUCCESS){
                        Lestari.alertSuccess(getContext(), "Password berhasil diubah");
                    }else if(response.getInt("code")==LestariConstant.CODE_EMPTY){
                        Lestari.alertWarning(getContext(), "Password Anda salah");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                LestariUi.HideLoadingDialog(getContext());
            }

            @Override
            public Map<String, String> requestParam() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("lestari_edit_password", "true");
                param.put("lestari_new_password", input_password_baru.getText().toString());
                param.put("lestari_old_password", input_password_lama.getText().toString());
                param.put("lestari_id_user", Lestari.getSPString(getContext(), LestariConstant.SP_USER_ID));
                return param;
            }

            @Override
            public Map<String, String> requestHeaders() {
                Map<String, String> header_param = new HashMap();
                return header_param;
            }
        });
    }
}
